# Documentation for extended Dynamical Causal Modeling for Phase Coupling (eDCM PC)

## Table of contents

[[_TOC_]]

* [Short overview of the theory](Theory.md)
* [Short overview of the methods](Methods.md)
* [How to use eDCM PC](Usage.md)
* [Demonstration script 1: weakly coupled phase oscillators](Demo1.md)
* [Demonstration script 2: coupled neural mass models](Demo2.md)
* [Structure of the package](Structure.md)


## About eDCM PC
Extended Dynamical Causal Modelling for phase coupling (eDCM PC)[^Yeldesbay2019] is an additional collection of scripts to Dynamical Causal Modelling (DCM)[^Friston2003] that allows to estimate the coupling between oscillatory systems using the phase information extracted from the measured signals in the case of non-uniform phase distributions. This collection of scripts extends the version of the Dynamical Causal Modelling for phase coupling [^Penny2009].

## About this document
This document is a manual to the eDCM PC. It covers an introductory overview of the theory and methods the package is based on, explains how the package works, and provides practical demonstration examples of the usage of the package.

This manual is intended to an audience who is familiar with the theory of weakly coupled phase oscillators and synchronization, nevertheless a short introduction to these topics is provided. Knowledge on the Statistical Parametric Mapping (SPM) and the Dynamical Causal Modelling (DCM) is not necessary.

Here is a list of references prior to reading this documentation:
* You can find more information about the synchronization phenomenon and the theory of weakly coupled oscillators in [^SynchronizationBook], [^KuramotoBook], [^HoppensteadtIzhikevich1997].
* Detailed description of the SPM and DCM can be found in the manual of the SPM [^FristonSPM2007], especially the Chapters 41, 42, and 43. Also the original works on DCM [^Friston2003], also [^Daunizeau2011] and [^Stephan2010].
* The original work on DCM for phase coupling [^Penny2009].
* The original work about phase and protophase [^Kralemann2008], and the following related and subsequent works [^RosenblumPikovsky2001], [^Kralemann2007], and [^Kralemann2014]. Also, please, refer to [the manual of the DAMOCO toolbox](http://www.stat.physik.uni-potsdam.de/~mros/damoco.html)  [^DAMOCOManual].
* The original work on the extended DCM PC [^Yeldesbay2019].

## Installation

The eDCM PC is based on the Dynamic Causal Modelling package, which is part of the Statistical Parametric Mapping (SPM) package developed for Matlab.

### Installation of Matlab

For installation of Matlab, please follow the instructions given on the [website](https://www.mathworks.com/products/matlab.html).

### Installation of SPM

To install SPM follow the instructions provided on the [website](https://www.fil.ion.ucl.ac.uk/spm/software/download/) of the package.

After installation one can test the package by running the following code
```
addpath('C:/spm12'); % here give the path to the SPM.
spm eeg;
```
After running these commands you will see the Welcome-window of SPM 12 and the SPM Menu-Window as shown in the figure below.

<img src="Figs/SPM12Windows.png" alt="SPM12 Windows" width="600"/>

### Installation of eDCM PC

Download or clone the eDCM PC package from the GitLab [repository](https://gitlab.com/azayeld/edcmpc).

To test the package start Matlab, go to the folder where eDCM PC is copied and run the testing script by calling the following command:

```
cd SimPhOsc
addpath(`path/to/spm/folder`); % change the path!
test;
```

More detailed description of the testing script is given in the Chapter [How to use eDCM PC](Usage.md).

<!--
# Short overview of the theory
[Theory](Theory.md)

# Short overview of the methods
[Methods](Methods.md)

# How to use eDCM PC



[Usage](Usage.md)

# Demonstration script 1: weakly coupled phase oscillators
[Demonstration script 1](Demo1.md)

# Demonstration script 2: coupled neural mass models
[Demonstration script 2](Demo2.md)

# Structure of the package

* The components and folders.
* How to start using the package.  
-->

[Up](##table-of-contents) | [Next Chapter: Short overview of the theory](Theory.md)

## References

[^Yeldesbay2019]: Yeldesbay, A., Fink, G. R., & Daun, S. (2019). Reconstruction of effective connectivity in the case of asymmetric phase distributions. Journal of Neuroscience Methods, 317(February), 94–107. https://doi.org/10.1016/j.jneumeth.2019.02.009.
[^Friston2003]: Friston, K.J., Harrison, L., Penny, W., 2003. Dynamic causal modelling. NeuroImage 19 (4), 1273–1302. https://doi.org/10.1016/S1053-8119(03)00202-7.
[^Penny2009]: Penny, W.D., Litvak, V., Fuentemilla, L., Duzel, E., Friston, K., 2009. Dynamic causal models for phase coupling. J. Neurosci. Methods 183 (1), 19–30. https://doi.org/10.1016/j.jneumeth.2009.06.029.
[^SynchronizationBook]: Pikovsky, A., Rosenblum, M., Kurths, J., 2001. Synchronization: A Universal Concept in Nonlinear Sciences. Cambridge Nonlinear Science Series. Cambridge University Press. https://www.cambridge.org/de/academic/subjects/physics/nonlinear-science-and-fluid-dynamics/synchronization-universal-concept-nonlinear-sciences?format=PB&isbn=9780521533522
[^KuramotoBook]: Kuramoto, Y., 1984. Chemical Oscillations, Waves, and Turbulence, vol. 19 Springer, Berlin Heidelberg, Berlin, Heidelberg. https://doi.org/10.1007/978-3-642-69689-3.
[^SyncBook]: Strogatz, S. (2004). Sync: The Emerging Science of Spontaneous Order. Vereinigtes Königreich: Penguin Books Limited.
[^HoppensteadtIzhikevich1997]: Hoppensteadt, F.C., Izhikevich, E.M., 1997. Weakly Connected Neural Networks. Applied Mathematical Sciences, vol. 126 Springer, New York, New York, NY. https://doi.org/10.1007/978-1-4612-1828-9.
[^FristonSPM2007]: Friston, K., Ashburner, J., Kiebel, S., Nichols, T., Penny, W. (Eds.), 2007. Statistical Parametric Mapping. Elsevier. https://doi.org/10.1016/B978-0-12-372560-8.X5000-1.
[^Daunizeau2011]: Daunizeau, J., David, O., Stephan, K.E., 2011. Dynamic causal modelling: a critical review of the biophysical and statistical foundations. NeuroImage 58 (2), 312–322. https://doi.org/10.1016/j.neuroimage.2009.11.062.
[^Stephan2010]: Stephan, K.E., Penny, W.D., Moran, R.J., den Ouden, H.E.M., Daunizeau, J., Friston, K.J., 2010. Ten simple rules for dynamic causal modeling. NeuroImage 49 (4), 3099–3109. https://doi.org/10.1016/j.neuroimage.2009.11.015.
[^Kralemann2008]: Kralemann, B., Cimponeriu, L., Rosenblum, M., Pikovsky, A., & Mrowka, R. (2008). Phase dynamics of coupled oscillators reconstructed from data. Physical Review E, 77(6), 1–16. https://doi.org/10.1103/PhysRevE.77.066205.
[^RosenblumPikovsky2001]: Rosenblum, M.G., Pikovsky, a.S., 2001. Detecting direction of coupling in interacting oscillators. Phys. Rev. E: Stat. Nonlinear Soft Matter Phys. 64 (4 Pt 2), 045202. https://doi.org/10.1103/PhysRevE.64.045202.
[^Kralemann2007]: Kralemann, B., Cimponeriu, L., Rosenblum, M., Pikovsky, A., Mrowka, R., 2007. Uncovering interaction of coupled oscillators from data. Phys. Rev. E 76 (5), 1–4. https://doi.org/10.1103/PhysRevE.76.055201.
[^Kralemann2014]: Kralemann, B., Pikovsky, A., Rosenblum, M., 2014. Reconstructing effective phase connectivity of oscillator networks from observations. N. J. Phys. 16 (8), 085013. https://doi.org/10.1088/1367-2630/16/8/085013.
[^DAMOCOManual]: Rosenblum, M., Pikovsky, A., Kralemann, B., Rosenblum, M., & Pikovsky., A. (2011). DAMOCO : MATLAB toolbox for multivariate data analysis, based on coupled oscillators approach. http://www.stat.physik.uni-potsdam.de/~mros/damoco/damoco_manual_v1.pdf
