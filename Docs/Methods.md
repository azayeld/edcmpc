# Short overview of the methods

Extended DCM for phase coupling (eDCM PC) takes into account the non-linear relation between the theoretical and observable phases to resolve the reconstruction problem mentioned in the [theoretical part](Theory.md##Observable-vs-theoretical-phase) of this documentation. Namely, eDCM PC performs the reconstruction of the coupling functions together with the reconstruction of the forward transformation, i.e. the non-linear relation. This is done within the framework of the Dynamical Causal Modelling (DCM) package.

## Overview of DCM

Dynamical Causal Modelling (DCM) is a tool to analyze the effective connectivity between brain regions using the data measured with different modalities (fMRI, EEG, MEG, LFP, fNIRS) [^FristonSPM2007],[^Friston2003],[^David2006],[^Chen2008],[^Chen2012],[^Stephan2008],[^Moran2009],[^Tak2015], [^Stephan2010], [^Daunizeau2011]. All DCM variants have a common general structure [^Daunizeau2011], which is shown in a simplified form in Fig. 6.

<div align="center">
 <img src="Figs/DCMschemeV4.png" alt="Scheme of DCM" width="600">

 <em>Fig. 6. The general scheme of DCM (adapted from Yeldesbay et al. (2019) [^Yeldesbay2019].</em>
</div>

The measured data (1.) are first transferred to pre-processed observables (2.). These observables are "compared" with the output of a non-linear dynamic system. This system is called *the modelling component* (3.) of DCM [^Daunizeau2011]. The "comparison" is performed in *the statistical component* (4.) of DCM using Variational Bayesian Inference (5.). The cycle of generation of the new observables using the corrected parameters and comparing them with the observables form the measured data continues until a given error tolerance is reached (6.).

The final result is returned as mean values and standard deviations of the system parameters.

## Dynamic Causal Modelling for Phase Coupling

In Penny et al. (2009) [^Penny2009] DCM was implemented for finding the coupling between oscillatory systems using the phase information obtained from the signals. In this version of the DCM, i.e. DCM for phase coupling (DCM PC), the coupling function was taken as a function of the 1:1 resonance case:

```math
 \dot{\varphi}_i = \omega_i + \sum_{j=1, j\neq i}^{N_q} \Gamma_{ij}(\varphi_i-\varphi_j).
```

The coupling function $`\Gamma_{i,j}`$ was represented as Fourier series and the Fourier coefficients were used to asses the coupling strength between the oscillatory systems. The reconstruction of the coupling functions was performed directly for the theoretical phases.

## Extended modelling components

To be able to work with the observable phases an extension of DCM PC was presented in [^Yeldesbay2019]. The extended DCM PC (eDCM PC) is mostly implemented in the modelling component of the DCM. The scheme of eDCM PC is shown in Fig. 7. It is mainly similar to the one presented in Fig. 6.

<div align="center">
 <img src="Figs/Scheme_eDCMPC.png" alt="Scheme of the extended DCM PC" width="650">

 <em>Fig. 7. The scheme of the extended DCM PC (adapted from Yeldesbay et al., 2019 [^Yeldesbay2019]).</em>
</div>

The evolution equation is
```math
\dot{\varphi}_i = \omega_i + \sum_{j=1, j\neq i}^{N_q} q_{ij}(\varphi_i,\varphi_j),
```
where
```math
q_{ij}(\varphi_i,\varphi_j) = \sum_{n,m = N_q,n,m\neq 0}^{N_q} Q_{ij}e^{i(n\varphi_i + m \varphi_j)}.
```

This form of the evolution equation makes it possible to detect $`m:n`$ synchronization, which was not possible with DCM PC.

The forward transformation $`\Theta(\varphi)`$ is reconstructed in the observable equation (Fig. 7. (3a)). According to Kralemann et al. (2008)[^Kralemann2008] we have:
```math
\dot{\theta}_i=\Theta(\varphi_i) = \varphi_i + \sum_{k=-N_{\rho}, k\neq 0}^{N_{\rho}}(e^{ik\varphi_i}-1).
```

The eDCM PC uses the observable function as *the forward transformation* and its parameters ($`p_{\rho}`$) are reconstructed together with the parameters of the coupling function ($`p_q`$). However, for calculation of the initial conditions $`\varphi_0`$ also *the inverse transformation* is needed (Fig. 7 (3a)). We can approximate the inverse transformation using the distribution of the observable phases (Fig. 7 (3b)). Then, in every step, we update the inverse transformation using the parameters of the forward transformation (Fig. 7 (3a)).

Please note that in the modelling component of the extended DCM PC the coupling and the transformation functions are calculated with the real valued Fourier coefficients. Thus, the coupling function between the region $`i`$ and $`j`$ is given by
```math
 q_{ij}(\varphi_i,\varphi_j) = \sum_{n=1}^{N_q}\sum_{m=1}^{N_q} \big[a^{(n,m)}_{ij}
\cos(n\varphi_i)\cos(m\varphi_j) +  b^{(n,m)}_{ij}
\cos(n\varphi_i)\sin(m\varphi_j) + c^{(n,m)}_{ij} \sin(n\varphi_i)\cos(m\varphi_j) + d^{(n,m)}_{ij}
\sin(n\varphi_i)\sin(m\varphi_j)\big],
```
where $`a_{ij}^{(n,m)}`$, $`b_{ij}^{(n,m)}`$, $`c_{ij}^{(n,m)}`$, $`d_{ij}^{(n,m)}`$ are the real-valued Fourier coefficients, each of the size $`N_q \times N_q`$.  

And the forward transformation in real valued Fourier coefficients is given by
```math
 \theta_i = \Theta(\varphi_i) = \varphi_i +
\sum\limits_{k=1}^{N_{\rho}}\frac{1}{k}\left[\alpha_i^{(k)}\sin(n\varphi_i) -
\beta_i^{(k)}\cos(n\varphi_i) + \beta_i^{(k)}\right],
```
where $`\alpha_i^{(k)}`$ and $`\beta_i^{(k)}`$ are the real-valued Fourier coefficients, each of the size $`N_\rho`$.

[Previous Chapter: Theory](Theory.md) | [Up](#short-overview-of-the-methods) | [Next Chapter: How to use eDCM PC](Usage.md)


## References
[^Friston2003]: Friston, K.J., Harrison, L., Penny, W., 2003. Dynamic causal modelling. NeuroImage 19 (4), 1273–1302. https://doi.org/10.1016/S1053-8119(03)00202-7.
[^FristonSPM2007]: Friston, K., Ashburner, J., Kiebel, S., Nichols, T., Penny, W. (Eds.), 2007. Statistical Parametric Mapping. Elsevier. https://doi.org/10.1016/B978-0-12-372560-8.X5000-1.
[^David2006]: David, O., Kiebel, S.J., Harrison, L.M., Mattout, J., Kilner, J.M., Friston, K.J., 2006. Dynamic causal modeling of evoked responses in EEG and MEG. NeuroImage 30 (4), 1255–1272. https://doi.org/10.1016/j.neuroimage.2005.10.045.
[^Chen2008]: Chen, C.C., Kiebel, S.J., Friston, K.J., 2008. Dynamic causal modelling of induced responses. NeuroImage 41 (4), 1293–1312. https://doi.org/10.1016/j.neuroimage.2008.03.026.
[^Chen2012]: Chen, C.C., Kiebel, S.J., Kilner, J.M., Ward, N.S., Stephan, K.E., Wang, W.J., Friston, K.J., 2012. A dynamic causal model for evoked and induced responses. NeuroImage 59 (1), 340–348. https://doi.org/10.1016/j.neuroimage.2011.07.066.
[^Stephan2008]: Stephan, K.E., Kasper, L., Harrison, L.M., Daunizeau, J., den Ouden, H.E.M., Breakspear, M., Friston, K.J., 2008. Nonlinear dynamic causal models for fMRI. NeuroImage 42 (2), 649–662. https://doi.org/10.1016/j.neuroimage.2008.04.262.
[^Moran2009]: Moran, R.J., Stephan, K.E., Seidenbecher, T., Pape, H.C., Dolan, R.J., Friston, K.J., 2009. Dynamic causal models of steady-state responses. NeuroImage 44 (3), 796–811. https://doi.org/10.1016/j.neuroimage.2008.09.048.
[^Tak2015]: Tak, S., Kempny, A.M., Friston, K.J., Leff, A.P., Penny, W.D., 2015. Dynamic causal modelling for functional near-infrared spectroscopy. NeuroImage 111, 338–349. https://doi.org/10.1016/j.neuroimage.2015.02.035.
[^Daunizeau2011]: Daunizeau, J., David, O., Stephan, K.E., 2011. Dynamic causal modelling: a critical review of the biophysical and statistical foundations. NeuroImage 58 (2), 312–322. https://doi.org/10.1016/j.neuroimage.2009.11.062.
[^Stephan2010]: Stephan, K.E., Penny, W.D., Moran, R.J., den Ouden, H.E.M., Daunizeau, J., Friston, K.J., 2010. Ten simple rules for dynamic causal modeling. NeuroImage 49 (4), 3099–3109. https://doi.org/10.1016/j.neuroimage.2009.11.015.
[^Yeldesbay2019]: Yeldesbay, A., Fink, G. R., & Daun, S. (2019). Reconstruction of effective connectivity in the case of asymmetric phase distributions. Journal of Neuroscience Methods, 317(February), 94–107. https://doi.org/10.1016/j.jneumeth.2019.02.009.
[^Kralemann2008]: Kralemann, B., Cimponeriu, L., Rosenblum, M., Pikovsky, A., & Mrowka, R. (2008). Phase dynamics of coupled oscillators reconstructed from data. Physical Review E, 77(6), 1–16. https://doi.org/10.1103/PhysRevE.77.066205.
[^Penny2009]: Penny, W.D., Litvak, V., Fuentemilla, L., Duzel, E., Friston, K., 2009. Dynamic causal models for phase coupling. J. Neurosci. Methods 183 (1), 19–30. https://doi.org/10.1016/j.jneumeth.2009.06.029.
