# Short overview of the theory

## Measuring the strength of the coupling between oscillating systems.

When two oscillatory systems (i.e. systems with periodic output signal) interact with each other for some period of time they demonstrate synchronous activity [^SynchronizationBook]. The synchronization expresses the fact that the output signals have similar forms within a given frequency band and is thereby reflected in the phases of the signals. Constant phase difference means full synchronization.

The strength of the interaction of the systems can be measured by the magnitude of the synchronization: the stronger the interaction (coupling) between the systems the stronger the synchronization. One can thus measure the strength of the coupling between the systems by analyzing the phases of the signals.

The phases of a signal can be obtained by Hilbert or Wavelet transformation methods [^SynchronizationBook]. The phase is calculated as an argument of the complex variable.

## Weakly coupled phase oscillators
To analyze the interaction between oscillatory systems the theory of synchronization [^SynchronizationBook] [^KuramotoBook][^HoppensteadtIzhikevich1997] considers the model of weakly coupled phase oscillators.
```math
\dot{\varphi}_i = \omega_i + F_i(\varphi_1,\ldots,\varphi_N),
```
where $`\varphi_i`$ is the phase of the system $`i`$, $`\omega_i`$ is the natural frequency of the system, $`F_i`$ is the interaction function (coupling function) with other systems. In a simpler form the model is given by pairwise coupling functions:
```math
\dot{\varphi}_i = \omega_i + \sum_{j=1, j\neq i}^{N}q_{i,j}(\varphi_i,\varphi_j).\tag{1}
```
As we can see without any coupling a phase oscillator in theory grows linearly: $`\dot{\varphi}_i = \omega_i`$.

The idea of the reconstruction of the effective phase coupling is to find the coupling functions $`q_{i,j}`$ from the phases of the measured signals, which is equivalent to the reconstruction of a surface $`q_{i,j}(\varphi_i,\varphi_j)`$ .

<div align="center">
 <img src="Figs/q12surf.png" alt="Coupling function as a surface" width="300">

 <em>Fig.1. The coupling function $`q_{1,2}(\varphi_1,\varphi_2)`$ as a surface.</em>
</div>

## Observable vs theoretical phase

If we know the phase of all interacting systems of a network we can reconstruct the coupling functions in that network. However, in practice, the phase that is obtained from the measured signal (observable phase) can not be described with the theoretical model (1). According to the model the phase should grow linearly when input is zero (Fig. 1). This is not the case for the observable phase. For example, if the limit cycle of the two dimensional oscillation is not circular and we observe one variable then, as it is shown in Fig. 2, the phase reconstructed as the angle will grow non-linearly. This effect has been shown in Kralemann et al. (2008)[^Kralemann2008].

<div align="center">
 <img src="Figs/LCphosc_sin_phase_fast.gif" alt="Animation of linear growing theoretical phase" width="600" >

 <em>Fig.2. Linear growing theoretical phase.</em>
</div>

<div align="center">
<img src="Figs/LCVdP_sin_phase_fast.gif" alt="Animation of non-linear growing observable phase" width="600" >

<em>Fig.3. Due to not circular limit cycle the observable phase grows non-linearly.</em>
</div>

This non-linear growing of the observable phase can be considered as a non-linear relation between the theoretical and the observable phase (Fig. 4). Please note, that the theoretical and observable phase are called the true and proto phase in [^Kralemann2008], correspondingly.

<div align="center">
<img src="Figs/RelThPhObsPh.png" alt="Relation between the theoretical and the observable phases" width="300">

<em>Fig.4. The relation between the theoretical and the observable phases.</em>
</div>

The non-linear relation (or the non-linear grows of the observable phase) affects the reconstructed coupling function, as shown in Fig. 5. Here we generated a data set from a model of two coupled oscillators with given coupling functions shown in the left panels. Then we artificially added non-linear distortions shown in the middle panels and reconstructed the coupling functions. As a result we obtained the coupling functions shown in the right panels, which are strikingly different from the original functions.

<div align="center">
 <img src="Figs/ExplTransf_part2_cut.png" alt="Nonlinearily affects reconstruction" width="600">

 <em>Fig.5. Nonlinearity affects the reconstruction of the coupling functions.<br> 
 Left panels: the real coupling functions.<br> 
 Middle panels: the non-linear relation between theoretical and observable phases. <br> 
 Right panels: Reconstruction of the coupling functions from the observable phases.</em>
</div>

## Forward and inverse transformations

The non-linear relation between the theoretical and observable phases can be presented as a transformation function [^Kralemann2008]. According to [^Kralemann2008] the inverse transformation function $`\sigma(\theta)`$ can be estimated as a probability distribution of the observable phases $`\theta`$ for a long period of time (was meinst du mit for a long period of time?) (Fig. 6). The integral of $`\sigma(\theta)`$ gives us the inverse transformation $`\Phi(\theta)`$, which is the same as the non-linear relation between the observable phase $`\theta`$ and theoretical phase $`\varphi`$. The inverse of this transformation function is the forward transformation $`\Theta(\varphi)`$, which can also be obtained as the integral of the forward transformation function $`\rho(\varphi)`$.

In short, we use the inverse transformation to obtain the theoretical phases from the observable ones; and vise versa, we use the forward transformation to find the observable phases form the theoretical ones.

<div align="center">
 <img src="Figs/ExplTransf_part3_cut.png" alt="Explanation of the forward and the inverse transformation" width="600">

 <em>Fig. 6. The forward and inverse transformations. <br>
 The inverse transformation can be estimated by finding the distribution of the observable phases. <br>
 [Adapted from Yeldesbay et al (2019) [^Yeldesbay2019]]</em>
</div>

[Previous Chapter: Documentation](Doc.md) | [Up](#short-overview-of-the-theory) | [Next Chapter: Short overview of the methods](Methods.md)


## References

[^Yeldesbay2019]: Yeldesbay, A., Fink, G. R., & Daun, S. (2019). Reconstruction of effective connectivity in the case of asymmetric phase distributions. Journal of Neuroscience Methods, 317(February), 94–107. https://doi.org/10.1016/j.jneumeth.2019.02.009.
[^Kralemann2008]: Kralemann, B., Cimponeriu, L., Rosenblum, M., Pikovsky, A., & Mrowka, R. (2008). Phase dynamics of coupled oscillators reconstructed from data. Physical Review E, 77(6), 1–16. https://doi.org/10.1103/PhysRevE.77.066205
[^SynchronizationBook]: Pikovsky, A., Rosenblum, M., and Kurths, J. (2001). Synchronization: a Universal Concept in Nonlinear Sciences. Cambridge: University Press. doi: 10.1017/CBO9780511755743.
[^KuramotoBook]: Kuramoto, Y., 1984. Chemical Oscillations, Waves, and Turbulence, vol. 19 Springer, Berlin Heidelberg, Berlin, Heidelberg. https://doi.org/10.1007/978-3-642-69689-3.
[^HoppensteadtIzhikevich1997]: Hoppensteadt, F.C., Izhikevich, E.M., 1997. Weakly Connected Neural Networks. Applied Mathematical Sciences, vol. 126 Springer, New York, New York, NY. https://doi.org/10.1007/978-1-4612-1828-9.
