# Structure of the eDCM PC

## Structure of the folders
- `/ModComp` contains the modeling component of eDCM PC and other additional functions used in the package;

- `/Fitting` contains scripts that fit different simulated data sets using eDCM PC;

- `/PlotRes` contains scripts that plot the results;

- `/TestDataset` contains the scripts for preliminary testing the data sets, to find some parameters for running eDCM PC;

- `/Demos` contains the demonstration scripts;

- `/SimPhOsc` contains scripts for the simulation of two coupled phase oscillators and the results of the fitting with the original DCM PC and extended DCM PC;

- `/SimNMM` contains sctipts for the simulation of two coupled neural mass models and the results of the fitting with eDCM PC;

- `/Docs/` contains the documentation files.

## List of functions

<!---
Here we add the final list of functions after cleaning the temporary and old files.
--->

[Previous Chapter: Demonstration script 2](Demo2.md) | [Up](#structure-of-the-edcm-pc)  
