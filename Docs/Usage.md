# How to use eDCM PC

The extended DCM PC uses the `SPM` package. Therefore all parameters are defined in the form conventional in `SPM`, namely in the DCM for phase coupling [^Penny2009].  

eDCM PC can be used by setting the values of the fitting parameters manually in your own code, namely by defining the fields of the `DCM` structure and calling the function `spm_dcm_po(DCM)`. Below we present the minimal script that shows the usage of eDCM PC, which serves as a testing script. For more advanced examples for the usage of eDCM PC, please, refer to the demonstration scripts `Demo1_2PhOsc.m` and `Demo2_2NMM.m` in chapters [Demonstration script 1](Demo1.md) and [Demonstration script 2](Demo2.md).

In these scripts the function `fFit_eDCM_PC(fitpar)` sets the parameter values of the `DCM` structure and calls the function `spm_dcm_po(DCM)`.

## Testing script

The [testing script](../SimPhOsc/test.m) `test.m` is situated in the `SimPhOsc` folder. It loads the pre-simulated dataset (`test_dataset.mat`) generated from a system of two coupled phase oscillators by means of the script `gen_test_dataset.m`. To accelerate the convergence the system is integrated without noise.

### Sample code

The following example code shows the minimal sample code that shows which fields should be defined in the `DCM` structure before calling the function `spm_dcm_po(DCM)`:

```
DCM = [];           % allocate space for the DCM structure


for n = 1:Nt,       % Nt is the number of trials.
    DCM.xY.y{n} = phi{n,1}; % trials with the unwrapped phases
                    % phi{n,1} is the Nx2 matrix, N is the number of time samples
end     
DCM.xY.dt = dt;     % the time step
DCM.xY.pst = time;  % the peristimulus time, array of the size Nx1.
                    % if uniform distributed time then define as (0:N-1)'*dt.

% Find the derivatives of the observable phases and the mean frequency.
[dthetas,uphis,thetas,mFreq] = derivs(DCM.xY.y,DCM.xY.pst);

M  = [];            % allocate space for the M structure (the model structure).
M.freq = mFreq';    % define the mean frequencies, same as mean(dthetas)'.
M.isslowphase = 0;  % to reconstruct with slow phase. Not used any more, set to 0.

% Define the range of the frequency band. Can be defined manually.
M.fb = max(abs(dthetas - repmat(mean(dthetas),[size(dthetas,1),1])))';

DCM.options.Fdcm =repmat(M.freq,1,2) + [-M.fb M.fb];

DCM.options.istransf = 0; % an option to work with theoretical phase. Not used any more, set to 0.

% Define the parameters of the coupling coefficients
M.Nsig = Nsig;      % The order of Fourier terms of the sigma function
M.Nrho = Nrho;      % The order of Fourier terms of the rho function
M.Nq = Nq;          % The order of Fourier terms for the coupling function q_ij

% Define the coupling structure in the DCM structure
DCM.A = A;          % the coupling matrix, e.g. [0 1; 1 0] is bidirectional coupling
DCM.B{1} = zeros(size(DCM.A)); % inter-trial effect matrix, set if appropriate

% Define the initial phases from all trials
for n = 1:Nt
    phi0 = DCM.xY.y{n}(1,:);
    trial{n}.x = phi0';
end

% Define further the elements the M structure
M.trial = trial;      
M.x = zeros(Nr,1);
M.ns = N;             % number of time samples

DCM.M = M;

% Define the elements of the U structure
U.u = zeros(N,1);
U.dt = dt;
U.tims = (0:N-1)'*dt;
U.X = zeros(Nt,1);

DCM.xU = U;
DCM.xU.oldX = U.X;

% Call the fitting function of eDCM PC
DCM = spm_dcm_po(DCM);
```

The following points should be taken into account:
* The trials in the `DCM` structure are stored in the cells `DCM.xY.y{n}`.
* Every trial is a matrix of the size `N x Nr`, where `Nr` is the number of regions and `N` is the number of time points.
* The size of the peristimulus time vector `DCM.xY.pst` must be equal to `N` (the same as the size of the trial matrix).
* The values of the parameters `Nsig`, `Nrho`, and `Nq` should be adjusted depending on the properties of the data set. The properties of the data set can be tested using the script `test_dataset.m` in the `TestDataset` folder. Please, refer to the [Demo scripts](Demo1.md) for details.
* Matrix `A` defines the structure of the network. The diagonal elements are always zero.

## Calling the test script

To call the test script go to the folder `SimPhOsc` and correct the line 66 by adding the proper path to the SPM folder.
```
...
addpath('/Path/to/SPM/Folder'); % correct this path before running
spm eeg;
...
```
then save the `test.m` script and run it
```
test;
```
in the MATLAB command line.

The window of SPM (see [Installation chapter](Doc.md###Installation-of-eDCM-PC) for details) and the welcome header in the MATLAB command window is shown in the figure below.

<div align="center">
 <img src="Figs/MatlabCmdWind.png" alt="The DCM Command Windows" width="800">

 <em>Fig.1. The MATLAB command window during the fitting process.</em>
</div>

Then the process of fitting starts, which can be seen in the command window (see Figure above) and in the separate DCM fitting window as shown below.

<div align="center">
 <img src="Figs/DCMFittingWnd.png" alt="DCMFitting Window" width="400">

 <em>Fig.2. The DCM window during the fitting process.</em>
</div>

In the command line in every step of the Bayesian inference the values of the free energy `F` is presented together with the predicted and actual increment. During the correct fitting process the value of the free energy `F` should grow.

In the DCM fitting window in the upper panel the data trials are presented with dashed lines and the trials generated by the modelling component by solid lines. During the fitting process the latter lines and the trial lines should converge.

In Fig. 2 in the bottom panel the increment of the parameters for the current stem is presented.

### Testing the results

The end of the fitting process is displayed in the command line with the word `convergence`. The results of the fitting are returned in the `DCM` structure. The mean values of the parameters are returned in `DCM.Ep`, the standard deviations in `DCM.Cp`. Using these values and the parameter values of the simulated system the testing script generates the comparison plot (Fig. 3).

<div align="center">
 <img src="Figs/CompRes.png" alt="Comparison of the results" width="400">

 <em>Fig.3. Comparing the resulting parameter values with the real values used for simulation of the synthetic data set. Blue circles are the original parameter values, red stars are the ones estimated by eDCM PC.</em>
</div>

[Previous Chapter: Methods](Methods.md) | [Up](#how-to-use-edcm-pc) | [Next Chapter: Demonstration script 1](Demo1.md)

## References

[^Penny2009]: Penny, W.D., Litvak, V., Fuentemilla, L., Duzel, E., Friston, K., 2009. Dynamic causal models for phase coupling. J. Neurosci. Methods 183 (1), 19–30. https://doi.org/10.1016/j.jneumeth.2009.06.029.
