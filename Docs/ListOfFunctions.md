#The list of functions of the package "extended DCM for phase coupling" eDCM PC

Here we list the functions from the package with their description.

## Original simulation functions and files

### Functions for simulation of the coupled phase oscillators
* `run_gen_stpo_dataset.m` generates a dataset of weakly coupled stochastic phase oscillators. Calls `simSt2PhOsc`
* `simSt2PhOsc.m` produces trials (integrates) of the stochastic coupled phase oscillators model. Calls `calcQphij.m`.

### Datasets for the simulation and fitting of coupled phase oscillators

* `stpo_dataset4_compN15_[no]R1.mat` 2 files. The simulation results of the coupled stochastic phase oscillators.  

* `dcm_nsig1_setn[1:15]_stpo_dataset4_compN15_[no]R1.mat` 30 files. Results of fitting with eDCM PC for the coupled stochastic phase oscillators.
* `dcm_orig3_nsig1_setn[1:15]_stpo_dataset4_compN15_[no]R1.mat` 30 files. Results of fitting with DCM PC (original program) for the coupled stochastic phase oscillators.

### Functions for simulation of the coupled stochastic Jansen and Rit (Neural mass) models

* `simJR2set_dcm_Hilb_Delay_onoff.m` simulates signals for the coupled stochastic J&R model. Calls `initSimJR2param60_[0:3].m`, `simJR2alltrial3.m`, previously called `simJR2alltrialdcmDelay.m`.
* `simJR2alltrial3.m` generates simulation dataset, all trials, for on/off case. Calls `intJR2delay.m`.
* `simJR2alltrialdcmDelay.m` the previous version of the simulation script.
* `intJR2delay.m` script integrates the system of stochastic differential equations, namely, the neural mass model. Calls `fJRnmm2dcmDelay` and `gJRnmm2`
* `fJRnmm2dcmDelay.m` the rhs of the J&R model.
* `gJRnmm2.m` the stochastic rhs of the J&R model.

### Datasets for the simulation and fitting of coupled J&R model

* `initSimJR2param60_[0:3].m` scripts that initialize the parameters of the J&R model.
* `outSimJR2_H_test_onoff_noise_107_0.mat` output file of the simulations of the J&R model.
* `dcm_nsig2_outSimJR2_H_test_onoff_noise_107_1.mat` output of the fitting of the J&R model with eDCM PC.

## The new simulation functions and files used for Demo scripts

### Demo1 scripts
* `Demo1_2PhOsc.m` is the main demonstration script for coupled phase osillators. Calls `set_param_sim.m`, `gen_stPhOsc_dataset.m`, `set_param_fit_phosc`, `test_datasets` from `/TestDataset`, `plot_res_edcm_pc` and `plot_comp_res_edcm_pc` from `/PlotRes`.
* `set_param_sim.m` sets the parameters of the simulation for PhOSc model.
* `set_param_sim_sp.m` sets the parameters of the simulation for PhOSc model for comparison purposes. Not used yet.
* `set_param_fit_phosc.m` set parameters of the fitting.
* `gen_stPhOsc_dataset.m` generates syntetic data sets of the simulation of  the coupled stochastic phase oscillators  (PhOsc model). Calls `simSt2PhOsc.m`

### Demo1 related dataset files.

* `stPhOsc_dataset_R1.mat` the output of the simulation.
* `dcm_nsig1_setn1_stPhOsc_dataset_R1.mat` the results of the fitting in Demo1. Testing version. Deleted. 
* `dcm_nsig4_setn1_stPhOsc_dataset_R1.mat` the results of the fitting in Demo1. The final version.
* `DCM_PO.mat` the output that generated when the fitting runs.

### Demo2 scripts
* `Demo2_2JR_NMM.m` the man demo simulation file. Calls `initSimSt2JRparam.m`, `simSt2JRset.m`, `set_param_fit_JR_NMM`, and `test_datasets` in `/TestDataset`, `fFit_eDCM_PC` in `../Fitting`, `plot_res_edcm_pc` in `/PlotRes`.
Moved into `Demos/Demo2`.
* `initSimSt2JRparam.m` the script to initialize the parameters of the J&R system. Moved into `Demos/Demo2`.
* `simSt2JRset.m` this function generates the set of trials of the JR neural mass model. Calls `simJR2alltrial3`.
Moved into `Demos/Demo2`.
* `set_param_fit_JR_NMM.m` sets the parameters for fitting. Moved into `Demos/Demo2`.

### Demo2 related dataset files
* `outSimSt2JR_set_1[_t].mat` and `outSimSt2JR_set_2_t.mat` output file of the demo 2. Testing versions. Deleted.
* `outSimSt2JR_set_2.mat` the output file of the demo 2. The final one. Moved into `Demos/Demo2`.
* `dcm_nsig[1:4]_setn1_outSimSt2JR_set_1.mat` different tries of fitting outputs. with $N_{sig} = [1:4]$. Also this files  `dcm_nsig2_setn1_outSimSt2JR_set_1_t.mat` `dcm_nsig2_setn1_outSimSt2JR_set_2_t.mat`
All deleted.
* `dcm_nsig2_setn1_outSimSt2JR_set_2.mat` the results of the fitting, the final version. Moved into `Demos/Demo2`.
* `DCM_PO.mat` temporal file generated everytime when the fitting runs.

## Scripts for plotting

### Original Scripts
 * `\PlotRes\plot_res_dcm_phosc_xfreq.m`  plots the results for NMM. Calls `calcTransf2`, `calcAppxRho`, `calcRho`, `invcoef` (may be different one!), `distrPPhase` (Should be changed to `distrPPhase3`), `derivs2` (should be changed to `derivs` in TestDataset), `transfProto2Phase`, `calcCouplFunij_anm` (should be changed to `calcQphij` in ModComp), `calcIntRhoSigma2` (commented used `calcTransf2` instead).  
 * `\PlotRes\plot_res_dcm_po.m`  plots only the results of the fitting as surfaces for coupled stochastic phase oscillators. Calls `calcQphij`.
 * `\PlotRes\plotCoefSets_comp.m` plots the comparison graphs for set of data sets of coupled stochastic phase oscillators. Calls no additional functions.

### New plotting Scripts
* `\PlotRes\plot_comp_res_edcm_pc.m` plots the results of the fitting with eDCM PC with the true parameter values used during the synthetic data simulation.  Calls no extra functions.
* `\PlotRes\plot_comp_sim_edcm_pc.m` compares the results of the fitting with edcm pc with the known true parameters of the simulation. Calls `calcTransf2`.
* `\PlotRes\plot_res_edcm_pc.m` plots the results of the fitting with eDCM PC. Calls `calcTransf2`, `invcoef`, `derivs`, `transfv`, `distrPPhase3`, `calcQphij`.



##Additional functions that are used all over the package.

In the modelling components folder `ModComp`:
* `approxSigma` finds the approxiamtion of the `sigma` function from the data set. It uses the weight function `eta` to manage a non-full period trials.
* `invcoef` calculates the Fourier coefficients of the inverse function of a function given by its Fourier coefficients `a` and `b`.
* `transf(x,a,b)` transforms x to y using the Fourier coefficients `a` and `b` of the transformation function (integral of the `rho` or `sigma` functions). `a` and `b` are the cosine and the sine coefficients of the size of `[Nr x Ns]`.
* `transfv(x,a,b)` transforms a vector x to vector y using the Fourier coefficients `a` and `b` of the transformation function (integral of the rho or the sigma functions). `x` is a matrix of phase values of the size `Nt x Nr`. `Nt` is the number of time points. `Nr` the number of regions.

In the folder with the coupled oscillators simulation scripts `SimPhOsc`:
* `calcQphij(a,b,c,d,psii,psij)` calculates the coupling function for two coupled phase oscillators of two regions. The coefficients `a,b,c,` and `d` are of the size `Nnm x Nnm`. !!! Moved to `ModComp`.

In the folder of fitting scripts `Fitting`:
* `derivs(DCM)` finds the derivatives of the observable phases as a function of a uniform distributed phase (time) using the data set (observable/proto phase) stored in DCM structure. Returns the observables, their derivatives and the uniform distributed phase.

In the testing data folder `TestDataset`:
* `calcTransf2(a,b,N)` calculates a transformation function (either sigma or rho) using Fourier coefficients a and b. N is the number of points in the uniformly distributed phase uphi. `a` and `b` are the Fourier coeffieint for cosine and sine of size `Nr x Ntr`, where `Ntr` is the number of Fourier terms and `Nr` is the number of regions.
* `distrPPhase3(y,Nbins)` finds the distribution of the input data (proto phases) for a given dataset `y`. The distribution will be
calculated using weighting (`eta` function) and normalization (the mean is 1).
* `derivs2(y,pst)` finds the derivatives of the phases in dataset (proto phase) stored in `y` structure as a function of a uniform distributed phase (time). This function also returns the mean frequency.

In the folder of plotting scripts `PlotRes`:
* `calcAppxRho(DCM,N)` calculates the approximation of the inverse transformation `rho` for uniformly distributed phase (phi) using coefficients stored in DCM structure, i.e. using the prior values of the coefficients stored in DCM structure. ==Compare with `calcTransf2` in `TestDataset`.==
* `calcCouplFunij_anm(DCM,N,i,j)` calculates the coupling function `Q` between two regions `i` and `j` (`j` is coupled to `i`). The function calcualtes `Q` for uniformly distributed phases `(phii,phi_j)` using coefficients DCM.Ep.anm, DCM.Ep.bnm, DCM.Ep.cnm, and DCM.Ep.dnm, the final fitted values. ==Compare with `calcQphij` in 'SimPhOsc'.==
* `calcSigma_sssc(DCM,N)` calculates the approximation of the forward transformation `sigma` for uniformly distributed phase (`phi`) using coefficients stored in DCM structure, i.e. using the prior values of the coefficients stored in DCM structure.
* `derivs2(y,pst)` finds the derivatives of the phases in dataset (proto phase) stored in `y` structure as a function of a uniform distributed phase (time). This function also returns the mean frequency. ==Compare with the same function in `TestDataset`.==
* `distrPPhase(DCM,N)` finds the distribution of the input data (proto phases) for given dataset stored in DCM structure. The function calculates the distribution using weighting (`eta` function) and normalization (the mean is 1). ==Compare with `distrPPhase3` form `TestDataset`.== 'distrPPhase3' is more advanced and correct script. Change scripts to work only with `distrPPhase3`.
* `invcoef(a,b,N)` calculates the Fourier coefficients of the inverse function of the function given by its Fourier coefficients `a` anb `b`. -==Compare with the one from `ModComp`.==
* `transfProto2Phase(DCM,theta)` transforms the protophase to phase using coefficients stored in DCM structure (sc and ss), i.e. the prior values of the coefficients. ==Compare with `transf` function in `ModComp`.==
