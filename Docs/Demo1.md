# Demonstration script 1: unidirectionally coupled phase oscillators

The aim of this demonstration script is to explain how the eDCM PC works using a simple model of coupled phase oscillators. To do that the script produces synthetic signals from a system of coupled phase oscillators with known parameters and then reproduces the coupling parameters and the transformation functions from the synthetic signals (observable phases) using eDCM PC.

## The model of coupled phase oscillators

We consider a simple model of coupled phase oscillators, where only one (1st) oscillator is coupled to another (2nd) oscillator. The following system of equations describes the model:
```math
  \left\{
    \begin{array}{l}
      \dot{\varphi}_1 = \omega_1 + \eta_1(t), \\
      \dot{\varphi}_2 = \omega_2 + q_{2,1}(\varphi_1,\varphi_2) + \eta_2(t).
    \end{array}
  \right.
```
The functions $`\eta_1(t)`$ and $`\eta_2(t)`$ are normally distributed noises, and the coupling function is defined as
```math
\begin{array}{l}
q_{i,j}(\varphi_1,\varphi_2) = &a_i \cos(\varphi_i)\cos(\varphi_j) + b_i \cos(\varphi_i)\sin(\varphi_j) + \\
&c_i \sin(\varphi_i)\cos(\varphi_j) + d_i \sin(\varphi_i)\sin(\varphi_j).
\end{array}
```
Here we assume that the coefficients of $`q_{1,2}`$ are zero, i.e. $`a_1=b_1=c_1=d_1=0`$.

We add an artificial distortion/transformation to the phases to imitate the real case when the phase is growing non-linearly even in the absence of the coupling. Thereby we aim at testing how eDCM PC reconstructs the coupling at the presence of the distortion. The transformation functions are defined in the following form:
```math
\rho(\varphi_i) = 1 + \sum_{k=1}^{N_{\rho}} \left[\alpha_i^{(k)}\cos(n\varphi_i) +
\beta_i^{(k)} \sin(n\varphi_i)\right], i=1,2.
```
For simplicity we choose $`N_\rho=1`$, thus we have two parameters $`\alpha_i`$ and $`\beta_i`$ for each oscillator.

## Simulation of the synthetic signals
The function `set_param_sim(casenum,istransf)` sets the parameters of the simulation and returns them in the structure `sim`. The function `gen_stPhOsc_dataset(sim)` using the parameters stored in the structure `sim` integrates the system using the Runge Kutta method for stochastic differential equations. The integration itself is carried out in the intrinsic function `simSt2PhOsc(sim)`.

The results of the simulation - the synthetic signals - are stored in the `.m` file given in `sim.foutname`.


## Testing the synthetic signals
The testing of the signals is not necessary to fit the model parameters using the eDCM PC package. However, it is helpful to set the fitting parameters by looking at the distribution of the observable phases and for the calculation of the coefficients of the 2D Fourier series from the signals.

The function `test_datasets.m` in the `/TestDataset` folder performs the tests and plots the corresponding figures. Here is the output of the function.

__Test 1__. Looking at the signal phases - observable phases.

<div align="center">
 <img src="Figs/Demo1_signals.png" alt="The observable phases in all trials plotted using the modular function" width="">

 <em>Fig. 1. The observable phases in all trials plotted using the modular function.</em>
</div>

__Test 2__. Comparing the distribution of the observable phases $`\theta`$ and the initial approximation of the inverse transformation function $`\sigma(\theta)`$. The $`\sigma(\theta)`$ function should envelope the distribution of the observable phases. If an oscillators has no input coupling, then

```math
\begin{array}{c}
 \frac{d\varphi}{dt} = \frac{d\varphi}{d\theta}\frac{d\theta}{dt} =  \sigma(\theta)\frac{d\theta}{dt}, \\
 \omega = \sigma(\theta)\frac{d\theta}{dt}, \\
 \frac{dt}{d\theta} = \sigma(\theta)/\omega.
 \end{array}
```
However, the initial approximation of the $`\sigma(\theta)`$ function does not always coincides with the mean of the reciprocal derivatives of the observable phases $`\theta`$.

<div align="center">
 <img src="Figs/Demo1_distr_sigma.png" alt="The distribution of the observable phases $`\theta`$ and the inverse transformation function $`\sigma`$ that envelopes the distribution." width="500">

 <em>Fig. 2. Upper panels: The distribution of the observable phases $`\theta`$ and the inverse transformation function $`\sigma(\theta)`$ that envelopes the distribution. <br>
Lower panels: The reciprocal derivative of the observable phases compared with the $`\sigma(\theta)`$ function corrected with the frequency of the oscillator.</em>
</div>

This test shows us how good the initial approximation of the $`\sigma(\theta)`$ function is.

__Test 3__. An elucidative way to test the properties of the analyzed system is to calculate the coefficients of the 2D Fourier series from the signals. For that the program interpolates the derivatives of the observable phases by a surface using a nearest neighbor algorithm (the MATLAB's `fit()` function with the argument `deffittype = 'nearestinterp'`).

<div align="center">
 <img src="Figs/Demo1_fit_surf.png" alt="Interpolation of the derivative of the observable phases by surfaces." width="500">

 <em>Fig. 3. Interpolation of the derivative of the observable phases by surfaces.</em>
</div>

Thereafter we calculate the coefficients of the 2D Fourier series. The results are presented as the norm of the complex Fourier coefficients $`F_{nm}`$ and as the real valued Fourier coefficients $`a,b,c`$ and $`d`$, which are related to $`F_{nm}`$ as follows

```math
\begin{array}{l}
 a_{n,m} & = \left(F_{n,m} + F_{-n,m} + F_{n,-m} + F_{-n,-m}\right), \\
 b_{n,m} & = i\left(-F_{n,m} + F_{-n,m} - F_{n,-m} + F_{-n,-m}\right), \\
 c_{n,m} & = i\left(-F_{n,m} - F_{-n,m} + F_{n,-m} + F_{-n,-m}\right), \\
 d_{n,m} & = \left(-F_{n,m} + F_{-n,m} + F_{n,-m} - F_{-n,-m}\right).
\end{array}
```

<div align="center">
 <img src="Figs/Demo1_Fnm.png" alt="The norm of the Fourier coefficients of the interpolated surfaces" width="500">

 <em>Fig. 4. The norm of the Fourier coefficients of the interpolated surfaces.</em>
</div>

<div align="center">
 <img src="Figs/Demo1_abcd1.png" alt="The real valued Fourier coefficients of the interpolated surfaces of the 1st oscillator" width="400">

 <em>Fig. 5. The real valued Fourier coefficients of the interpolated surfaces of the 1st oscillator.</em>
</div>

<div align="center">
 <img src="Figs/Demo1_abcd2.png" alt="The real valued Fourier coefficients of the interpolated surfaces of the 2nd oscillator" width="400">

 <em>Fig. 6. The real valued Fourier coefficients of the interpolated surfaces of the 2nd oscillator.</em>
</div>

Since the 1st oscillator in our example has no input coupling the Fourier coefficients are everywhere almost zero except the ones with $`m=0`$. Moreover, the coefficients with $`m=0`$ are related to the non-linear growing of the phase, i.e. to the transformation function $`\sigma(\theta)`$ and $`\rho(\varphi)`$.

The 2nd oscillator has an input coupling from the 1st oscillator, therefore some Fourier coefficients with $`n\neq 0`$ and $`m \neq 0`$ are non zero. Here again $`n=0`$ relates to the transformation function of the 2nd oscillator. Note, that the coefficients with $`m=0`$ are close to zero, which is an important condition for successful reconstruction of the coupling functions and the transformation function. However, if the coupling is strong, then the Fourier coefficients lying along the diagonal $`m+n=\textrm{const}`$ become non-zero, which indicates synchronization of the oscillators and makes the reconstruction procedure less effective.

As a result of this test we can choose the number of terms of the Fourier series for reconstruction of the coupling functions as $`N_{\rho} = 1`$ and $`N_{q}=1`$. This choice should cover the main non-zero Fourier coefficients.

The DAMOCO package implements a similar method of analyzing the Fourier coefficients of the observable phases without interpolation of the surfaces. For details, please, refer to the documentation of the DAMOCO package and the related articles (bitte Referenzen noch einfügen).  

## Reconstruction of the coupling and the transformation functions

The reconstruction of the coupling and the transformation functions are performed by calling the function `spm_dcm_po(DCM)` with the predefined `DCM` structure, which contains the initial parameters of the system (priors) and the reconstruction parameters. To set the reconstruction parameters the function `set_param_fit_phosc.m` is called. The important parameters are

```
  fitpar.Nr = 2;          % number of regions
  fitpar.A = [0 1; 1 0];  % the connectivity matrix
  fitpar.Nsig = 4;        % the number of Fourier terms for the inverse transformation
  fitpar.Nrho = 1;        % the number of Fourier terms for the forward transformation
  fitpar.Nq = 1;          % the number of Fourier terms for the coupling function
```

Further, the function `fFit_eDCM_PC(fitpar)` from the `/Fitting` folder is called with the fitting parameters as an argument to set the fields of the `DCM` structure and call the `spm_dcm_po(DCM)` function.

## The reconstruction results

The function `plot_res_edcm_pc` plots the results as shown below.

Similar to the testing figures (Fig. 2) we compare: (1) the distribution of the observable phase, the reciprocal derivative of the observable phase, and the initially approximated and fitted transformation function $`\Sigma`$; (2) the derivatives of the observable phases and the initial approximation and the final fitted transformation function $`\rho`$. Fig. 7 shows that the final fitted values of the transformation functions coincide with the average of the data points.

<div align="center">
 <img src="Figs/Demo1_res_distr_sig_rho.png" alt="Results: the distribution of the observable phases, the reciprocal derivative of the observable phases, the approximated and fitted transformation functions sigma and rho." width="400">

 <em>Fig. 7. Results: the distribution of the observable phases, the reciprocal derivatives of the observable phases, the approximated and fitted transformation functions $`\sigma(\theta)`$ and $`\rho(\varphi)`$.</em>
</div>

The function `plot_res_edcm_pc` also plots the forward and inverse transformation functions separately as shown in Figs. 8 and 9.

<div align="center">
 <img src="Figs/Demo1_res_rho.png" alt="Results: the reconstructed rho functions" width="400">

 <em>Fig. 8. Results: the reconstructed $`\rho(\varphi)`$ functions.</em>
</div>

<div align="center">
 <img src="Figs/Demo1_res_sigma.png" alt="Results: the reconstructed sigma functions" width="400">

 <em>Fig. 9. Results: the reconstructed $`\sigma(\theta)`$ functions.</em>
</div>

Another way to analyze the results is to compare the data points with the reconstructed coupling functions represented as surfaces. Since

```math
 \frac{d\varphi_i}{dt} = \omega_i + q_{i,j}(\varphi_i,\varphi_j),
```

and

```math
  \frac{d\theta_i}{dt} \sigma(\theta) = \frac{d\varphi_i}{dt}, \varphi = \Phi(\theta)
```

we can compare $`q_{i,j}(\varphi_i,\varphi_j)`$ with $`\frac{d\varphi_i}{dt} - \omega_i`$, where the derivative of the theoretical phase is recalculated using the inverse transformation function. Fig. 10 shows the result of the comparison.

<div align="center">
 <img src="Figs/Demo1_res_surf_th_phase.png" alt="Results: the reconstructed coupling functions represented as surfaces compared with the theoretical phases recalculated using sigma function" width="600">

 <em>Fig. 10. Results: The reconstructed coupling functions represented as surfaces compared with the theoretical phases. The theoretical phases are recalculated from the observable phases using the $`\sigma(\theta)`$ function.</em>
</div>

On the other hand, we can compare the derivatives of the observable (measured) phases with the "observable" surface - the surface of the derivatives of the observable phases generated by the reconstructed system. Note, that in this case the derivative of the observable (measured) phases should be represented as a function of the theoretical phase $`\varphi`$ using the inverse transformation function.

```math
 \frac{d\theta_i}{dt}(\Theta(\varphi_i)) = \rho(\varphi_i)\frac{d\varphi_i}{dt} = \rho(\varphi_i)(\omega_i + q_{i,j}(\varphi_i,\varphi_j)).
```

The result is shown in Fig. 11.

<div align="center">
 <img src="Figs/Demo1_res_surf_obs_phase.png" alt="Results: comparing the observable phases with the surfaces of the reconstructed coupling functions scaled with rho function" width="600">

 <em>Fig. 11. Results: comparing the observable phases with the surfaces of the reconstructed coupling functions scaled with the $`\rho(\varphi)`$ function.</em>
</div>

Finally we compare the parameter values used for generation of the synthetic data set with the ones found by fitting with eDCM PC. The results are shown in Fig. 12.

<div align="center">
 <img src="Figs/Demo1_comp_rcrs_abcd.png" alt="Comparison of the fitted parameter values with the true ones." width="600">

 <em>Fig. 12. Comparison of the resulting parameter values (red stars) with the true parameter values (black circles).</em>
</div>

[Previous Chapter: Usage](Usage.md) | [Up](##the-model-of-coupled-phase-oscillators) | [Next Chapter: Demonstration script 2](Demo2.md)
