# The coupled Jansen and Rit model

The Jansen-Rit neural mass model simulates the electroencephalographic signals of the cortical column [^JansenRit1995]. Every region of the model consists of three blocks as shown in Fig. 1: two excitatory $`h_e(t)`$ and one inhibitory $`h_i(t)`$ blocks. These block is refered in [Jansen and Rit, 1995] as postsyaptic potential blocks (PSP), and introduced with two differential equations

```math
\begin{array}{ll}
 \dot{y} & = z, \\
 \dot{z} & = A a x(t) - 2 a z - a^2 y.
\end{array}
```

In other words, the PSP block is a linear transformation with a response to an impulse
```math
h_e(t) = A a t e^{-a t}, t\geq 0, \text{ and } h_i(t) = B b t e^{-b t}, t\geq 0.
```

![Jansen and Rit model](Figs/Jansen_Rit_Model.png)

*Fig. 1. The Janden and Rit neural mass model, one cortical region (adapted form Jansen and Rit, (1995)[^JansenRit1995].*

In the scheme the `Sigm` block represents the sigmoid function
```math
S(x) = \frac{2 e_0}{1 + e^{r(V_0 - x)}}.
```

Two cortical colums connected with each other have noisy external inputs $`p_i(t)`$ and the internal delay blocks with coupling coefficients $`K_i`$ and the delay constant $`a_d`$, as shown in Fig. 2.

![Two coupled cortical columns](Figs/TwoCoupledJRmodels.png)

*Fig.2. Two coupled cortical columns of Jansen and Rit model. (adapted from Jansen and Rit, (1995)[^JansenRit1995].*

Thus, the system of equations for two coupled the Jansen and Rit model reads (one region $`i`$)
```math
\begin{array}{ll}
 \dot{y}^{(0)}_i & = z^{(0)}_i, \\
 \dot{z}^{(0)}_i & = A_i a_i S(y^{(1)}_i-y^{(2)}_i) - 2 a_i
			z^{(0)}_i - a_i^2 y^{(0)}_i, \\
 \dot{y}^{(1)}_i & = z^{(1)}_i, \\
 \dot{z}^{(1)}_i & = A_i a_i \left(p_i(t) + C_2 S(C_1 y^{(0)}_i) +
\sum_{j=1,j\neq i}^{N_s} K_j y^{(d)}_j\right) \\
		  & - 2 a_i z^{(1)}_i - a_i^2 y^{(1)}_i, \\
 \dot{y}^{(2)}_i & = z^{(2)}_i, \\
 \dot{z}^{(2)}_i & = B_i b_i \left(C_4 S(C_3 y^{(0)}_i)\right) -
			2 b_i z^{(2)}_i - b_i^2 y^{(2)}_i, \\
% delay block
 \dot{y}^{(d)}_i & = z^{(d)}_i, \\
 \dot{z}^{(d)}_i & = A_i a^{(d)} S(y^{(1)}_i-y^{(2)}_i) - 2 a^{(d)}
			z^{(d)}_i - (a^{(d)})^2 y^{(d)}_i.
\end{array}
```

The output signals of the regions are $`Y_1 = y_1^{(1)}-y_1^{(2)}`$ and $`Y_2 = y_2^{(1)}-y_2^{(2)}`$.

Following [^JansenRit1995] we use the specific relation between the parameters:
```math
C_1 = C, C_2 = 0.8C, C_3 = C_4 = 0.25C,
```
where $`C = 135`$.

For simulation we used the following parameter values (see [Yeldesbay et al.,2019]) $`a^{(d)} = 30`$, $`A_1 = 1.625`$, $`a_1 = 50`$, $`B_1 = 29.333`$, $`b_1 = 66.667`$, $`A_2 = 3.250`$, $`a_2 = 100`$, $`B_2 = 44`$, $`b_2 = 100`$,  $`r = 0.56`$, $`V_0 = 6.0`$,  $`e_0 = 2.5`$. As noisy inputs to every region we used $`p_i(t) = \langle p_1(t)\rangle+\tilde{p_1}\eta(t)`$, where $`\eta \sim N(0,s^2_{p_i})`$ is a normally distributed noise with the amplitudes $`\tilde{p_1}=\tilde{p_2}=0.25`$ and the standard deviations $`s_{p_1}=s_{p_2}=1.0`$. Without coupling, the inputs have the means $`\langle p_1(t)\rangle=220`$, $`\langle p_2(t)\rangle=220`$.

With these parameter values the regions generate oscillations with the mean frequencies $`f_1 = 9.233 \text{ Hz}`$ and $`f_2 = 16.160 \text{ Hz}`$.

## References

[^JansenRit1995]: Jansen, B. H., & Rit, V. G. (1995). Electroencephalogram and visual evoked potential generation in a mathematical model of coupled cortical columns. Biological Cybernetics, 73(4), 357–366. https://doi.org/10.1007/BF00199471
