function [yy,tt] = simSt2PhOsc(sim)
%%% This function produces trials (integrates) of the stochastic coupled
%%% phase oscillators model for given values of the system parameters 
%%% and of the integration parameters storred in sim structure.
%%% For the integration the function uses the Runge Kutta method for SDE, 
%%% with modified model as in David et al 2003 2004.
%%% The function integrates only true phases with noise and then applies 
%%% non-linear transfomation (distortion) thereafter.  
%%%
%%%  Input parameters:
%%%     sim is a structure of the integration and of the
%%%     system parameter values, see details in run_gen_stpo_dataset.m.
%%%  Ouput:
%%%     - out is a structure:
%%%         tt - time      
%%%         yy - array of trials.
%%%     Both variables are cut from the end of the transent time till the end of the
%%%     simulation.
%%%
%%% The program is written on the base of the script JRSim2_RK2noise.m and
%%% simJR2trialdcm.m. 
%%%
%%% -----------------------------------------------------------------------
%%% AY 24.04.2018
%%% -----------------------------------------------------------------------
%%% Last modification: AY 21.02.2019 Edited comments

%% Run the script with the initiation of the parameters. 
% inputfilename; % Commented, not used.

dt = sim.dt; % time step 
N = round(sim.T/sim.dt); % number of time intervals
% T is the total time of integration

Nr = sim.Nr; % number of  phase oscillators (regions)
Nt = sim.Ntrials; % the number of trials

% The coupling coefficients of the 1st oscillator. Only 2nd oscillator is connected.
a1 = sim.anm(1,2);
b1 = sim.bnm(1,2);
c1 = sim.cnm(1,2);
d1 = sim.dnm(1,2);
% The coupling coefficients of the 2nd oscillator. Only 1st oscillator is connected.
a2 = sim.anm(2,1);
b2 = sim.bnm(2,1);
c2 = sim.cnm(2,1);
d2 = sim.dnm(2,1);
% The coefficients of the transformation function (distortion)
rc = sim.rc;
rs = sim.rs;

f= sim.f; % the natural frequencies of the oscillators 

% The parameters of the noise
eta = sim.noise.eta; % the noise amplitude
dev = sim.noise.dev; % the standart deviation of the noise 

yy{Nt} = [];

addpath('..\ModComp'); % to find the function transfv.m -- the transformation function

%% loop by trials 
for itr = 1:Nt
    %% set inital values
    % initial vectors
    phi0=2*pi*rand(1,Nr); % uniform distributed between 0 and 2pi

    %% alocate space for the outputs 
    Y = zeros(N,Nr);
    T = zeros(N,1);

    %% integration of the true phases
    Yt = phi0; 
    t = 0.0;
    for j = 1:N 
        % the weak second order Runge-Kutta method for SDE
        t = t + dt;
        Wincv = sqrt(dt)*normrnd(0.0,dev,[1,Nr]); % defined for every region

        af = fx(t,Yt);
        bf = gx(t,Yt);
        u = Yt + af*dt;  
        up = u + bf*sqrt(dt);  
        um = u - bf*sqrt(dt);  
        u = u + bf.*Wincv;

        au = fx(t,u);
        bup = gx(t,up);
        bum = gx(t,um);

        Yt = Yt + 0.5*(au+af)*dt + 0.25*(2*bf+bup+bum).*Wincv + ...
            0.25*(bup-bum).*(Wincv.*Wincv - dt)/sqrt(dt); 

        Y(j,:) = Yt;
        T(j) = t;
    end

     % apply the transformation function for the whole time series
    Y = transfv(Y,rc,rs);
    
    yy{itr} = Y;
    tt = T;
end

    %% definition of the functions
    function out = fx(t,y)
        % only Nr=2 case is considered
        out(1) = f(1) + calcQphij(a1,b1,c1,d1,y(1),y(2));
        out(2) = f(2) + calcQphij(a2,b2,c2,d2,y(2),y(1));
    end

    function out = gx(t,y)
        % the case of Nr=2 and the same noise level are considered
        out(1) = eta;
        out(2) = eta;
    end
end
