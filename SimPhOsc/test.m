%%% this script is a minimal code that tests the extended Dynamic Causal 
%%% Modelling for phase coupling (eDCM PC). 
%%% Input is the syntetic signals generated from the system of two coupled
%%% oscillators without noise. 
%%% 
%%% AY 2021.06.18
clear; 

DCM = [];           % allocate space for the DCM structure

% load test dataset
load('test_dataset.mat'); % yy, tt, and sim are loaded

DCM.xY.y = yy;       % yy{i} is the Nx2 matrix; N is the number of time samples
DCM.xY.dt = sim.dt; % the time step
DCM.xY.pst = tt;    % the peristimulus time, array of the size Nx1.
                    % if uniform distributed time then define as (0:N-1)'*dt.

% Find the derivatives of the observable phases and the mean frequency.
[dthetas,uphis,thetas,mFreq] = derivs(DCM.xY.y,DCM.xY.pst);

M  = [];            % allocate space for the M structure (the model structure).
M.freq = mFreq';    % define the mean frequencies, same as mean(dthetas)'.
M.isslowphase = 0;  % to reconstruct with slow phase. Not used any more, set to 0.

% Define the range of the frequency band. Can be defined manually.
M.fb = max(abs(dthetas - repmat(mean(dthetas),[size(dthetas,1),1])))';

DCM.options.Fdcm =repmat(M.freq,1,2) + [-M.fb M.fb];

DCM.options.istransf = 0; % an option to work with theoretical phase. Not used any more, set to 0.

% Define the parameters of the coupling coefficients
M.Nsig = 1;         % The order of Fourier terms of the sigma function
M.Nrho = 1;         % The order of Fourier terms of the rho function
M.Nq = 1;           % The order of Fourier terms for the coupling function q_ij

% Define the coupling structure in the DCM structure
DCM.A = [0,1; 1,0]; % the coupling matrix; [0 1; 1 0] is bidirectional coupling
DCM.B{1} = zeros(size(DCM.A)); % inter-trial effect matrix, set if appropriate

% Define the initial phases from all trials
for n = 1:sim.Ntrials
    phi0 = DCM.xY.y{n}(1,:);
    trial{n}.x = phi0';
end

% Define further the elements the M structure
M.trial = trial;      
M.x = zeros(sim.Nr,1);
N = size(tt,1);
M.ns = N;             % number of time samples

DCM.M = M;

% Define the elements of the U structure
U.u = zeros(N,1);
U.dt = sim.dt;
U.tims = (0:N-1)'*sim.dt;
U.X = zeros(sim.Ntrials,1);

DCM.xU = U;
DCM.xU.oldX = U.X;

% start SPM
addpath('C:/MWork/spm12/');
spm eeg;

% Call the fitting function of eDCM PC
DCM = spm_dcm_po(DCM);

% compare results
orgParVec = [sim.f(1),sim.f(2),...
             sim.anm(1,2),sim.bnm(1,2),sim.cnm(1,2),sim.dnm(1,2), ...
             sim.anm(2,1),sim.bnm(2,1),sim.cnm(2,1),sim.dnm(2,1), ...
             sim.rc(1),sim.rc(2),sim.rs(1),sim.rs(2)];
res = DCM.Ep;
resParVec = [DCM.M.freq(1)+res.df(1),DCM.M.freq(2)+res.df(2),...
             res.anm(1,2),res.bnm(1,2),res.cnm(1,2),res.dnm(1,2), ...
             res.anm(2,1),res.bnm(2,1),res.cnm(2,1),res.dnm(2,1), ...
             res.rc(1),res.rc(2),res.rs(1),res.rs(2)];
         
figure;
plot(orgParVec,'ob');
hold on;
plot(resParVec,'*r');
hold off;
set(gca,'XTick',[1:14]);
set(gca,'XTickLabel',{'f_1','f_2',...
    'a_{12}','b_{12}','c_{12}','d_{12}',...
    'a_{21}','b_{21}','c_{21}','d_{21}',...
    '\alpha_1','\alpha_2','\beta_1','\beta_2'});