%%% this scripts generates the dataset for testing script test.m
%%% AY 2021.06.18
clear;

addpath('../SimPhOsc/');
%% Definition of the integration parameters
sim.dt = 0.01;      % the time step
sim.tr = 10;        % to take every tr point. (!) dt for phase is also corrected. 
sim.T = 5;          % simulation time
sim.Nr = 2;         % number of regions
sim.Ntrials = 15;   % number of trials
% sim.Nnm = 1;        % Number of terms in Fourier series. Only the 1st terms.

% define the coupling parameters
sim.anm = zeros(sim.Nr,sim.Nr);
sim.bnm = zeros(sim.Nr,sim.Nr);
sim.cnm = zeros(sim.Nr,sim.Nr);
sim.dnm = zeros(sim.Nr,sim.Nr);
sim.anm(1,2) = 0.0;% a1
sim.bnm(1,2) = 0.1;   % b1
sim.cnm(1,2) = -0.1;   % c1
sim.dnm(1,2) = 0.0;   % d1

sim.anm(2,1) = 0.1;% a1
sim.bnm(2,1) = 0.0;   % b1
sim.cnm(2,1) = 0.0;   % c1
sim.dnm(2,1) = 0.1;   % d1

% define coefficients of the forward transformation
sim.rs = [0.1;
          0.15];
sim.rc = [0.0;
          0.0]; 
      
sim.f = [1; 1]; % frequency of the oscillators.

% set noise to zero
sim.noise.eta = 0.0;
sim.noise.dev = 0.0;

% generation of the trials with the unwrapped phases
[yy,tt] = simSt2PhOsc(sim);

% thin out the data to sim.tr 
for itr=1:sim.Ntrials
    tmp = yy{itr};
    yy{itr} = tmp(1:sim.tr:end,:);
end
tt = tt(1:sim.tr:end); 
sim.dt = sim.dt*sim.tr;

S{1}.xx=yy; %% to fit the format of the output
S{1}.t = tt;

save('test_dataset.mat','yy','tt','sim','S');