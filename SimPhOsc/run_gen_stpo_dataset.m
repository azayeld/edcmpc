%%% This script generates syntetic data sets of the simulation of 
%%% the coupled stochastic phase oscillators with the real-valued Fourier 
%%% coefficients. The script can produce the syntetic data sets with or 
%%% without the application of the transformation function (the forward 
%%% transformation function rho). 
%%% The data set is generated from a model of two coupled oscillators 
%%% with different frequencies 
%%% dphi1/dt = omg1+sum_nm (anm(1,2)*cos(phi1)*cos(phi2)+bnm(1,2)*cos(phi1)*sin(phi2)
%%%                +cnm(1,2)*sin(phi1)*cos(phi2)+dnm(1,2)*sin(phi1)*sin(phi2))+ eta1(t),
%%% dphi2/dt = omg2+sum_nm (anm(2,1)*cos(phi2)*cos(phi1)+bnm(2,1)*cos(phi2)*sin(phi1)
%%%                +cnm(2,1)*sin(phi2)*cos(phi1)+dnm(2,1)*sin(phi2)*sin(phi1))+ eta2(t),
%%% where, eta1(t) and eta2(t) are normally distributed noises. 
%%% The transformation function is applied to the generted phases (phi1) using 
%%% theta1 = phi1 + \sum_l 1/l (sc*sin(l phi1) + ss*(1-cos(l phi1))).
%%%
%%% The data set is produced by the external funciton simSt2PhOsc.m.
%%% The results are collected by trials and by sets of data sets.
%%% The format of the output of the data set is sutable for the programs
%%% plot_rhs_phase_dataset.m and for test_dataset.m.
%%%
%%% -------------------------------------------------------
%%% This script is a modification/combination of run_gen_po_dataset.m, 
%%% test_gen_phase_data_real.m and 
%%% test_gen_phxfreq_dataset.m, and of run_gen_phosc_xfreq_dataset.m.
%%% In this version of the sctipt produces the data set without plotting the results.
%%% The plotting part is shifted to the script test_dataset.m.
%%% Some parts of the script is written on the base of the script gen_finger.m from
%%% SPM DCM toolbox of Wellcome Trust Centre for Neuroimaging
%%% written by Will Penny.
%%% 
%%% AY 24.04.2018
%%% --------------------------
%%% Last modification: AY 21.02.2019 Editted comments.
clear;

%% define the output file name
% The definition of only the root of the filename; the index is defined later, 
% when the connection type is specified. 
foutname = 'stpo_dataset5_test_compN15'; 

% gen_data = 1; % a flag to control generation of the data. Not used.

%% define the data set properties in the sim structure 
sim.do_plot = 0; 
sim.Nset = 15; % number of sets
sim.Ntrials = 20; % Number of trials in each set
sim.noise.is = 0; % not used.
% sim.noise.eta = 0.075;
% sim.noise.dev = 0.15;
%%% special case: to compare the original and new extension of DCM PC
sim.noise.eta = 0.1;
sim.noise.dev = 0.15;
%%% end of the special case:

sim.Nr = 2; % the number of regions

%%% the parameters of the oscillators - the natural frequencies
% sim.f = [1; 1+0.43]; % 
sim.f = [1; 1]; % 

%%% allocate coefficients
sim.Nnm = 1; % Number of therms in Fourier series. Only the 1st terms.
sim.anm = zeros(sim.Nr,sim.Nr,sim.Nnm,sim.Nnm);
sim.bnm = zeros(sim.Nr,sim.Nr,sim.Nnm,sim.Nnm);
sim.cnm = zeros(sim.Nr,sim.Nr,sim.Nnm,sim.Nnm);
sim.dnm = zeros(sim.Nr,sim.Nr,sim.Nnm,sim.Nnm);

%%% Different connection types. (Uncomment the considered case)
%%% 0. no connection
% sfxout = 0; % default state
% no change 
%%% 1. 1st osc is connected to 2nd osc
% sfxout = 1;
% sim.anm(2,1,1,1) = 0.2;
% sim.bnm(2,1,1,1) = 0.1;
% sim.cnm(2,1,1,1) = -0.17;
% sim.dnm(2,1,1,1) = 0.15;
%%% 2. 2nd osc is connected to 1st osc
% sfxout = 2;
% sim.anm(1,2,1,1) = -0.1;
% sim.bnm(1,2,1,1) = -0.2;
% sim.cnm(1,2,1,1) = 0.19;
% sim.dnm(1,2,1,1) = -0.15;
% % 
% sim.anm(1,2,1,2) = 0.6;
% %%% 3. both osc.s are connected to each other
% sfxout = 3;
% sim.anm(2,1,1,1) = 0.2;
% sim.bnm(2,1,1,1) = 0.1;
% sim.cnm(2,1,1,1) = -0.17;
% sim.dnm(2,1,1,1) = 0.15;
% sim.anm(1,2,1,1) = -0.1;
% sim.bnm(1,2,1,1) = -0.2;
% sim.cnm(1,2,1,1) = 0.19;
% sim.dnm(1,2,1,1) = -0.15;
%%% end connectivity matrix

%%% the special case of connectivivty matrix
%%% for comparison of the original and the extended DCM PC.
sfxout = 1;
a=0.2;
% the case with negative As and positive Ac.
% sim.anm(2,1,1,1) = a;
% sim.bnm(2,1,1,1) = a;
% sim.cnm(2,1,1,1) = -a;
% sim.dnm(2,1,1,1) = a;

% the case 2. 
% sim.anm(2,1,1,1) = -a;
% sim.bnm(2,1,1,1) = a;
% sim.cnm(2,1,1,1) = a;
% sim.dnm(2,1,1,1) = a;

% the case 4. 
sim.anm(2,1,1,1) = 0;
sim.bnm(2,1,1,1) = a;
sim.cnm(2,1,1,1) = -a;
sim.dnm(2,1,1,1) = 0;

%%%% the parameters of the transformation (uncomment the considered case)
%%% without transformation:
% sfxrho = '_noR'; % suffix _noR added when no transformation is applied
% sim.rs = zeros(2,1); % (Nr,Nsig)    
% sim.rc = zeros(2,1); % (Nr,Nsig)

% %%% with transformation:
sfxrho = '_R'; % suffix _R added when the transformation is applied
%%% case 1
% sim.rs = [ 0.2 0.0;
%            0.1 0.0];
% sim.rc = [ 0.15 0.0;
%            0.25 0.0];
%%% case 2       
% sim.rs = [ 0.13, 0.0, 0.0, 0.0;
%            0.1, 0.0, 0.0, 0.0];
% sim.rc = [ 0.1, 0.0, 0.0, 0.0;
%           0.05, 0.0, 0.0, 0.0];       
%%% case 3
sim.rs = [ 0.15;
           0.05];
sim.rc = [ 0.13;
           0.07];

%%% special case for comparison of the original and the extended DCM PC
% sim.rs = [ 0.15;
%            0.1];
% sim.rc = [ 0.1;
%            0.05];       

%%% define the frequency band
% fb = max([max(abs(sim.ss),[],2), max(abs(sim.ss),[],2), [sim.p.a; sim.p.b]],[],2);
fb = 0.5; % fixed value of the frequency band
       
sim.fb = fb;  

%%% the integration parameters
sim.dt = 0.01; % the time step
sim.tr = 5; % to take every tr point. (!) dt for phase is also corrected. 
sim.T = 5; %4; % 50; % simulation time
sim.T1 = 0.0; % the cut off ratio to remove transient time; zero means no cutting
%%% Note! The real time interval of the trial is T-T1.

%% generation of the dataset
% if gen_data == 1 
    S = cell(sim.Nset,1);
    for n = 1:sim.Nset
        [yy,tt] = simSt2PhOsc(sim); % external function that generates the data set
        
        % thin out the data to sim.tr 
        for itr=1:sim.Ntrials
            tmp = yy{itr};
            yy{itr} = tmp(1:sim.tr:end,:);
        end
        tt = tt(1:sim.tr:end);

        S{n}.xx=yy; %% to fit the format of the outout
        S{n}.t = tt;
    end
    sim.dt = sim.tr*sim.dt; % increase the dt tr times.
    save([foutname sfxrho num2str(sfxout) '.mat'],'sim','S');
    %%% !!! turn off this output when using test_jumps.m !!!
    disp(['The results are saved in ' foutname sfxrho num2str(sfxout) '.mat' '.']);
% end
