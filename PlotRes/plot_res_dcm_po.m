%%% Plots the results of  the fitting with extenede DCM PC.
%%% This script is modified to plot the results of the fitting of original 
%%% and nwe extended DCM for phase coupling. 
%%% The script plots the surfaces without calculating the transformation
%%% funcitions sigma and rho. 
%%% The main part is taken from plot_res_dcm_phosc_xfreq.m.
%%% Original script AY 21.06.2018
%%% Last modification AY 21.03.2019: edited comments and changed some
%%% functions to be compatible with new folder structure. Removed
%%% dublicated functions. 
clear; 

%%% the data set with the results of the coupled stochastic phase
%%% oscillators. This data set was generated to compare the resutls of the
%%% original DCM PC and the extended DCM PC. 
origFileNameRoot = 'stpo_dataset4_compN15';

addpath('../ModComp');

%% define parameters: change if necessary
%%% which index
inindex = 1; % always 1 for Stochastic phase oscillators
%%% orig or new
isnew = 1; % 0 orig, 1 new. Set to 1 for plotting the NMM results.
%%% transformed or not
isR = 1; % 0 no transformation, 1 with transformation
%%% the number of sets
iset = 9;

Nmesh = 50; % the size of the mesh and of the uniform distributed phase

%%% fixed limits
% fLim = 14.0; % for NMM
fLim = 0.5; % for stochastic phase oscillators
% fLim = 0.5; % for comparison of orig and new DCM PC

FntSize = 50; % in pixels, for 300dpi it is ~12 pt?

%% define the prefixes 
pfx = '..\SimPhOsc\'; % the folder with the results
addpath(pfx); % to access some functions. 
if isnew
    pfxdcm = [pfx 'dcm_nsig1_setn' num2str(iset) '_'];
else
    pfxdcm =[pfx 'dcm_orig3_nsig1_setn' num2str(iset) '_'];
end

%% define sufixes
if isR
    sfx = ['_R' num2str(inindex) '.mat'];
else
    sfx = ['_noR' num2str(inindex) '.mat'];
end

inputfilename = [pfxdcm origFileNameRoot sfx];

%% Define input file names for other results: 
% inputfilename = ['..\SimNMM\dcm_nsig2_outSimJR2_H_test_onoff_noise_107_' num2str(inindex) '.mat'];
%%% !!! correct other parameters, i.e. isnew = 1; 


%% load data set
load(inputfilename);
disp(['loaded: ' inputfilename]);

%% plot figure two regions together 
hf16 = figure(16);
set(hf16,'Renderer','Painters','Position',[200 200 950 450],'Color',[1 1 1]);

% create mesh 
phi = (0:Nmesh-1)*2*pi/Nmesh;
[phii, phij] = meshgrid(phi,phi);

% define surfaces and borders:
for ii=1:2 % loop by regions, we consider only two regions
        if isnew % if we are plotting eDCM PC results
            % find coefficients
            Nnm=size(DCM.Ep.dnm,3);
            a = reshape(DCM.Ep.anm(ii,mod(ii,2)+1,:,:),[Nnm,Nnm]); % anm is 2D
            b = reshape(DCM.Ep.bnm(ii,mod(ii,2)+1,:,:),[Nnm,Nnm]); % bnm is 2D
            c = reshape(DCM.Ep.cnm(ii,mod(ii,2)+1,:,:),[Nnm,Nnm]); % cnm is 2D
            d = reshape(DCM.Ep.dnm(ii,mod(ii,2)+1,:,:),[Nnm,Nnm]); % dnm is 2D

        else % if we are plotting the results of the original DCM PC
            Nnm = 1;
            As = DCM.Ep.As(ii,mod(ii,2)+1);
            try 
                Ac = DCM.Ep.Ac(ii,mod(ii,2)+1);
            catch
                Ac = 0.0;
            end
            % for original DCM PC the coefficients are defined as
            % a_nn = Ac_nn, b_nn = As_nn, c = -As_nn, d = Ac_nn.
            % the other coefficients are zero
            a = Ac; b = As; c = -As; d = Ac;
        end
        
        Qs{ii} = arrayfun(@(x,y) calcQphij(a,b,c,d,x,y),phii,phij);
        mxQ{ii} = max(max(abs(Qs{ii}))); % maximum deviation without mean
         
end
mxQs = max([mxQ{1},mxQ{2}]);

%%%-- 21.06.2018 AY: plot with fixed limits 
for ii=1:2 % loop by regions, we consider only two regions
%         subplot(1,2,ii);
        subplot('Position',[0.44*(ii-1)+0.08 0.12 0.35 0.8]);      
        sf = surf(phii/(2*pi),phij/(2*pi),Qs{ii},'LineWidth',0.05); %  
        hold on; 
        CData = get(sf,'CData');
        surf(phii/(2*pi),phij/(2*pi),ones(size(phii))-fLim-1,'CData',CData,'LineStyle','none');
        hold off;
        % colorbar;
        % colormap('gray');
        colormap('jet');
        % caxis([-mxQs mxQs]);
        %%%-- 11.06.2018 AY: fixed limits
        caxis([-fLim fLim]);
        %%%-- 
        axis square;

        %%% -- 11.06.2018 AY: fixed limits
        zlim([-fLim, fLim]);
        %%% -- 
        
        xlim([0 1]);
        ylim([0 1]);
        set(gca,'XTick',[0  0.5 1.0],'XTickLabel',{'0','\pi','2\pi'});
        set(gca,'YTick',[0  0.5 1.0],'YTickLabel',{'0','\pi','2\pi'});
        xlabel(['$\phi_' num2str(ii) '$'],'Interpreter','latex');
        ylabel(['$\phi_' num2str(mod(ii,2)+1) '$'],'Interpreter','latex');
        zlabel(['$q_{' num2str(ii) ','  num2str(mod(ii,2)+1) '}(\phi_' num2str(ii) ',\phi_' num2str(mod(ii,2)+1) ')$'],'Interpreter','latex');
%         get(gcf,'Renderer')
        set(gca,'XGrid','on');
        set(gca,'YGrid','on');
        set(gca,'ZGrid','on');
        set(gca,'FontSize',14);
end
%         colormap('gray');
        colormap('jet');
        colorbar('Position',[0.92 0.1 0.02 0.8]);

%% plot two figures separately and export them into a file.
for ii=1:2
    hfii = figure(16+ii);
    set(hfii,'Renderer','Painters','Units','pixels','Position',[100 200 782 782],'Color',[1 1 1]);        
    sf = surf(phii/(2*pi),phij/(2*pi),Qs{ii},'LineWidth',0.05); %  
%     hold on; 
%     CData = get(sf,'CData');
%     surf(phii/(2*pi),phij/(2*pi),ones(size(phii))-fLim-1,'CData',CData,'LineStyle','none');
%     hold off;
%     colorbar;
%     colormap('gray');
    colormap('jet');
%     caxis([-mxQs mxQs]);
    %%%-- 11.06.2018 AY: fixed limits
    caxis([-fLim fLim]);
    %%%-- 
    axis square;

%     zlim([-1.2, 1.2]*mxQs);
    %%% -- 11.06.2018 AY: fixed limits
    zlim([-fLim, fLim]);
    %%% -- 

    xlim([0 1]);
    ylim([0 1]);
    set(gca,'XTick',[0  0.5 1.0],'XTickLabel',{'0','\pi','2\pi'});
    set(gca,'YTick',[0  0.5 1.0],'YTickLabel',{'0','\pi','2\pi'});
    xlabel(['$\phi_' num2str(ii) '$'],'Interpreter','latex');
    ylabel(['$\phi_' num2str(mod(ii,2)+1) '$'],'Interpreter','latex');
%     zlabel(['$q_{' num2str(ii) ','  num2str(mod(ii,2)+1) '}(\phi_' num2str(ii) ',\phi_' num2str(mod(ii,2)+1) ')$'],'Interpreter','latex');
    zlabel(['$\Delta\phi_{' num2str(ii) '}/\Delta t$'],'Interpreter','latex',...
        'Position',[0.55, 2.0, -16.0]);
%     get(gcf,'Renderer')
%     set(gca,'XGrid','on');
%     set(gca,'YGrid','on');
%     set(gca,'ZGrid','on');
    set(gca,'Box','on');
    set(gca,'FontUnits','pixels','FontSize',FntSize);
    set(gca,'FontSize',FntSize);
    drawnow;
    frame = getframe(hfii);
    im = frame2im(frame);
    [imind,cm] = rgb2ind(im,256);
    imwrite(imind,cm,['q_NMM_' num2str(inindex) '_' num2str(ii) '_col_one.png'],'png');
end
%     colormap('gray');
%     colormap('jet');
%     colorbar('Position',[0.92 0.1 0.02 0.8]);
    
    