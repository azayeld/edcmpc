function plot_comp_sim_edcm_pc(simfilename, resfilename)
%%% this function compares the results of the fitting with edcm pc with the
%%% known true parameters of the simulation. 
%%% Inputs:
%%%     - simfilename is the name of the file where the structure of the 
%%%             simulation parameters (sim) are stored.
%%%     - resfilename is the name of the file with the results of the fitting
%%%     stored in DCM structure. 

load([simfilename]);
disp(['loaded: ' simfilename]); % the sim structure is loaded

load([resfilename]);
disp(['loaded: ' resfilename]); % the DCM structure is loaded

irc = sim.rc;
irs = sim.rc;
ianm = sim.anm;
ibnm = sim.bnm;
icnm = sim.cnm;
idnm = sim.dnm;

rc = DCM.Ep.rc;
rs = DCM.Ep.rc;
anm = DCM.Ep.anm;
bnm = DCM.Ep.bnm;
cnm = DCM.Ep.cnm;
dnm = DCM.Ep.dnm;

%% calculate the functions
Nmesh = 50;
[rho,rphi] = calcTransf2(rc,rs,Nmesh);
[irho,irphi] = calcTransf2(irc,irs,Nmesh);

figure(10);
plot(rphi,rho,'r');
hold on
plot(irphi,irho,'b');
hold off;

end