function [Qm,phii,phij] = calcCouplFunij_anm(DCM,N,i,j)
%%% this function calculates the coupling function Q between two regions i
%%% and j (j is coupled to i). The function is calcualted for uniformly distributed phases 
%%% (phii,phi_j) using coefficients 
%%% DCM.Ep.anm, DCM.Ep.bnm, DCM.Ep.cnm, and DCM.Ep.dnm, - the final fitted values. 
%%% Input: 
%%%     - DCM structure.    
%%%     - N sample size of uniformly distriuted phases. 
%%%     - i and j are the indexes of the regions
%%% Output:
%%%     - Qm is a matrix of the size NxN of the calculated function.
%%%     - phii and phij is the values of phases as an array of the size NxN. 
%%%
%%% AY 08.01.2018

% phi = (0:N-1)*2*pi/N;
phi = (0:N-1)*2*pi/(N-1);
[phii, phij] = meshgrid(phi,phi);

Nnm=size(DCM.Ep.dnm,3);
anm = reshape(DCM.Ep.anm(i,j,:,:),[Nnm,Nnm]); % anm is 2D
bnm = reshape(DCM.Ep.bnm(i,j,:,:),[Nnm,Nnm]); % bnm is 2D
cnm = reshape(DCM.Ep.cnm(i,j,:,:),[Nnm,Nnm]); % cnm is 2D
dnm = reshape(DCM.Ep.dnm(i,j,:,:),[Nnm,Nnm]); % dnm is 2D

%% check up here!

Qm = arrayfun(@calcQ,phii,phij);

    % function for calculation of the coupling function for two phase
    % values of two regions. The coefficients a,b,c, and d are of the size
    % Nnm x Nnm.  
    function Q = calcQ(psii,psij)
        nmx = (1:Nnm)'*[psii psij]; % should be of size Nnm x Nr
        cosnx = cos(nmx);   % 
        sinnx = sin(nmx);   % of size 2xNm
        Q = cosnx(:,1)'*(anm*cosnx(:,2) + bnm*sinnx(:,2))...
          + sinnx(:,1)'*(cnm*cosnx(:,2) + dnm*sinnx(:,2));
    end
end % function
