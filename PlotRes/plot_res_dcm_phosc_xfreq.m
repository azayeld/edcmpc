%%% plots the results of DCM fitting with the modeling part po - the new
%%% extended DCM for phase coupling. 
%%% based on original script plot_res_dcm_phosc_xfreq AY 08.01.2018
%%% The script plots several figures:
%%% 1. The figure of the initial approximation to the sigma funciton, 
%%% initial approximation of the rho function and it's final value,
%%% compared with the derivative of the observables.
%%% Last modification AY 21.03.2019: made compatible to the new structure
%%% of the folders, corrected and substituted some functions. Edited
%%% comments and removed commented parts.
clear;

%% set parameters of the script
showthetas = 1; %  1 show points, 0 do not show points
showobservables = 1; % 1 show calculated derivative of the observables, 0 does not show
Nmesh = 20; % the size of the mesh and of the uniform distributed phase

%%% plot results for the stochastic phase oscillators 
% pfx = '..\SimPhOsc\';
% inputfilename = 'dcm_nsig1_setn10_stpo_dataset4_compN15_R1.mat';
% inputfilename = 'dcm_nsig1_setn15_stpo_dataset4N15_R1.mat';

%%% comparison of the original an the new versions of DCM PC. 
% inputfilename = 'dcm_orig4_nsig1_setn15_stpo_dataset_compN15_noR1.mat';
% inputfilename = 'dcm_nsig1_setn1_stpo_dataset_compN15_R1.mat';

%%% plot the results for NMM
% pfx = '..\SimNMM\'; % folder with the data set files. 
%%% with Hilbert transformation and the transient synchronization
% inputfilename = 'dcm_nsig2_outSimJR2_H_test_onoff_noise_107_2.mat';
% inputfilename = 'dcm_nsig2_outSimJR2_H_test_onoff_noise_108_1.mat';

%%% plot demo results
pfx = '..\Demos\Demo1\';
inputfilename = 'dcm_nsig4_setn1_stPhOsc_dataset_R1.mat';
% pfx = '..\Demos\Demo2\';
% inputfilename = 'dcm_nsig2_setn1_outSimSt2JR_set_2.mat';


% change the last number to see other cases. 

load([pfx inputfilename]);
disp(['loaded: ' inputfilename]);

addpath('..\TestDataset\');
addpath('../ModComp');

% Testing part: we substitute the coefficients with those from
% the simulation. Turn this part off to see the real results.
% DCM = substcoefs(DCM,'phosc_xfreq_data_set_smpl_trans3.mat');
    
    %% transpose sc to calculate sigma
    [sigma,sphi] = calcTransf2(DCM.sc,DCM.ss,Nmesh); % calculate sigma from DCM.sc and DCM.ss
    if isfield(DCM,'rc')
        [appxrho,raphi] = calcTransf2(DCM.rc,DCM.rs,Nmesh); % calculate sigma from DCM.rc and DCM.rs 
    else % we did transformation before fitting
        if isfield(DCM.M,'Nrho')
            Nrho = DCM.M.Nrho;
        else
            Nrho = 1;
        end
        [rc,rs] = invcoef(DCM.sc,DCM.ss,1000,Nrho); 
        [appxrho,raphi] = calcTransf2(rc,rs,Nmesh);
    end
    [rho,rphi] = calcTransf2(DCM.Ep.rc,DCM.Ep.rs,Nmesh); % calculate rho from DCM.Ep.rs and DCM.Ep.rc

    
    %%% stopped here testing distributions. 
    [ntheta1,xbins1] = distrPPhase3(DCM.xY.y,Nmesh);
    [ntheta,xbins] = distrPPhase3(DCM.xY.y,Nmesh);
    [dthetas,uphis,thetas,freq] = derivs(DCM.xY.y,DCM.xY.pst);
    
    % we calculate the coefficients of sigma from the coefficients of rho
    if isfield(DCM,'rc') 
        if isfield(DCM.M,'Nsig')
            Nsig = DCM.M.Nsig;
        else
            Nsig = 2;
        end
        [sc,ss] = invcoef(DCM.rc,DCM.rs,1000,Nsig);
        DCM1.sc=sc;
        DCM1.ss=ss;
    elseif isfield(DCM,'sc') % then we performed transformation before fitting
        DCM1.sc=DCM.sc;
        DCM1.ss=DCM.ss;
    end

    phis = transfv(thetas,DCM1.sc,DCM1.ss);
    
    hf12 = figure(12);
    set(hf12,'Position',[67 677 560 420]);
    for ii=1:2
%         % distribution of the data points
%         subplot(2,2,ii);
%         bar(xbins,ntheta(:,ii),'b');
%         hold on;
%         if ~isempty(sigma)
%             % sigma calculated from sc and ss
%             plot(sphi,sigma(:,ii),'r-');
%         end
%         hold off;
%         xlim([0 2*pi]);
        
        % reciprocal of the derivatives
        subplot(2,2,ii);
        plot(thetas(:,ii),1./dthetas(:,ii),'b.');
        hold on;
        if ~isempty(sigma)
            % sigma calculated from sc and ss
            plot(sphi,sigma(:,ii)/freq(ii),'r-');
        end
        hold off;
        xlim([0 2*pi]);
        
        
        subplot(2,2,ii+2);
        plot(uphis(:,ii),dthetas(:,ii),'k.');
%         plot(thetas(:,ii),dthetas(:,ii),'k.');
        hold on;
        if isfield(DCM.Ep,'rs')
            plot(raphi,appxrho(:,ii)*DCM.M.freq(ii),'b-','LineWidth',2);
            %%% AY 27.09.2018 here is the problem, that we plot rho(phi)
            %%% not rho(phi(theta)). To do that we need to apply the transformation 
            %%% Rho to rphi. 
            plot(rphi,rho(:,ii)*(DCM.M.freq(ii)+DCM.Ep.df(ii)),'r-','LineWidth',2);
        end

%         plot(sphi,(2-sigma(:,ii))*DCM.M.freq(ii),'r-');
        hold off;
        xlim([0 2*pi]);
        xlabel('$\phi$','interpreter','latex');
        ylabel('$d\phi/dt,\rho$','interpreter','latex');
        legend('data','approx','final');
    end
    
%% check up from here! 
    % plot coupling functions
    
    % define borders: 
    mxdth = max(max(abs(dthetas)));
        
    for ii=1:2 % loop by regions, we consider only two regions
%         subplot(1,2,ii);
        figure(13+ii);
        set(gcf,'Renderer','opengl','Position',[1290 140 540 420]);
        %-% calcQphij in SimPhOsc, as in plot_res_dcm_po.m
        [Q,phii,phij] = calcCouplFunij_anm(DCM,Nmesh,ii,mod(ii,2)+1);
        Q = Q + DCM.M.freq(ii)+DCM.Ep.df(ii);
        if showobservables==1
            if isfield(DCM,'ss')
            Q = Q.*repmat(rho(:,ii)',Nmesh,1);
            end
        end
%         mxdth = max([mxdth,max(max(abs(Q)))]);
        
        surf(phii,phij,Q,'LineStyle','none'); % 
        colormap; 
        caxis([-mxdth mxdth]);
        axis square;

        if showthetas==1
            hold on;
%             scatter3(thetas(:,ii),thetas(:,mod(ii,2)+1),dthetas(:,ii),36,'.'); % ,'Color',[0.5 0.5 0.5]);
            scatter3(phis(:,ii),phis(:,mod(ii,2)+1),dthetas(:,ii),36,'.'); % ,'Color',[0.5 0.5 0.5]);
            set(gcf, 'Renderer', 'opengl');
            hold off;
        end
        zlim([-1, 1]*20+DCM.M.freq(ii)+DCM.Ep.df(ii));
%         zlim([-1, 1]*2+DCM.M.freq(ii)+DCM.Ep.df(ii));
        xlim([0 2*pi]);
        ylim([0 2*pi]);
        xlabel(['\phi_' num2str(ii)]);
        ylabel(['\phi_' num2str(mod(ii,2)+1)]);
%         get(gcf,'Renderer')
    end
    
    
%% plot figure two regions together 
hf16 = figure(16);
set(hf16,'Renderer','Painters','Position',[195 108 950 450],'Color',[1 1 1]);
% define surfaces and borders:
for ii=1:2 % loop by regions, we consider only two regions
       %-% calcQphij in SimPhOsc, as in plot_res_dcm_po.m
       [Qs{ii},phii,phij] = calcCouplFunij_anm(DCM,Nmesh,ii,mod(ii,2)+1);
        Qs{ii} = Qs{ii} + DCM.M.freq(ii)+DCM.Ep.df(ii);
        if showobservables==1
            if isfield(DCM,'ss')
            Qs{ii} = Qs{ii}.*repmat(rho(:,ii)',Nmesh,1);
            end
        end
        Qs{ii} = Qs{ii}-DCM.M.freq(ii)-DCM.Ep.df(ii);
        mxQ{ii} = max(max(abs(Qs{ii}))); % maximum deviation without mean
         
end
mxQs = max([mxQ{1},mxQ{2}]);

%%%-- 21.06.2018 AY: fixed limits 
% fLim = 24.0; % for NMM
% fLim = 14.0; % for NMM
% fLim = 0.5; % for stochastic phase oscillators
% fLim = 0.5; % for comparison of old and new 

%%% --- 0.1.10.2018 AY 
% fLim = 3.0;

if showobservables == 0
    fLim = 3.0;
else
    fLim = 14.0;
end

for ii=1:2 % loop by regions, we consider only two regions
%         subplot(1,2,ii);
        subplot('Position',[0.44*(ii-1)+0.08 0.12 0.35 0.8]);      
%         sf = surf(phii/(2*pi),phij/(2*pi),Qs{ii},'LineWidth',0.05,'EdgeColor','interp','FaceColor','flat'); % 
        sf = surf(phii/(2*pi),phij/(2*pi),Qs{ii},'LineStyle','-','LineWidth',0.05,'EdgeColor',[0.5 0.5 0.5],'FaceColor','flat'); % 
%         surf(psii,psij,Q,'LineStyle','-','FaceColor','flat','EdgeColor',[0.5 0.5 0.5]);
%         colormap('gray');%  
%         hold on; 
%         CData = get(sf,'CData');
%         surf(phii/(2*pi),phij/(2*pi),ones(size(phii))-fLim-1,'CData',CData,'LineStyle','none');
%         hold off;
        %         colorbar;
        
%         colormap('jet');
        colormap('bone');
%         caxis([-mxQs mxQs]);
        %%%-- 11.06.2018 AY: fixed limits
        caxis([-fLim fLim]);
        %%%-- 
        axis square;

        if showthetas==1
            hold on;
            scatter3(phis(:,ii)/(2*pi),phis(:,mod(ii,2)+1)/(2*pi),...
                dthetas(:,ii)-DCM.M.freq(ii)-DCM.Ep.df(ii),36,'.'); % ,'Color',[0.5 0.5 0.5]);
            set(gcf, 'Renderer', 'opengl');
            hold off;
        end
        
%         zlim([-1.2, 1.2]*mxQs);
        %%% -- 11.06.2018 AY: fixed limits
        zlim([-fLim, fLim]);
        %%% -- 
        
%         xlim([0 2*pi]);
%         ylim([0 2*pi]);
        xlim([0 1]);
        ylim([0 1]);
        set(gca,'XTick',[0  0.5 1.0],'XTickLabel',{'0','\pi','2\pi'});
        set(gca,'YTick',[0  0.5 1.0],'YTickLabel',{'0','\pi','2\pi'});
        xlabel(['$\varphi_' num2str(ii) '$'],'Interpreter','latex');
        ylabel(['$\varphi_' num2str(mod(ii,2)+1) '$'],'Interpreter','latex');
        zlabel(['$q_{' num2str(ii) ','  num2str(mod(ii,2)+1) '}(\varphi_' num2str(ii) ',\varphi_' num2str(mod(ii,2)+1) ')$'],'Interpreter','latex');
%         get(gcf,'Renderer')
        set(gca,'XGrid','on');
        set(gca,'YGrid','on');
        set(gca,'ZGrid','on');
        set(gca,'FontSize',14);
end
%         subplot(); 
%         colormap('gray');
%         colormap('jet');
        colorbar('Position',[0.92 0.1 0.02 0.8]);

%%% plot the results of the nonlinear relation between obseravble phase and theoretical phase.
%%% calculate the integral
% [Rhoint, urhoint] = calcIntRhoSigma2(DCM.rc,DCM.rs,500); 
%-% change to calcTransf2 in TestDataset
[rhos,urhos] = calcTransf2(DCM.Ep.rc,DCM.Ep.rs,500);
hf17 = figure(17);
set(hf17,'Renderer','Painters','Position',[300 200 830 450],'Color',[1 1 1]);
for ii=1:2 % loop by regions, we consider only two regions
%         subplot(1,2,ii);
        subplot('Position',[0.45*(ii-1)+0.1 0.12 0.35 0.8]);      

%         plot(urhoint,Rhoint(1,:),'k');
        plot(urhos/(2*pi),rhos(:,ii),'k-','LineWidth',2);
        axis square;
        
        xlim([0 1]);
        ylim([0.9 1.1]);
        set(gca,'XTick',[0  0.5 1.0],'XTickLabel',{'0','\pi','2\pi'});
        set(gca,'YTick',[0.9  1.0 1.1]); % ,'YTickLabel',{'0','\pi','2\pi'});
        xlabel(['$\varphi_' num2str(ii) '$'],'Interpreter','latex');
        ylabel(['$\rho_' num2str(ii) '(\varphi_' num2str(ii) ')$'],'Interpreter','latex'); 
        set(gca,'XGrid','on');
        set(gca,'YGrid','on');
        set(gca,'FontSize',14);
end