function plot_comp_res_edcm_pc(resfile,simfile)
%%% plots the results of the fitting with eDCM PC with the true parameter 
%%% values used during the synthetic data simulation.
%%% 
%%% Input:
%%%     - resfile is the file where the results of the fitting with eDCM PC
%%%     are stored, e.g. dcm_nsig1_setn1_stPhOsc_dataset_R1.mat.
%%%     - simfile is the file where the simulation parameters are strored,
%%%     e.g. stPhOsc_dataset_R1.mat.

load([resfile]); % the DCM structure is loaded
disp(['loaded: ' resfile]); 

load([simfile]); % the sim structure is loaded
disp(['loaded: ' simfile]);

% Extract some values of the simulation parameters 
Nrho = size(sim.rc,2);  % number of Fourier terms of the transformation function
Nq = sim.Nnm;           % number of Fourier terms of the coupling function 
Nr = sim.Nr;            % number of regions

Nrc = Nr*Nrho;          % the size of vectorized rc or rs
Nanm = Nr*Nr*Nq*Nq;     % the size of vectorized anm,bnm,cnm and dnm

Nv = 2*Nrc + 4*Nanm - 4*Nr*Nq*Nq;  
% the size of the vector is the sum of vetorized sizes of
% rc,rs,anm,bnm,cnm,dnm minus the diagonal elements with i=j.

% The vector of the simulation parameter values
simvec = zeros(Nv,1);

% The vector of the fitted parameter values
fitvec = zeros(Nv,1);

% Allocare the labels vector
lblvec = cell(Nv,1);
iv = 1;
for ir = 1:Nr
    for irho = 1:Nrho
        simvec(iv) = sim.rc(ir,irho);
        fitvec(iv) = DCM.Ep.rc(ir,irho);
        lblvec{iv} = ['rc_{', num2str(Nrho), '}^{', num2str(ir), '}'];
        iv = iv + 1;
        
        simvec(iv) = sim.rs(ir,irho);
        fitvec(iv) = DCM.Ep.rs(ir,irho);
        lblvec{iv} = ['rs_{', num2str(Nrho), '}^{', num2str(ir), '}'];
        iv = iv + 1;        
    end
end

for jnm = 1:Nq
    for inm = 1:Nq
        for ii = 1:Nr
            for jj = 1:Nr
                if ii~=jj % the terms with ii==jj are 0
                    simvec(iv) = sim.anm(ii,jj,inm,jnm);
                    fitvec(iv) = DCM.Ep.anm(ii,jj,inm,jnm);
                    lblvec{iv} = ['a_{', num2str(inm), ',', num2str(jnm), '}^{',...
                        num2str(ii), ',', num2str(jj), '}'];
                    iv = iv + 1;
                    
                    simvec(iv) = sim.bnm(ii,jj,inm,jnm);
                    fitvec(iv) = DCM.Ep.bnm(ii,jj,inm,jnm);
                    lblvec{iv} = ['b_{', num2str(inm), ',', num2str(jnm), '}^{',...
                        num2str(ii), ',', num2str(jj), '}']; 
                    iv = iv + 1;
                    
                    simvec(iv) = sim.cnm(ii,jj,inm,jnm);
                    fitvec(iv) = DCM.Ep.cnm(ii,jj,inm,jnm);
                    lblvec{iv} = ['c_{', num2str(inm), ',', num2str(jnm), '}^{',...  
                        num2str(ii), ',', num2str(jj), '}']; 
                    iv = iv + 1;
                    
                    simvec(iv) = sim.dnm(ii,jj,inm,jnm);
                    fitvec(iv) = DCM.Ep.dnm(ii,jj,inm,jnm);
                    lblvec{iv} = ['d_{', num2str(inm), ',', num2str(jnm), '}^{',...
                        num2str(ii), ',', num2str(jj), '}']; 
                    iv = iv + 1;
                end
            end
        end
    end
end

hf30 = figure(30);
set(hf30,'Renderer','Painters','Position',[405 45 750 315],'Color',[1 1 1]);
plot([1:Nv],simvec,'ok');
hold on;
plot([1:Nv],fitvec,'*r');
hold off;
set(gca,'XTick',[1:Nv]);
set(gca,'XTickLabel',lblvec);


end