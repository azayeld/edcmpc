function plot_res_edcm_pc(inputfilename,plotpar)
% This function plots the results of the fitting with eDCM PC.
% The function plots following figures:
% * the derivatives of the observable phases along with the inverse 
% transformation function sigma; 
% * the derivatives of the theoretical phases along wtih the prior and 
% fitted forward transformation functions rho;
% * the final fitted forms of the inverse and forward transformation
% functions simga and rho;
% * the coupling functions, presented as surfaces, along with the derivatives
% of the theoretical phase (after performing the inverse tranformation);
% * the general surfaces [(coupling + frequency)*transformation] together
% with the derivatives of the observable phases.
% * the original values of the Fourier coefficients and the ones found by
% fitting with eDCM PC. (?)
%   
% Input:
%   - inputfilename is the full path plus name of the eDCM PC file. The file
%     should contain the results in the DCM structure.
%   - plotpar is the structure of the plotting parameters. This structure
%   has the following fields:
%       - Nmesh is the size of the mesh of all figues. The number of points
%       for the uniform distributed phase.
%       - showdata defines whether the figure contains the data points.
%       - showbars defines whether the distribution bar will be plotted.
% 
% -------------------------------------------------------------------------
% AY 23.03.2021

%% Initial redefenitions.
Nmesh = plotpar.Nmesh;

%% Load the results 
load([inputfilename]);
disp(['loaded: ' inputfilename]);

%% Find the initial approximation of the transformation functions.
% they are stored in DCM.sc and DCM.ss, and if istransf is false then also
% in DCM.rc and DCM.rs. 
addpath('..\TestDataset\'); % to call calcTransf2
addpath('..\ModComp\'); % to call derivs and calcQphij
[appxSigma,saphi] = calcTransf2(DCM.sc,DCM.ss,Nmesh); % sphi is the uniform distributed phase for sigma function

if isfield(DCM,'rc')
   [appxRho,raphi] = calcTransf2(DCM.rc,DCM.rs,Nmesh); 
else
   [rc,rs] = invcoef(DCM.sc,DCM.ss,1000,DCM.M.Nsig);
   [appxRho,raphi] = calcTransf2(rc,rs,Nmesh); 
end

%% Find the final forms of the transformation functions.
[Rho,rphi] = calcTransf2(DCM.Ep.rc,DCM.Ep.rs,Nmesh); 
[sc,ss] = invcoef(DCM.Ep.rc,DCM.Ep.rs,1000,size(DCM.sc,2));
[Sigma,sphi] = calcTransf2(sc,ss,Nmesh);

%% Plot the results (Fig 17).
mxsig = ceil(10*max(max(abs(Sigma)-1)))/10;
mxrho = ceil(10*max(max(abs(Rho)-1)))/10;

hf17 = figure(17);
set(hf17,'Renderer','Painters','Position',[1150 180 390 250],'Color',[1 1 1]);
for ii=1:2 % loop by regions, we consider only two regions
%         subplot(1,2,ii);
        subplot('Position',[0.45*(ii-1)+0.1 0.12 0.35 0.8]);      

%         plot(urhoint,Rhoint(1,:),'k');
        plot(rphi/(2*pi),Rho(:,ii),'k-','LineWidth',2);
        axis square;
        
        xlim([0 1]);
        ylim(1+[-mxrho mxrho]);
        set(gca,'XTick',[0  0.5 1.0],'XTickLabel',{'0','\pi','2\pi'});
        % set(gca,'YTick',[0.9  1.0 1.1]); % ,'YTickLabel',{'0','\pi','2\pi'});
        xlabel(['$\varphi_' num2str(ii) '$'],'Interpreter','latex');
        ylabel(['$\rho_' num2str(ii) '(\varphi_' num2str(ii) ')$'],'Interpreter','latex'); 
        set(gca,'XGrid','on');
        set(gca,'YGrid','on');
        set(gca,'FontSize',10);
end

hf18 = figure(18);
set(hf18,'Renderer','Painters','Position',[1150 525 390 250],'Color',[1 1 1]);
for ii=1:2 % loop by regions, we consider only two regions
%         subplot(1,2,ii);
        subplot('Position',[0.45*(ii-1)+0.1 0.12 0.35 0.8]);      

%         plot(urhoint,Rhoint(1,:),'k');
        plot(sphi/(2*pi),Sigma(:,ii),'k-','LineWidth',2);
        axis square;
        
        xlim([0 1]);
        ylim(1+[-mxsig mxsig]);
        set(gca,'XTick',[0  0.5 1.0],'XTickLabel',{'0','\pi','2\pi'});
        % set(gca,'YTick',[0.9  1.0 1.1]); % ,'YTickLabel',{'0','\pi','2\pi'});
        xlabel(['$\theta_' num2str(ii) '$'],'Interpreter','latex');
        ylabel(['$\sigma_' num2str(ii) '(\theta_' num2str(ii) ')$'],'Interpreter','latex'); 
        set(gca,'XGrid','on');
        set(gca,'YGrid','on');
        set(gca,'FontSize',10);
end

%% Calculate the derivatives of the observable phases.
[dthetas,uphis,thetas,freq] = derivs(DCM.xY.y,DCM.xY.pst);
%% Find the theoretical/true phases using the final inverse transformation function.
for n=1:length(DCM.xY.y) % we transform all data from observable (protophase) into theoretical (true) phase.
    xY.y{n} = transfv(DCM.xY.y{n},sc,ss); % using universal function
end

%% Calculate the derivatives of the theoretical phases.
[dphis,uphis,phis,pfreq] = derivs(xY.y,DCM.xY.pst);

%% Find the distribution of the observable and the theoretical phases.
% usde distrPPhase3.
[ntheta,thxbins] = distrPPhase3(DCM.xY.y,20);
[nphi,phxbins] = distrPPhase3(xY.y,20);

%% Plot the results (Fig. 12)
% Plot the derivatives along with the initial approximation and the final 
% fitted transformation functions.
hf12 = figure(12);
set(hf12,'Position',[1 380 400 400]);
    for ii=1:2
        il1=1; % counters for legends
        % for proto/observable phase
        subplot(2,2,ii);
        % distribution of the data points
        if plotpar.showbars         
            bar(thxbins/(2*pi),ntheta(:,ii),'b');
            hold on;
            legstrs1{il1} = 'distr'; il1=il1+1;
        end
        
        % reciprocal of the derivatives
        if plotpar.showdata       
            % plot(thetas(:,ii),1./dthetas(:,ii),'b.');
            % scatter(uphis(:,ii)/(2*pi),1./dthetas(:,ii),'.',...
            scatter(thetas(:,ii)/(2*pi),freq(ii)*1./dthetas(:,ii),'.',...
            'MarkerFaceColor','g','MarkerEdgeColor','g'); %,...
                % 'MarkerFaceAlpha',.2,'MarkerEdgeAlpha',.2);
            hold on;
            alpha(0.5);
            legstrs1{il1} = 'data'; il1=il1+1;
        end
    
        % the prior approximation of the inverse transfromation function
        plot(saphi/(2*pi),appxSigma(:,ii),'k-','LineWidth',2);
        legstrs1{il1} = 'appx'; il1=il1+1;
        % the inverse transformation function
        plot(sphi/(2*pi),Sigma(:,ii),'r-','LineWidth',2);
        legstrs1{il1} = 'final'; il1=il1+1;
        hold off;
        xlim([0 1]);
        xlabel(['$\theta_' num2str(ii) '$'],'interpreter','latex');
        ylabel(['$\frac{dt}{d\theta_' num2str(ii)...
                '}(\theta_' num2str(ii) '),\sigma_' num2str(ii)...
                '(\theta_' num2str(ii) ')/\omega_' num2str(ii)...
                '$'],'interpreter','latex');
        
        if ii==2
                legend(legstrs1,'Location','northoutside','Orientation','horizontal',...
                        'Position',[0.145 0.945 0.755 0.05]);
        end
        
        subplot(2,2,ii+2);
        % reciprocal of the derivatives
        if plotpar.showdata       
            scatter(phis(:,ii)/(2*pi),dthetas(:,ii),'.',...
                'MarkerFaceColor','g','MarkerEdgeColor','g'); %,...
                % 'MarkerFaceAlpha',.2,'MarkerEdgeAlpha',.2);
            hold on;
            alpha(0.5);
        end
        
        % the prior approximation of the inverse transfromation function
        plot(raphi/(2*pi),appxRho(:,ii)*freq(ii),'k-','LineWidth',2);
        % the inverse transformation function
        plot(rphi/(2*pi),Rho(:,ii)*(DCM.M.freq(ii)+DCM.Ep.df(ii)),'r-','LineWidth',2);
        hold off;
        xlim([0 1.0]);
        xlabel(['$\varphi_' num2str(ii) '$'],'interpreter','latex');
        ylabel(['$\frac{d\theta_' num2str(ii) '}{dt}(\varphi_' num2str(ii)...
            '),\rho_' num2str(ii) '(\varphi_' num2str(ii)...
            ')$'],'interpreter','latex');
    end

    
%% Calculate the coupling functions
% create mesh 
phi = (0:Nmesh-1)*2*pi/(Nmesh-1);
[phii, phij] = meshgrid(phi,phi);

% define surfaces and borders:
for ii=1:2 % loop by regions, we consider only two regions
        % find coefficients
        Nnm=size(DCM.Ep.dnm,3);
        a = reshape(DCM.Ep.anm(ii,mod(ii,2)+1,:,:),[Nnm,Nnm]); % anm is 2D
        b = reshape(DCM.Ep.bnm(ii,mod(ii,2)+1,:,:),[Nnm,Nnm]); % bnm is 2D
        c = reshape(DCM.Ep.cnm(ii,mod(ii,2)+1,:,:),[Nnm,Nnm]); % cnm is 2D
        d = reshape(DCM.Ep.dnm(ii,mod(ii,2)+1,:,:),[Nnm,Nnm]); % dnm is 2D
      
        Qs{ii} = arrayfun(@(x,y) calcQphij(a,b,c,d,x,y),phii,phij);
        mxQ{ii} = max(max(abs(Qs{ii}))); % maximum deviation without mean
         
end
mxQs = max([mxQ{1},mxQ{2}]);

%% Plot the results for theoretical phases. 
fLim = 5*(floor(mxQs/5)+1); % vertical limits with the step multiple to 5.  
hf16 = figure(16);
set(hf16,'Renderer','Painters','Position',[405 460 750 315],'Color',[1 1 1]);
for ii=1:2 % loop by regions, we consider only two regions
%         subplot(1,2,ii);
        subplot('Position',[0.44*(ii-1)+0.08 0.12 0.35 0.8]);      
        sf = surf(phii/(2*pi),phij/(2*pi),Qs{ii},'LineWidth',0.05); %  
        hold on; 
        CData = get(sf,'CData');
        surf(phii/(2*pi),phij/(2*pi),ones(size(phii))-fLim-1,'CData',CData,'LineStyle','none');
        
        if plotpar.showdata==1
            hold on;
            scatter3(phis(:,ii)/(2*pi),phis(:,mod(ii,2)+1)/(2*pi),...
                dphis(:,ii)-DCM.M.freq(ii)-DCM.Ep.df(ii),36,'.'); % ,'Color',[0.5 0.5 0.5]);
            set(gcf, 'Renderer', 'opengl');
            hold off;
        end
        
        % colorbar;
        % colormap('gray');
        colormap('jet');
        caxis([-mxQs mxQs]);
        % caxis([-fLim fLim]);
        axis square;
       
        zlim([-fLim, fLim]);
       
        xlim([0 1]);
        ylim([0 1]);
        set(gca,'XTick',[0  0.5 1.0],'XTickLabel',{'0','\pi','2\pi'});
        set(gca,'YTick',[0  0.5 1.0],'YTickLabel',{'0','\pi','2\pi'});
        xlabel(['$\phi_' num2str(ii) '$'],'Interpreter','latex');
        ylabel(['$\phi_' num2str(mod(ii,2)+1) '$'],'Interpreter','latex');
        zlabel(['$q_{' num2str(ii) ','  num2str(mod(ii,2)+1) '}(\phi_' num2str(ii) ',\phi_' num2str(mod(ii,2)+1) ')' ...
                ',\frac{d\varphi_' num2str(ii) '}{dt}(\varphi_' num2str(ii) ')-\omega_' num2str(ii) '$'],...        
                'Interpreter','latex');
%         get(gcf,'Renderer')
        set(gca,'XGrid','on');
        set(gca,'YGrid','on');
        set(gca,'ZGrid','on');
        set(gca,'FontSize',11);
end
%         colormap('gray');
        colormap('jet');
        colorbar('Position',[0.92 0.1 0.02 0.8]);
        
        
%% Plot the results for observable phases. 
hf15 = figure(15);
sp = cell(2,1);
set(hf15,'Renderer','Painters','Position',[405 45 750 315],'Color',[1 1 1]);
for ii=1:2 % loop by regions, we consider only two regions
%         subplot(1,2,ii);
        sp{ii} = subplot('Position',[0.44*(ii-1)+0.08 0.12 0.35 0.8]);      
        sf = surf(phii/(2*pi),phij/(2*pi),...
            (Qs{ii}+DCM.M.freq(ii)+DCM.Ep.df(ii)).*repmat(Rho(:,ii)',Nmesh,1),...
            'LineWidth',0.05); %  
        hold on;
        scatter3(phis(:,ii)/(2*pi),phis(:,mod(ii,2)+1)/(2*pi),...
            dthetas(:,ii),36,'.'); % ,'Color',[0.5 0.5 0.5]);
        set(gcf, 'Renderer', 'opengl');
        hold off;

        % colormap('gray');
        colormap('jet');
       
        axis square;
       
        xlim([0 1]);
        ylim([0 1]);
        set(gca,'XTick',[0  0.5 1.0],'XTickLabel',{'0','\pi','2\pi'});
        set(gca,'YTick',[0  0.5 1.0],'YTickLabel',{'0','\pi','2\pi'});
        xlabel(['$\varphi_' num2str(ii) '$'],'Interpreter','latex');
        ylabel(['$\varphi_' num2str(mod(ii,2)+1) '$'],'Interpreter','latex');
        zlabel(['$\frac{d\varphi_' num2str(ii) '}{dt}(\varphi_' num2str(ii) ')\cdot\rho(\varphi_' num2str(ii) '),' ...            ' 
            '\frac{d\theta_' num2str(ii) '}{dt}(\varphi_' num2str(ii) ')$'],...
            'Interpreter','latex');
%         get(gcf,'Renderer')
        set(gca,'XGrid','on');
        set(gca,'YGrid','on');
        set(gca,'ZGrid','on');
        set(gca,'FontSize',11);
end
        %%% set similar amplitude limits for both subplots
        mxQsThs = max(diff(caxis(sp{1})),diff(caxis(sp{2})));
        mxQsThs = 10*ceil(mxQsThs/10)/2; % make multiple of 10
        m5freq = 5*ceil((DCM.M.freq+DCM.Ep.df)/5); % find freq multiple to 5
        m10freq = 10*ceil((DCM.M.freq+DCM.Ep.df)/10); % find freq multiple to 10
        
        %%% set new colormap limits 
        caxis(sp{1},mxQsThs*[-1 1]+m5freq(1));
        caxis(sp{2},mxQsThs*[-1 1]+m5freq(2));
        
        %%% set new plot limits
        zlim(sp{1},mxQsThs*[-1 1]+m10freq(1));
        zlim(sp{2},mxQsThs*[-1 1]+m10freq(2));
        
        %%% set labels of the colormap to show amplitudes
        cb = colorbar('Position',[0.92 0.1 0.02 0.8]);
        
        set(cb,'XTickLabel',get(cb,'XTick')-m10freq(2));
        
end 