function [y,uphi] = calcTransf2(a,b,N)
%%% this function calculates a transformation funcion (either sigma or rho)
%%% using Fourier coefficients a and b. N is the number of points in the
%%% uniformly distributed phase uphi.
%%% Input: 
%%%     - a and b are the Fourier coeffieint for cosine and sine 
%%%         of size NrxNtr, where Ntr is the number of Fourier terms and Nr
%%%         is the number of regions. 
%%%     - N sample size of uniformly distriuted phase.
%%% Output:
%%%     - y is a matrix of the size NxNr of the calculated function 
%%%         y(uphi) = 1 + \sum_{k=1}^{Ntr} (a*cos(k*uphi) + b*sin(k*uphi)) 
%%%     - uphi is its argument, an array of the size Nx1. 
%%%
%%% This is a new version of calcTransf from AY 24.01.2018
%%% The size of a and b is changed
%%% AY 08.03.2018
%%% Last modification: AY 28.02.2019, edited comments

    [Nr,Nt] = size(a); 
       
    if nargin<2  
        N = 500; % default number of points for uniformly distributed phases
    end
    
%     uphi = (0:N-1)'*2*pi/N; % uniformly distributed phase
    uphi = (0:N-1)'*2*pi/(N-1); % uniformly distributed phase
    kuphi = uphi*[1:Nt];
    coskuphi = cos(kuphi);
    sinkuphi = sin(kuphi);
    
    y = ones(N,Nr); % allocate a non-uniformly distributed phase variable
    for c = 1:Nr
        y(:,c) = y(:,c) + coskuphi*a(c,:)'+sinkuphi*b(c,:)';
    end   
end