function [ndstr,xbins] = distrPPhase3(y,Nbins)
%%% This function finds the distribution of the input data (proto phases)
%%% for given dataset y. The distribution will be
%%% calculated using weighting (eta function) and normalization (mean is 1).
%%% Input:
%%%     - y is an array of trials, dataset. 
%%%     - pst is the peristimulus time.
%%%     - mFreq is the frequencies.
%%%     - Nbins is the number of bins. Default is 20. 
%%% Output:
%%%     - ndstr is a vector of bincounts
%%%     - xbins is a bin ranges of the size N
%%%
%%% AY 05.10.2018.
%%% Modification AY 10.11.2017: add loop by regions
%%% Last modification: AY 28.02.2019, edited comments

    if nargin<4
        Nbins=20; % default number of bins
    end
        
    
    Nt     = length(y);      % number of trials
    Nr     = size(y{1},2);     % number of sources
    Ns     = size(y{1},1);   % number of samples
        
    % create bin ranges
    bins = (0:Nbins)'*2*pi/Nbins; % array of the size N+1
    xbins = bins(1:end-1)+1/(2*Nbins); % the center of the bins, of the size N
    
    % allocate counts
    ndstr = zeros(Nbins,Nr);
    
    % loop by trials
    for n=1:Nt
        for c=1:Nr
            thetas = y{n}(:,c); % a sample of size Ns 
            % define the rectangular function eta - weighting function
            ak = floor((thetas(end)-thetas(1))/(2*pi));  % the number of full periods
            % difference of theta(end)-theta(1) in mod 2pi
            dmth = mod(thetas(end)-thetas(1),2*pi); 
            % we find the weight at xbins for every region
            t1 = dmth/(2*pi);
            
            eta = ones(size(thetas)); % this line we need to escape 
                                      % "devision by zero" when calculating wgt.
            eta = (ak + (mod(thetas-thetas(1),2*pi)<=dmth))./(ak+t1);
            
            % find histogram values
            [bincounts,bin] = histc(mod(thetas,2*pi),bins); % returns bin counts of the size N+1
            % create eta function for bins
            etabins = ones(size(bins));
            etabins(bin) = eta; % correct values of etabins with respect to the weight of the thetas
            
            ndstr(:,c) = ndstr(:,c) + bincounts(1:end-1)./etabins(1:end-1);
    %         ndstr = ndstr + bincounts(1:end-1,:); % without weighting
        end
    end
    normc = Nbins*1./sum(ndstr); % returns 1xNr normalization coefficients
    ndstr = ndstr.*repmat(normc,[Nbins,1]);
end