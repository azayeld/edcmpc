function test_datasets(inputfilename,Nsig,istransf)
%%% test_datasets: 
%%% This function produces different plots to test the properties of the 
%%% dataset which is relevant for finding the coupling function using 
%%% eDCM PC. Namely, this function does the folowing:
%%%  ...
%%% Finds the Fourier coefficients of the datasets using
%%% approxiamtion of the surface. 
%%% The inputs of the function:
%%%     - inputfilename is the full path to the file where the data sets are
%%%     stored.
%%%     - Nsig the number of Fourier terms of the approximated inverse
%%%     transformation function sigma.
%%%     - istransf is the flag that indicates whether the input data set
%%%     should be tested as observable or theoretical phase, i.e whether
%%%     should the (approximate) transformation be applied or not. When the
%%%     value is 1, the approximate transformation will be applied to the
%%%     data, and the results will be presented for an approximate
%%%     theoretical phase. 
%%%
%%% The input datasets are:
%%%     a - coupled phase oscillators model with a given transformation 
%%%     b - Neural mass models 
%%%
%%% (a) is used to test the method, (b) is to test the dcm modelling part
%%% later. 
%%% Some part of the program are taken from test_DCM_PHXFREQ_JR2_damoco.m
%%% AY 07.02.2018
%%% -------------------------------
%%% Last modification AY 22.02.2019: edited the comments and changed the
%%% parts of the code to make it suitable for the repository folders
%%% structure.

% istransf = 0; 

% inindex = 1; % intex that defines different cases.
% % inindex defines different coupling configurations:
% % 0 - no coupling 
% % 1 - 2nd to 1st
% % 2 - 1st to 2nd
% % 3 - bidirectional
% 
%% add path to other parts of the codes
addpath('..\ModComp\'); % for transfv.m, invcoef3.m, appxSigma, derivs 
% 
% %%% phase oscillators data set
% % pfx = '..\SimPhOsc\'; % prefix, the folder with data sets
% %%% stochastic phase oscillators for comparing the original and extended
% %%% DCM for phase coupling
% % inputfilename = ['stpo_dataset4_compN15_R' num2str(inindex) '.mat'];
% % inputfilename = ['stpo_dataset4_compN15_noR' num2str(inindex) '.mat'];
% 
% 
% %%% Neural mass model data set
% pfx  = '..\SimNMM\'; % prefix, the folder with data sets
% %%% Hilbert with noise, Ks1 = 400, Ks2 = 800, on-off, with rand init
% %%% conditions:
% inputfilename = ['outSimJR2_H_test_onoff_noise_107_' num2str(inindex) '.mat'];


%% load input data
% load([pfx inputfilename]);
load(inputfilename); % the file name should include the full or relativ path

% disp(['loaded: ' pfx inputfilename]);
disp(['loaded: ' inputfilename]);

y = S{1,1}.xx; % trials. Only the first set is considered.

% only one trial
% y{1} = S{1,1}.xx{2}; % trial

t = S{1,1}.t; % time variable

% the time variable for the data set of coupled phases oscillators can be 
% different from the one for the original generated data set.
if isfield(S{1,1},'tphi')
    t = S{1,1}.tphi;
end

% correct sizes
if size(y{1},1)~=size(t,1)
    t =t';
end

Ntr = length(y); % number of trials


%% swap two regions to test to the stability of the scripts
%%% comment this part if unnecessary
% for itr=1:Ntr
%     ybuf = y{itr}(:,1);
%     y{itr}(:,1) = y{itr}(:,2);
%     y{itr}(:,2) = ybuf;
% end

%% Here we call different functions to test the data set

% % plot derivatives vs observable phase
% figure(450);
% for itr = 1:Ntr
%     sp1 =subplot(2,1,1);
%     plot(sp1,mod(y{itr}(1:end-1,1),2*pi),diff(y{itr}(:,1))./diff(t),'b.');
%     hold(sp1,'on');
%     sp2 = subplot(2,1,2);
%     plot(sp2,mod(y{itr}(1:end-1,2),2*pi),diff(y{itr}(:,2))./diff(t),'r.');
%     hold(sp2,'on');
% end
% xlim([0 2*pi]);
% hold(sp1,'off');
% hold(sp2,'off');

%% Calculate derivatives 
[dthetas,uphit,thetas,freq] = derivs(y,t); 

%% Approximate distribution with Fourier series (using weighting)
if Nsig == 0 % set default value of Nsig, if it is not given. 
    Nsig = 4;
end
[sc,ss] = appxSigma(y,Nsig); %%%  finds the F.coefficients of eta function.
                              %%% appxSigma is called from ModComp

% Calculate the Fourier coefficients of the inverse function, i.e. of rho
% function
[rc,rs] = invcoef(sc,ss,1000,Nsig); % this function is called from ModComp
    
Nbins = 20;
[ntheta, xbins] = distrPPhase3(y,Nbins); % this function calculates the distribution
                                         % of the protophase using weighted function eta
                                         % with unknown period.
[sigma,uphis] = calcTransf2(sc,ss,1000); % calculate transformation function sigma
[rho,uphir] = calcTransf2(rc,rs,1000); % calculate transformation function rho
uther = transfv([uphir uphir],rc,rs); % calculate theta out of uniform distributed phase (phi)
                                      % this function is called from ModComp

%% plot distribution of the observable phase and thier derivatives, sigma and rho
figure(100);
set(gcf,'Position',[2,346,524,434],'Color','w');
for ii=1:2
        subplot(2,2,ii);
        bar(xbins,ntheta(:,ii),'b');
        hold on;
        plot(uphis,sigma(:,ii),'r-');
        hold off;
        xlim([0 2*pi]);
        xlabel(['\theta_' num2str(ii)]);
        ylabel(['distr$(\theta_' num2str(ii) '),\sigma_' num2str(ii) '(\theta_' num2str(ii) ')$'],'Interpreter','Latex');
        % title(['Osc.' num2str(ii)]);
        
        subplot(2,2,ii+2);
        plot(thetas(:,ii),1./dthetas(:,ii),'b.');
        hold on;
        plot(uphis,sigma(:,ii)./freq(ii),'r-');
        hold off;
        xlim([0 2*pi]);
        xlabel(['\theta_' num2str(ii)]);
        ylabel(['$\frac{dt}{d\theta_' num2str(ii) '}(\theta_' num2str(ii) '),\sigma_' num2str(ii) '(\theta_' num2str(ii) ')/\omega_' num2str(ii) '$'],'Interpreter','Latex');
%         
%         subplot(3,2,ii+4);
%         plot(thetas(:,ii),dthetas(:,ii),'k.');
%         hold on;
% %         plot(uphir,rho(:,ii)*freq(ii),'r-','LineWidth',2); % rho(phi)
%         plot(uther(:,ii),rho(:,ii)*freq(ii),'b--','LineWidth',2); % rho(theta(phi))
%         hold off;
%         xlim([0 2*pi]);
%         xlabel('\theta');
%         ylabel('$\frac{d\theta}{dt},\rho(\theta)\omega$','Interpreter','Latex');        
end

% %%% the distribution vs the resiprocal of the derivatives. 
% figure(1001);
% set(gcf,'Position',[20,530,560,235],'Color','w');
% for ii=1:2
%         subplot(1,2,ii);
%         bar(xbins,ntheta(:,ii),'b');
%         hold on;
%         plot(thetas(:,ii),freq(ii)./dthetas(:,ii),'r.');         
%         plot(uphis,sigma(:,ii),'k--','LineWidth',2);
%         hold off;
%         xlim([0 2*pi]);
%         xlabel('\theta');
%         ylabel('$\textrm{distr}(\theta),\sigma(\theta), \omega \frac{dt}{d\theta}$','Interpreter','latex');
%         
% %
% %         hold on;
% %         plot(uphis,sigma(:,ii)/freq(ii),'r-');
% %         hold off;
% %         xlim([0 2*pi]);
% end

%% plot all trials
hfh3 = figure(103);
set(hfh3,'Name','Plotting all trials (modular)');
set(hfh3,'Position',[2,60,525,200],'Color','w');
for ii=1:2
        subplot(1,2,ii);
        for itr = 1:Ntr
            yth = mod(y{itr}(:,ii),2*pi);
%             jind = find(abs(diff(yth))>pi); % jump indexes
%             jind = [0;jind;length(yth)]; %% add last point
%                 for ij = 1:(length(jind)-1)
%                     plot(1:(jind(ij+1)-jind(ij)),yth((jind(ij)+1):jind(ij+1)),'r-'); 
%                     hold on;
%                 end
            plot(t,yth,'r.'); hold on;  
%             plot(t,y{itr}(:,ii),'r.'); hold on;  
        end
        hold off;
        ylim([0 2*pi]);
        xlim([0 max(t)]);
        xlabel('t,time');
        ylabel(['$\theta_' num2str(ii) '(t)$'],'Interpreter','latex');
end

%% Transformation
% if the transformation of the initial observables is enabled
if istransf
%     %%%!!! test !!!!
%     %%% set coefficients to known ones from sim.rc and sim.rs
%     [sim.sc, sim.ss] = invcoef2(sim.rc,sim.rs,1000);
%     %%%!!! end test !!!
    
    %%%<><><> transform from protophase to phase for all trials <><><>
    for n=1:Ntr
%         y{n} = transfv(y{n},sim.sc,sim.ss); % !!! test !!!
        y{n} = transfv(y{n},sc,ss); % original code 
    end
    %%%<><><>   <><><>%%%
    
    %%% recalculate derivatives
    [dthetas,uphit,thetas,freq] = derivs(y,t); %%% should be overwritten
end


%% Fiting the surface
% remove constant part, to make a(0,0) smaller (almost zero)
dthetas = dthetas - repmat(mean(dthetas),[size(dthetas,1),1]);
% create uniform mesh
dth = 0.05;
[th1,th2] = meshgrid((dth:dth:2*pi-dth));

disp('Start fitting the surface.');

%%% Parameters of the fit function: nearestinterp is stable
% deffittype = 'cubicinterp';
deffittype = 'nearestinterp';
% deffittype = 'linearinterp';
% deffittype = 'biharmonicinterp';
% % deffittype = 'smoothingspline';
% deffittype = 'lowess';
% deffittype = 'loess';
% deffittype = 'poly23';
% deffittype = 'poly11';
% 

try
    % In some versions of the matlab this functions does not work
    % Therefore, we run it within try
    sf1 = fit(thetas,dthetas(:,1),deffittype);
    sf2 = fit(thetas,dthetas(:,2),deffittype);

catch
    
    % Using scatteredInterpolate: realeased in  R2013a. Tested in 2014b
    sf1 = scatteredInterpolant(thetas(:,1),thetas(:,2),dthetas(:,1));
    sf2 = scatteredInterpolant(thetas(:,1),thetas(:,2),dthetas(:,2));
end


dthfit1 = sf1(th1,th2);
dthfit2 = sf2(th1,th2);
disp('Finished fitting.');


%%% plot the surfaces and the observable phases as scatter plot.
mdth = max(max(abs(dthetas)));
figure(101);
set(gcf,'Renderer','opengl');
set(gcf,'Position',[527,495,560,285],'Color','w');
% for the first region
subplot(1,2,1);
scatter3(thetas(:,1),thetas(:,2),dthetas(:,1),36,'r.');
hold on;
surf(th1,th2,dthfit1,'LineStyle','none');
caxis([-mdth mdth]);
hold off;
xlabel('\theta_1');
ylabel('\theta_2');
zlabel('d\theta_1/dt');
xlim([0 2*pi]);
ylim([0 2*pi]);
zlim([-mdth mdth]);

% for the second region
subplot(1,2,2);
scatter3(thetas(:,1),thetas(:,2),dthetas(:,2),36,'r.');
hold on;
surf(th1,th2,dthfit2,'LineStyle','none');
caxis([-mdth mdth]);
hold off;
xlabel('\theta_1');
ylabel('\theta_2');
zlabel('d\theta_2/dt');
xlim([0 2*pi]);
ylim([0 2*pi]);
% zlim(2*[-mdth mdth]);
zlim([-mdth mdth]);
colormap(bone);

%% Finding the Fourier coefficients of the surfaces
% Nsp = 101;
Nsp = ceil(size(th1,1)/2)*2+1; % make it always odd number.
i0 = ceil(Nsp/2); % index of the zero coefficients

disp('Calculate fft2');

Ydth1=fft2(dthfit1,Nsp,Nsp);
Ydth2=fft2(dthfit2,Nsp,Nsp);

disp('Finished fft2');

% Shift the coefficients to the center and normalize
%%% renorm coefficients: dphi1*dphi2 = (2pi/100)^2 =
%%% pi*pi/2500=pi*pi*4.0e-4
dr = (1/Nsp)^2;
spec1 = fftshift(Ydth1)*dr;
spec2 = fftshift(Ydth2)*dr;

% We consider only plus-minus Nn coefficients. We also assume that the
% coefficients are of real-values functions and ignore the imaginary parts.

Nn = 5;
fnm1 = spec1(i0-Nn:i0+Nn,i0-Nn:i0+Nn); 
fnm2 = spec2(i0-Nn:i0+Nn,i0-Nn:i0+Nn);
i0f = Nn+1;

% Define labels
ticks = (1:2*Nn+1);
tickslbl = cellstr(num2str((-Nn:Nn)'));
xticks = repmat(ticks,[length(ticks), 1])-0.5;
yticks = repmat(ticks',[1, length(ticks)])-0.5;

%% Ploting the spectrum
% mxfnms = max(max(abs([fnm1,fnm2])));
% hfh2 = figure(102);
% set(hfh2,'Renderer','Painters','Position',[200 200 800 400],'Color',[1 1 1]);
% %%% here we reflect fnm1 to be consistent with the formula of Q^(n,m)_{i,j}
% fnm1 = transpose(fnm1);
% subplot(1,2,1); 
% imagesc(abs(fnm1),[0 mxfnms]);  
% % pcolor(abs(fnm1)); caxis([0 mxfnms]);
% colorbar; title('$|F^{(n,m)}_{1,2}|$','Interpreter','latex');
% set(gca,'XTick',ticks,'XTickLabel',tickslbl,'YTick',ticks,'YTickLabel',tickslbl);
% xlabel('n'); ylabel('m','Rotation',0,'Position',[-1.4,6.4,1.0]);
% axis(gca,'square');
% subplot(1,2,2); 
% imagesc(abs(fnm2),[0 mxfnms]);
% % pcolor(xticks,yticks,abs(fnm2)); caxis([0 mxfnms]);
% colorbar; title('$|F^{(n,m)}_{2,1}|$','Interpreter','latex');
% set(gca,'XTick',ticks,'XTickLabel',tickslbl,'YTick',ticks,'YTickLabel',tickslbl); 
% xlabel('n'); ylabel('m','Rotation',0,'Position',[-1.4,6.4,1.0]);
% axis(gca,'square');
% % colormap(bone);
% colormap(flipud(bone));

%%% ---------------------------------------------
%%% the same figure as 102 but plotted using pcolor
%% plot spectrum 2
mxfnms = max(max(abs([fnm1,fnm2])));
hfh21 = figure(121);
set(hfh21,'Renderer','Painters','Position',[527 185 560 225],'Color',[1 1 1]);

Nn2 = 2*Nn+1; % the size of the matrices fnm1 and fnm2. 

% create extended matrices
xfnm1 = zeros(Nn2+1,Nn2+1);
xfnm2 = zeros(Nn2+1,Nn2+1);

% abs of fnms
afnm1 = abs(fnm1);
afnm2 = abs(fnm2);

% set left bottom corner equal to fnm's
xfnm1(1:Nn2,1:Nn2) = afnm1;
xfnm2(1:Nn2,1:Nn2) = afnm2;

% % flip the matrices upside down
% xfnm1 = flipud(xfnm1);
% xfnm2 = flipud(xfnm2);
% 
% % and left to right
% xfnm1 = fliplr(xfnm1);
% xfnm2 = fliplr(xfnm2);

subplot(1,2,1); 
h1 = pcolor(xfnm1); caxis([0 mxfnms]);
% set(h1,'LineStyle','none','EdgeColor','none'); 
set(h1,'LineStyle','-','EdgeColor',[0.5 0.5 0.5]); 
colorbar; title('$|F^{(n,m)}_{1,2}|$','Interpreter','latex');
set(gca,'XTick',ticks+0.5,'XTickLabel',tickslbl,...
    'YTick',ticks+0.5,'YTickLabel',tickslbl);
xlabel('n'); ylabel('m','Rotation',0,'Position',[-1.4,6.4,1.0]);
axis(gca,'square');

subplot(1,2,2); 
h2 = pcolor(xfnm2); caxis([0 mxfnms]);
% set(h2,'LineStyle','none','EdgeColor','none'); 
set(h2,'LineStyle','-','EdgeColor',[0.5 0.5 0.5]); 
colorbar; title('$|F^{(n,m)}_{2,1}|$','Interpreter','latex');
set(gca,'XTick',ticks+0.5,'XTickLabel',tickslbl,...
    'YTick',ticks+0.5,'YTickLabel',tickslbl); 
xlabel('n'); ylabel('m','Rotation',0,'Position',[-1.4,6.4,1.0]);
axis(gca,'square');
% colormap(bone);
colormap(flipud(bone));
% colormap(jet);
% colormap(flipud(gray));
%%% ----------------------------------------

%%% calculate different coefficients 
f_nm1 = flipud(fnm1); % f1_{-n,m}
fn_m1 = fliplr(fnm1); % f1_{n,-m}
f_n_m1 = fliplr(f_nm1); % f1_{-n,-m}

f_nm2 = fliplr(fnm2);
fn_m2 = flipud(fnm2);
f_n_m2 = flipud(f_nm2);

%%% calculate real-valued coefficients
anm1 = ( fnm1+f_nm1+fn_m1+f_n_m1);
bnm1 = (-fnm1+f_nm1-fn_m1+f_n_m1);    
cnm1 = (-fnm1-f_nm1+fn_m1+f_n_m1);
dnm1 = (-fnm1+f_nm1+fn_m1-f_n_m1);
a1 = real(anm1(i0f:i0f+Nn,i0f:i0f+Nn));
b1 = imag(bnm1(i0f:i0f+Nn,i0f:i0f+Nn));
c1 = imag(cnm1(i0f:i0f+Nn,i0f:i0f+Nn));
d1 = real(dnm1(i0f:i0f+Nn,i0f:i0f+Nn));

anm2 = ( fnm2+f_nm2+fn_m2+f_n_m2);
bnm2 = (-fnm2+f_nm2-fn_m2+f_n_m2);    
cnm2 = (-fnm2-f_nm2+fn_m2+f_n_m2);
dnm2 = (-fnm2+f_nm2+fn_m2-f_n_m2);
a2 = real(anm2(i0f:i0f+Nn,i0f:i0f+Nn));
b2 = imag(bnm2(i0f:i0f+Nn,i0f:i0f+Nn));
c2 = imag(cnm2(i0f:i0f+Nn,i0f:i0f+Nn));
d2 = real(dnm2(i0f:i0f+Nn,i0f:i0f+Nn));

%% plot real-valued coefficients
mxcoef = max(max([a1,b1,c1,d1,a2,b2,c2,d2]));
mxclim=mxcoef; %-mod(mxcoef,10)+5;
ticksnm = (1:Nn+1);
ticksnmlbl = cellstr(num2str((0:Nn)'));
% for the first region
hfh11 = figure(111);
set(hfh11,'Renderer','Painters','Position',[1090 460 445 320],'Color',[1 1 1]);
subplot(2,2,1); imagesc(a1,[-mxclim mxclim]); axis(gca,'square'); title('a_1'); colorbar;
set(gca,'XTick',ticksnm,'XTickLabel',ticksnmlbl,'YTick',ticksnm,'YTickLabel',ticksnmlbl);
xlabel('n'); ylabel('m');
subplot(2,2,2); imagesc(b1,[-mxclim mxclim]); axis(gca,'square'); title('b_1'); colorbar;
set(gca,'XTick',ticksnm,'XTickLabel',ticksnmlbl,'YTick',ticksnm,'YTickLabel',ticksnmlbl);
xlabel('n'); ylabel('m');
subplot(2,2,3); imagesc(c1,[-mxclim mxclim]); axis(gca,'square'); title('c_1'); colorbar;
set(gca,'XTick',ticksnm,'XTickLabel',ticksnmlbl,'YTick',ticksnm,'YTickLabel',ticksnmlbl);
xlabel('n'); ylabel('m');
subplot(2,2,4); imagesc(d1,[-mxclim mxclim]); axis(gca,'square'); title('d_1'); colorbar;
set(gca,'XTick',ticksnm,'XTickLabel',ticksnmlbl,'YTick',ticksnm,'YTickLabel',ticksnmlbl);
xlabel('n'); ylabel('m');
colormap(bone);
% colormap(flipud(bone));

% for the second region
hfh12 = figure(112);
set(hfh12,'Renderer','Painters','Position',[1090 50 445 320],'Color',[1 1 1]);
subplot(2,2,1); imagesc(a2,[-mxclim mxclim]); axis(gca,'square'); title('a_2'); colorbar;
set(gca,'XTick',ticksnm,'XTickLabel',ticksnmlbl,'YTick',ticksnm,'YTickLabel',ticksnmlbl);
xlabel('n'); ylabel('m');
subplot(2,2,2); imagesc(b2,[-mxclim mxclim]); axis(gca,'square'); title('b_2'); colorbar;
set(gca,'XTick',ticksnm,'XTickLabel',ticksnmlbl,'YTick',ticksnm,'YTickLabel',ticksnmlbl);
xlabel('n'); ylabel('m');
subplot(2,2,3); imagesc(c2,[-mxclim mxclim]); axis(gca,'square'); title('c_2'); colorbar;
set(gca,'XTick',ticksnm,'XTickLabel',ticksnmlbl,'YTick',ticksnm,'YTickLabel',ticksnmlbl);
xlabel('n'); ylabel('m');
subplot(2,2,4); imagesc(d2,[-mxclim mxclim]); axis(gca,'square'); title('d_2'); colorbar;
set(gca,'XTick',ticksnm,'XTickLabel',ticksnmlbl,'YTick',ticksnm,'YTickLabel',ticksnmlbl);
xlabel('n'); ylabel('m');
colormap(bone);

end
