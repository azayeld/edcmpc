%%% The script tests the eDCM PC modeling part with synthetic dataset.
%%% (The modelling part has the suffix _po.)
%%% The script works with several collection of data sets, i.e. every data
%%% set has several, independently generated, collection of data sets.
%%% This is done for statistical test. 
%%% 
%%% The elements of this code is taken from 
%%% the script dcm_fit_finger.m from
%%% SPM DCM  toolbox of Wellcome Trust Centre for Neuroimaging
%%% written by Will Penny. 
%%%
%%% -----------------------------------------------------------
%%% AY 20.11.2017
%%% -----------------------------------------------------------
%%% The final name used in the article is the extended DCM for phase 
%%% coupluing (eDCM PC).
%%% Last modification: AY 18.02.2019 edited comments.

%%% Synthetic data sets:
%%% 1. The Jansen and Rit neural mass model. The input files 
%%% were created by simJR2set.m and it's subsequent derivatives. See the folder SimNMM. 
%%%
%%% 2. The coupled stochastic phase oscillators model. The input files were
%%% created by simSt2PhOsc.m and it's derivatives. See the folder SimPhOsc.
%%% 

clear;

% suffix of the output file
isslowphase = 0; % not used anymore
% sfx = 'sp_'; % slow phases
% sfx = 'new_U_J_pr_';
% pfx = '../NMModels/';
pfx = ''; % 
sfx = 'nsig1_'; % is related to the number of Fourer terms in the transformation function.

istransf = 0; % do transformation of the data (1) or not (0), 
% i.e. the observables are transfered to true phases using the
% approximation of the sigma function. This option is not used.


if istransf
    sfx = ['tP2P_' sfx]; % suffix transformation from protophase to phase.
end

addpath('../ModComp/');

%% Different cases: 
% 1 - the data set was generated with non-linear distortion.
% 2 - the data set was generated without non-linear distortion.
for icase = 1:2
    % icase = 1; % uncomment for fixed icase.
    
    % inindex defines different coupling configurations:
    % 1 - 2nd to 1st
    % 2 - 1st to 2nd
    % 3 - bidirectional
    % the loop can be commented for the case when only one coupling
    % configuration is fitted.    
    for inindex = 1:3 % cycle over inindex
    %     inindex = 1; % uncomment for fixed inindex.
        %%% dataset to compare the original DCM PC and the extended DCM PC
        pfx = '..\SimPhOsc\'; %
        %%% dataset with only sine uni-directional coupling
        if icase == 1
            inputfilename = ['stpo_dataset4_compN15_R' num2str(inindex) '.mat']; % with transformation
        else % icase==2
            inputfilename = ['stpo_dataset4_compN15_noR' num2str(inindex) '.mat']; % without transformation
        end

    %     %%% fitting with stronger noise, only sine uni-directional
    %     if icase == 1
    %         inputfilename = ['stpo_dataset5_test_compN15_R' num2str(inindex) '.mat']; % with transformation
    %     else % icase==2
    %         inputfilename = ['stpo_dataset5_test_compN15_noR' num2str(inindex) '.mat']; % without transformation
    %     end

        %% load input data
        load([pfx inputfilename]);

        disp(['loaded: ' inputfilename]);

        %% set parameters for dcm 
        Nr = 2; % number of regions
        Nu = 0; % number of between-trial effects

        %% select data set
        for iset = 1:sim.Nset
            tic
            disp(['set: ' num2str(iset)]);
            
            Ss = S{iset};
            sfx2 = ['setn' num2str(iset) '_']; % suffix for different sets.

            try isfield(Ss,'Ntrials');
                Nt = sim.Ntrials; % the number of trials
            catch
                Nt = sim.Ntrial;
            end


            %%% !!! sim.dt is only correct for the phase data, 
            %%% but for the original activity (the original observable)
            %%% we use the definition in catch 
            try isfield(sim,'dt');
                fs = 1/sim.dt; % sampling rate
                dt = sim.dt;
            %     secs = sim.T;
            catch
                fs = intP.N/intP.Ttotal;
                dt = 1/fs;
                sim.dt = dt;
            %     secs = intP.Ttotal;
            end

            try isfield(Ss,'tphi');
                N = length(Ss.tphi); % the time length can be different for the phase data.
                tphi = Ss.tphi;
            catch 
                N = length(Ss.t);
                tphi = Ss.t;
            end  

            %%% we create a DCM structure with trail data
            if ~isfield(Ss,'phiy')
                Ss.phiy = Ss.xx';
            end

            DCM=[];
            for n=1:Nt,
                DCM.xY.y{n}=Ss.phiy{n,1}; % should be phases (N*2);
            end
            DCM.xY.dt=sim.dt;
            DCM.xY.pst=(0:N-1)'*dt; % time

            [dthetas,uphis,thetas,mFreq] = derivs(DCM.xY.y,DCM.xY.pst); % finds derivatives of the observable phases.
            M  = [];
            M.freq = mFreq'; % mean(dthetas)'; % define mean frequencies  
            M.isslowphase = isslowphase; % we reconstruct with slow phase. Not used.

            try isfield(sim.p,'fb')
                M.fb = sim.p.fb;
            catch
                try isfield(sim,'fb')
                    M.fb = [sim.fb; sim.fb];
                catch
                    % the width of the frequency band
                    M.fb = max(abs(dthetas - repmat(mean(dthetas),[size(dthetas,1),1])))';
                end
            end

            DCM.options.Fdcm =repmat(M.freq,1,2) + [-M.fb M.fb];
            DCM.options.istransf = istransf;

            % define coupling coefficients:
            Nsig = 1; % The order of F.terms for transformation
            Nq = 1;   % The order of F.terms for coupling

            M.Nsig = Nsig;
            M.Nq = Nq;

            % Initial phases from all trials
            for n=1:Nt
                phi0 = DCM.xY.y{n}(1,:);
                trial{n}.x= phi0';
            end

            U.u=zeros(N,1);
            U.dt=sim.dt;
            U.tims=(0:N-1)'*dt;
            U.X=sparse(1,0);

            M.trial=trial;
            M.x=zeros(Nr,1); 
            M.ns=N; % number of time samples

            % Define DCM structure
            DCM.A=[0 1; 1 0]; % bidirectional
            DCM.B{1} = zeros(size(DCM.A)); % after & operation it remains the same.

            U.X=zeros(Nt,1);
            DCM.xU=U;
            DCM.xU.oldX=U.X;

            DCM.M=M;

            % profile clear
            % profile on
            DCM = spm_dcm_po(DCM); 
            % profile off 
            % profile viewer

            save([pfx 'dcm_' sfx sfx2 inputfilename],'DCM');
            elapsedTime = toc;
            disp(['The results are saved in ' ['dcm_' sfx sfx2 inputfilename] '. Elapsed time ' num2str(elapsedTime)]);

        end %%% end of loop by set
    end %%% end loop by inindex
end %% end of modification for differnet cases