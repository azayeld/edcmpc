function foutname = fFit_eDCM_PC(fitpar)
%%% The function fits synthetic dataset with eDCM PC modeling part.
%%% (The modelling part has the suffix _po.)
%%% The function works with single data set.
%%% Input parameters:
%%% - finname is the name of the input file. The file should contain the S
%%% array of cells where the data sets are stored.
%%% - fitpar is the sturcture of the fitting parameters:
%%%     - inputfilename
%%%     - inputfilepath
%%%     - istransf 
%%%     - Nr is the number of regions.
%%%     - Nu is the number of between trial effects.
%%%     - isslowphase is a flag for calculation in slow phases, not used.
%%%     - A=[0 1; 1 0]; the coupling matrix.
%%%     - B{1} = zeros(size(A)); % trial effect matrix.
%%%     - Nsig is the number of the Fourier terms for the Sigma function.
%%%     - Nq is the number of the Fourier terms for the coupling function q.
%%%     - issets is the flag to perform the fitting for all sets in the
%%%     data sets. The results for every set will be saved in separate
%%%     output file.
%%% Output
%%%     - foutname is the name of the output file. 
%%% This script is the modification of the scripts Fit_eDCM_PC.m and 
%%% Fit_eDCM_PC_sets.m, done for the demonstration script.
%%% Some elements of this code is taken from 
%%% the script dcm_fit_finger.m from
%%% SPM DCM  toolbox of Wellcome Trust Centre for Neuroimaging
%%% written by Will Penny. 
%%%
%%% --------------------------------------------------------------------
%%% AY 12.03.2021 


pfx = fitpar.inputfilepath; % 
sfx = ['nsig' num2str(fitpar.Nsig) '_']; 
              % nsig#_, where # is related to the number of Fourer terms in
              % the transformation function.

if fitpar.istransf
    sfx = ['tP2P_' sfx]; % suffix transformation from protophase to phase.
end

addpath('../ModComp/'); 

        %% load input data
        load([pfx fitpar.inputfilename]);

        disp(['loaded: ' fitpar.inputfilename]);
                % loaded variables: 
                %   S array of structure.
                %   sim the structure of the simulation parameters.

        %% set parameters for dcm 
        Nr = fitpar.Nr; % 2 number of regions
        Nu = fitpar.Nu; % 0 number of between-trial effects

        %% select data set
        for iset = 1:sim.Nset
            tic
            disp(['set: ' num2str(iset)]);
            
            Ss = S{iset};
            sfx2 = ['setn' num2str(iset) '_']; % suffix for different sets.

            try isfield(Ss,'Ntrials');
                Nt = sim.Ntrials; % the number of trials
            catch
                Nt = sim.Ntrial;
            end


            %%% !!! sim.dt is only correct for the phase data, 
            %%% but for the original activity (the original observable)
            %%% we use the definition in catch 
            try isfield(sim,'dt');
                fs = 1/sim.dt; % sampling rate
                dt = sim.dt;
            %     secs = sim.T;
            catch
                fs = intP.N/intP.Ttotal;
                dt = 1/fs;
                sim.dt = dt;
            %     secs = intP.Ttotal;
            end

            try isfield(Ss,'tphi');
                N = length(Ss.tphi); % the time length can be different for the phase data.
                tphi = Ss.tphi;
            catch 
                N = length(Ss.t);
                tphi = Ss.t;
            end  

            %%% we create a DCM structure with trail data
            if ~isfield(Ss,'phiy')
                Ss.phiy = Ss.xx';
            end

            DCM=[];
            for n=1:Nt,
                DCM.xY.y{n}=Ss.phiy{n,1}; % should be phases (N*2);
            end
            DCM.xY.dt=sim.dt;
            DCM.xY.pst=(0:N-1)'*dt; % time

            [dthetas,uphis,thetas,mFreq] = derivs(DCM.xY.y,DCM.xY.pst); % finds derivatives of the observable phases.
            M  = [];
            M.freq = mFreq'; % mean(dthetas)'; % define mean frequencies  
            M.isslowphase = fitpar.isslowphase; % we reconstruct with slow phase. Not used.

            try isfield(sim.p,'fb')
                M.fb = sim.p.fb;
            catch
                try isfield(sim,'fb')
                    M.fb = [sim.fb; sim.fb];
                catch
                    % the width of the frequency band
                    M.fb = max(abs(dthetas - repmat(mean(dthetas),[size(dthetas,1),1])))';
                end
            end

            DCM.options.Fdcm =repmat(M.freq,1,2) + [-M.fb M.fb];
            DCM.options.istransf = fitpar.istransf;

            % define coupling coefficients:
            M.Nsig = fitpar.Nsig;   % 1, The order of F.terms for sigma function
            M.Nrho = fitpar.Nrho;   % 1, The order of F.terms for rho function
            M.Nq = fitpar.Nq;       % 1, The order of F.terms for coupling

            % Initial phases from all trials
            for n=1:Nt
                phi0 = DCM.xY.y{n}(1,:);
                trial{n}.x= phi0';
            end

            U.u=zeros(N,1);
            U.dt=sim.dt;
            U.tims=(0:N-1)'*dt;
            U.X=sparse(1,0);

            M.trial=trial;
            M.x=zeros(Nr,1); 
            M.ns=N; % number of time samples

            % Define DCM structure
            DCM.A=fitpar.A; % the coupling matrix, e.g. [0 1; 1 0] is bidirectional
            DCM.B{1} = zeros(size(DCM.A)); % after & operation it remains the same.

            U.X=zeros(Nt,1);
            DCM.xU=U;
            DCM.xU.oldX=U.X;

            DCM.M=M;

            % profile clear
            % profile on
            DCM = spm_dcm_po(DCM); 
            % profile off 
            % profile viewer

            save([pfx 'dcm_' sfx sfx2 fitpar.inputfilename],'DCM');
            elapsedTime = toc;
            disp(['The results are saved in ' ['dcm_' sfx sfx2 fitpar.inputfilename] '. Elapsed time ' num2str(elapsedTime)]);

        end %%% end of loop by set
        foutname = [pfx 'dcm_' sfx sfx2 fitpar.inputfilename];
end