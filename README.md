# Extended Dynamic Causal Modeling for Phase Coupling (eDCM PC)

Collection of codes (MATLAB scripts) of the extended Dynamic Causal Modelling for phase coupling.

Please refer to the following publication [Yeldesbay et al. 2019][Yeldesbay2019]:

Yeldesbay, A., Fink, G. R., & Daun, S. (2019). Reconstruction of effective connectivity in the case of asymmetric phase distributions. Journal of Neuroscience Methods, 317(February), 94–107. https://doi.org/10.1016/j.jneumeth.2019.02.009

## Documentation

The [documentation](Docs/Doc.md) for the package can be found in the folder `/Docs`. 
Information about installation can be found [here](Docs/Doc.md#installation). 

# Structure of the folders
- `/ModComp` contains the modeling component of eDCM PC and other additional functions used in the package;

- `/Fitting` contains scripts that fit different simulated data sets using eDCM PC;

- `/PlotRes` contains scripts that plot the results;

- `/TestDataset` contains the scripts for preliminary testing the data sets, to find some parameters for running eDCM PC;

- `/Demos` contains the demonstration scripts;

- `/SimPhOsc` contains scripts for simulation of two coupled phase oscillators and the results of the fitting with original DCM PC and extended DCM PC;

- `/SimNMM` contains scripts for simulation of two coupled neural mass models and the results of the fitting with eDCM PC;

- `/Docs` contains the documentation files.


[Yeldesbay2019]: https://doi.org/10.1016/j.jneumeth.2019.02.009 "Yeldesbay, A., Fink, G. R., & Daun, S. (2019). Reconstruction of effective connectivity in the case of asymmetric phase distributions. Journal of Neuroscience Methods, 317(February), 94–107."
