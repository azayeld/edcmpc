% This function calculates the coupling function for two coupled phase 
% oscillators of two regions. 
% The coefficients a,b,c, and d are of the size Nnm x Nnm.  
function Q = calcQphij(a,b,c,d,psii,psij)
        Nnm=size(a,3);
        nmx = (1:Nnm)'*[psii psij]; % should be Nnm x Nr matrix
        cosnx = cos(nmx);   % 
        sinnx = sin(nmx);   % is the size of 2xNm
        Q = cosnx(:,1)'*(a*cosnx(:,2) + b*sinnx(:,2))...
          + sinnx(:,1)'*(c*cosnx(:,2) + d*sinnx(:,2));
end