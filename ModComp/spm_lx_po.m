function [G] = spm_lx_po (P,M)
% Linear observation function for coupled phase oscillators model.
% FORMAT [G] = spm_lx_po (P,M)
%
% G     Observations y = Gx
% this is the same as the function spm_lx_phase.m
% from SPM DCM toolbox of Wellcome Trust Centre for Neuroimaging
% by Will Penny
% ---------------------------------------------------------------
% Last modification AY 19.02.2018
% Edited comments AY 18.02.2019
% ---------------------------------------------------------------

Nr=length(P.L);
G=eye(Nr); 