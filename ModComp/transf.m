function y = transf(x,a,b)
% This function transforms x to y using the Fourier coefficients a and b of the
% transformation function (integral of the rho or sigma functions).
% input:
% x is a column of the phase values of the size of Nr
% a and b are the cosine and the sine coefficients of the size of [Nr x Ns]
% output:
% y has the same size as x.
% Difference to transfv is that x and y are not vectors. 
% Last modification AY 19.02.2018
% Edited comments AY 18.02.2019

[Nr,Ns] = size(a); % Nr is the number of regions, Ns is the number of coefficients

nmx = x*(1:Ns); % is of the size of Nnm x Nr
rn = repmat(1./(1:Ns),Nr,1); % reciprocal of n=1:Ns
cosnx = cos(nmx); % cosine of [phi1 2phi1 3phi1 ...; 
                  %            phi2 2phi2 3phi2 ...
                  %            ...                 ];
sinnx = sin(nmx); % is the size of NxNm

% calculate Rho - the integral
y = x + sum((sinnx.*a+ (1-cosnx).*b).*rn,2);
end