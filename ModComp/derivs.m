function [dthetas,uphis,thetas,mFreq] = derivs(y,pst)
%%% finds the derivatives of the observable phases as a function of 
%%% a uniform distributed phase (time) using the data set (observable/proto phase)
%%% stored in stored in y structure as a function of a uniform distributed
%%% phase (time).
%%% The results of this function can be compared with rho if the oscillator
%%% has no input (the case without coupling). 
%%% Input:
%%%     - y is an array of trials (in the same format as DCM.xY.y)
%%%     - pst is the pristimulus time.
%%% Output:
%%%     - dthetas is the derivaties of the phases for the whole the data set (as one vector).
%%%     - uphis is the values of corresponding uniformly distributed phases (also as one vector).
%%%     - thetas is the values of all corresponding phases in the data set (observable/proto phases).
%%%     - mFreq is the means frequencies
%%%
%%% AY 10.11.2017
%%% AY 08.01.2018 corrected uphis part.
%%% Last modification: AY 18.02.2019 edited comments.

%     pst = DCM.xY.pst;           % peristimulus time;
%     Nt     = length(DCM.xY.y);      % the number of trials
%     Nr     = size(DCM.xY.y{1},2);     % the number of sources
%     Ns     = size(DCM.xY.y{1},1);   % the number of samples
    Nt     = length(y);      % the number of trials
    Nr     = size(y{1},2);     % the number of sources
    Ns     = size(y{1},1);   % the number of samples
        
    % allocate space for all phases (observable/proto phases)
    thetas = zeros((Ns-1)*Nt,Nr);
    % allocate space for all derivatives of phases
    dthetas = zeros((Ns-1)*Nt,Nr);
    % allocate space for corresponding unifromly growing phases
    uphis = zeros((Ns-1)*Nt,Nr);

    % loop by trials
    for n=1:Nt          
        % recalculate indices for all phase matrices
        ib = (n-1)*(Ns-1)+1;
        ie = ib + Ns-2;
        
        for c=1:Nr % loop by regions  
            phi = y{n}(:,c); % only phase values of one region
            thetas(ib:ie,c) = mod(phi(1:end-1),2*pi); 
            dthetas(ib:ie,c) = diff(phi)./diff(pst); 
        end
    end % end loop by trials 
    
    % mean frequencies 
    mFreq = mean(dthetas);
    % loop by trials
    for n=1:Nt    
        ib = (n-1)*(Ns-1)+1;
        ie = ib + Ns-2;
        for c=1:Nr % loop by regions  
            uphi = y{n}(1,c) + (pst(1:end-1)-pst(1))*mFreq(c);
            uphis(ib:ie,c) = mod(uphi,2*pi);
        end % end loop by regions
    end  % end loop by trials
end % end of the function