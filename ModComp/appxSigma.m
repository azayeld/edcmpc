function [sc,ss] = appxSigma(y,Nsig)
%%% This function finds the approximation of the transformation
%%% sigma for given dataset stored in DCM structure. The resulting 
%%% Fourier coefficients of the sigma function is returned in sc and ss
%%% Input:
%%%     - y is an array of cells with trials. Every trial is Nr x Nsig.
%%%     - Nsig the number of terms of the Fourier series for the trasformation.
%%% The results, sc and ss, are the coefficient of the Fourier series of 
%%% the function sigma (the inverse transformation function).
%%% 
%%% ---------------
%%% AY 16.04.2018
%%% Last modification: AY 28.02.2019, edited comments.

    Nt      = length(y);      % number of trials
    [Ns,Nr] = size(y{1});     % number of samples and sources
    
    if nargin<2 
        Nsig = 8; % default number of points for uniformly distributed phases
    end
   
%   % Finding the Fourier coefficients of the forward transformation using
    % weighting procedure for every trial (calculating with eta).
    a = zeros(Nr,Nsig);  
    b = zeros(Nr,Nsig);    
    % loop by trials
    for n=1:Nt
        for c=1:Nr % loop by regions
            thetas = y{n}(:,c); 
            
            % find eta - a weight function
            ak = floor((thetas(end)-thetas(1))/(2*pi));  % the number of full periods
            dmth = mod(thetas(end)-thetas(1),2*pi); % the remainder of division 
            
            t1 = dmth/(2*pi);
            
            eta = ones(size(thetas)); % we need this line to escape 
                                      % "devision by zero" when calculating wgt.
            eta = (ak + (mod(thetas-thetas(1),2*pi)<=dmth))./(ak+t1);
            % when the condition fulfills (ak+1)/(ak+t1), otherwise ak/(ak+t1)
 
            % find the weight for every point of the trial
            wgt = 1./eta;

            % find coefficients of the forward transformation
            for k = 1:Nsig
            a(c,k) = a(c,k) + sum(wgt.*cos(k*thetas),1);
            b(c,k) = b(c,k) + sum(wgt.*sin(k*thetas),1);
            % for testing without weight, commented
            % % a(c,k) = a(c,k) + sum(cos(k*thetas),1);
            % % b(c,k) = b(c,k) + sum(sin(k*thetas),1);
            end
        end % end loop by regions
    end % end loop by trials
    a = 2*a/(Ns*Nt); 
    b = 2*b/(Ns*Nt);
    
    sc = a;
    ss = b;
end