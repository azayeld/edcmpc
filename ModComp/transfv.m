function y = transfv(x,a,b)
% This function transforms a vector x to vector y using the Fourier coefficients 
% a and b of the transformation function (integral of the rho or the sigma
% functions).
% inputs:
% x is a matrix of phase values of the size Nt x Nr. Nt is the number of
% time points. Nr the number of regions.
% a and b are the cosine and the sine coefficients of the size of [Nr x Ns]. 
% Ns is the number of Fourier terms.
% output:
% y has the same size as x.

    [Nr,Ns] = size(a); % find the number of regions and of the Fourer terms.
    
    x = x'; % for optimization
    Nt = size(x,2);
    y = zeros(size(x));

    for it = 1:Nt
        nmx = x(:,it)*(1:Ns); % is of the size of Nnm x Nr
        cosnx = cos(nmx); % cosine of [phi1 2phi1 3phi1 ...; 
                          %            phi2 2phi2 3phi2 ...
                          %            ...                 ];
        sinnx = sin(nmx); % of the size of NxNm

        % calculate Rho
        y(:,it) = x(:,it) + sum(sinnx.*a+ (1-cosnx).*b,2);
    end
    y = y';
end