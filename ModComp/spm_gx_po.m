function [y] = spm_gx_po (phi,u,P,M)
% Observable equation for a coupled phase oscillators model.
% This is non-linear trasformation function from theoretical (true phase)
% to observable phase (protophase).
% FORMAT [f,J] = spm_gx_po(phi,u,P,M)
%
% phi       state variables
% u         []
% P         model (variable) parameter structure
% M         model (fixed) parameter structure
%
% ---------------------------------------------
% Last modification AY 19.02.2018
% Comments edited AY 18.02.2019

% if the rho function is fitted, then the structure P has
% P.rs is 2D matrix (Nr,Ns)
% P.rc is 2D matrix (Nr,Ns)
% ----------------------------------- %

if isfield(P,'rs') % with transformation fitting
    Nr = size(P.rs,1);
    Ns = size(P.rs,2);
else
    Nr = size(P.dnm,1); % if we do not use transformation, 
                        % then the number of regions is extracted from dnm.
    Ns = 0;
end

% input phi should always be a column
if size(phi,1)==1 % phi is a column (not line)
    phi = phi';
end

if isfield(P,'rs') % if we do transforamtion
    nmx = phi*(1:Ns); % should be the size of Nr x Ns
    rn = repmat(1./(1:Ns),Nr,1); % reciprocal of n=1:Ns
    cosnx = cos(nmx); % cosine of [phi1 2phi1 3phi1 ...; 
                      %            phi2 2phi2 3phi2 ...
                      %            ...                 ];
    sinnx = sin(nmx); % a size of NxNm

    % calculate Rho
    y = phi + sum((sinnx.*P.rc + (1-cosnx).*P.rs).*rn,2);
else % without transformation
    y = phi;
end

end
