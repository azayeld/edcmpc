function [x] = spm_gen_po(P,M,U)
% Generate state activities for trial-specific phase-coupled activity
% FORMAT [x] = spm_gen_po(P,M,U)
%
% P - parameters
% M - model structure
% U - trial-specific effects
%
% x - states
% 
% ------------------------------------------------------------
% This is the modified version of the script spm_gen_phase.m
% ($Id: spm_gen_phase.m 2908 2009-03-20 14:54:03Z will $)
% written by % Will Penny from SPM DCM Package
%  of Wellcome Trust Centre for Neuroimaging
% -------------------------------------------------------------
% 
% last modification AY 20.02.2018

% between-trial inputs
%==========================================================================
try, X = U.X; catch, X = sparse(1,0); end

if isfield(M,'trial')
    Mx=M.x;
end

% cycle over trials
%--------------------------------------------------------------------------
for  c = 1:size(X,1)
    
    % baseline parameters
    %----------------------------------------------------------------------
    Q  = P;
    % trial-specific inputs
    %----------------------------------------------------------------------
    for i = 1:size(X,2)
          Q.anm = Q.anm + X(c,i)*Q.Banm{i};
          Q.bnm = Q.bnm + X(c,i)*Q.Bbnm{i};
          Q.cnm = Q.cnm + X(c,i)*Q.Bcnm{i};
          Q.dnm = Q.dnm + X(c,i)*Q.Bdnm{i};
    end
        
    if isfield(M,'trial')
        % Set up trial-specific initial state
        xth = M.trial{c}.x'; % protophase            
%         xph = M.trial{c}.x0'; % true phase
        if isfield(P,'rc')
            % find coefficients of the inverse transformation
%             [sc,ss] = invcoef2(P.rc,P.rs,1000);
            [sc,ss] = invcoef(P.rc,P.rs,1000,M.Nsig); % with a given precision, previous invcoef3

            xph = transf(xth,sc,ss); % transform from protophase to phase
        else
            xph = xth; 
        end
        % shift back to one step using Euler method to achive 
        % correct comparison in spm_nlsi_GN.m
        [fx,J] = spm_fx_po(xph,U,P,M);
        xph = xph-fx*U.dt; 
%         xph = xph-(M.freq+P.df)*U.dt; 
        M.x = xph';
    end
    
%     x{c,1} = spm_int_ode(Q,M,U);
    x{c,1} = spm_int_U(Q,M,U);
    
end

if isfield(M,'trial')
    M.x=Mx;
end

end



