function [pE,pC] = spm_po_priors(DCM,fb,dipfit)
% Prior moments of the extended DCM for phase coupling.
% FORMAT [pE,gE,pC,gC] = spm_po_priors(DCM,fb,dipfit,freq_prior)
%
% freq_prior  irrelevant, not used in this verstion. 
%             The value is always 'soft_freq'
%             Priors on frequency: 'soft_freq' (default), 'hard_freq'
% 
% Fields of DCM:
%
% A,B{m} - binary constraints (first two mandatory)
% fb     - frequency band 
% dipfit - prior forward model structure
%
% pE - prior expectation - f(x,u,P,M) and g(x,u,G,M)
%
% connectivity parameters
%--------------------------------------------------------------------------
%    pE.A    - trial-invariant
%    pE.B{m} - trial-dependent
% ----------------------------------------------------------------
% this program is a modification of spm_phase_priors.m
% from SPM DCM  toolbox of Wellcome Trust Centre for Neuroimaging
% written by Will Penny (% $Id: spm_phase_priors.m 3637 2009-12-11
% 16:40:15Z will $)
% ----------------------------------------------------------------
% last modification AY 20.11.2017
% Edited comments AY 18.02.2019

if nargin < 4 | isempty(freq_prior)
    freq_prior='soft_freq';
end

anm = DCM.anm;
bnm = DCM.bnm;
cnm = DCM.cnm;
dnm = DCM.dnm;

Banm = DCM.Banm;
Bbnm = DCM.Bbnm;
Bcnm = DCM.Bcnm;
Bdnm = DCM.Bdnm;
% orders
%--------------------------------------------------------------------------
Nr = size(dnm,1);                                 % number of sources 
Nq = size(dnm,3);

if isfield(DCM,'rs') % if we fit the transformation function 
%     Brc = DCM.Brc;
%     Brs = DCM.Brs;
    rc = DCM.rc;
    rs = DCM.rs;
    R = sum(sqrt(rc.*rc + rs.*rs),2); % the norm of the rho function of Nrx1
else
    R = 0; 
end

Df = fb - DCM.M.freq.*R; % the shifted frequency bands

% set the threshold for probability
p_exceed=0.001*0.5;

% for normal distribution
z_exceed=abs(spm_invNcdf(0.5*p_exceed,0,1));

% for product of two normally distributed variables approximated by bessel
% functions. 
t = -7:0.001:0.0;
pz = real(t.*(besselk(0,t).*struvem(-1,t)+besselk(1,t).*struvem(0,t))/2) + 0.5;
ipz = find([0 pz-p_exceed].*[pz-p_exceed 0]<0);
% the true value is between t(ipz-1) and t(ipz), we linearly approximate it
z1_exceed = abs(t(ipz-1) + (p_exceed-pz(ipz-1))*(t(ipz)-t(ipz-1))/(pz(ipz)-pz(ipz-1)));

%%% set priors
% -----------------------
% Frequency priors
% -----------------------
freq_prior = 'soft_freq'; % we fix the priors for soft freq, 

E.df=zeros(Nr,1);
switch freq_prior
    case 'hard_freq',
        V.df=1e-6*ones(Nr,1); % not used in this version
    case 'soft_freq',
%         new_df=(DCM.options.Fdcm(:,2)-DCM.options.Fdcm(:,1))/2;
%         new_sig=new_df/3;   
%         V.df=new_sig;
          V.df = (Df./((1+R)*z_exceed)).^2; % with rho correction
    otherwise
        disp('Unknown prior on frequencies');
        return
end

% --------------------------
% variances for coupling 
% --------------------------
prior_varq = (Df./((1+R)*z_exceed)).^2;

% --------------------------
% variances for transformation 
% --------------------------
if isfield(DCM,'rs') % if we have forward transformation
    vars1 = (Df./(DCM.M.freq*z_exceed)).^2;
    vars2 = (((1+R)*z_exceed)/z1_exceed).^2;
    prior_vars=min([vars1,vars2],[],2); % we chose the minimum of the two variances
    % Actually var2 is never used. Consider removing in the future version.
end

% Testing with different priors. Commented.
% Redefine priors for the weaker ones.
% prior_varq = (fb./((1+R)*z_exceed)).^2;
% prior_vars = (fb./(DCM.M.freq*z_exceed)).^2;

%--------------------------------------------------------------------------
% intrinsic connectivity 
%--------------------------------------------------------------------------
% for rho function 
if isfield(DCM,'rs')
    % set already approximated values of the Fourier coefficients of Rho
    E.rc  = rc; 
    E.rs  = rs;
end

% for coupling
E.anm  = zeros(size(anm));
E.bnm  = zeros(size(bnm));
E.cnm  = zeros(size(cnm));
E.dnm  = zeros(size(dnm));

% -----------------------
% define variances
% -----------------------
if isfield(DCM,'rs')
    % Every oscillator has it's own prior_var
    V.rc  = repmat(prior_vars,1,size(rc,2)); 
    V.rs  = repmat(prior_vars,1,size(rs,2));    
end

V.anm  = repmat(prior_varq,[1,Nr,Nq,Nq]).*anm;
V.bnm  = repmat(prior_varq,[1,Nr,Nq,Nq]).*bnm;
V.cnm  = repmat(prior_varq,[1,Nr,Nq,Nq]).*cnm;
V.dnm  = repmat(prior_varq,[1,Nr,Nq,Nq]).*dnm;



% input-dependent
%--------------------------------------------------------------------------
for i = 1:length(Banm)
    E.Banm{i}  = zeros(size(Banm{i}));
    E.Bbnm{i}  = zeros(size(Bbnm{i}));
    E.Bcnm{i}  = zeros(size(Bcnm{i}));
    E.Bdnm{i}  = zeros(size(Bdnm{i}));
    
    V.Banm{i}  = repmat(prior_varq,[1,Nr,Nq,Nq]).*Banm{i} & V.anm;
    V.Bbnm{i}  = repmat(prior_varq,[1,Nr,Nq,Nq]).*Bbnm{i} & V.bnm;
    V.Bcnm{i}  = repmat(prior_varq,[1,Nr,Nq,Nq]).*Bcnm{i} & V.cnm;
    V.Bdnm{i}  = repmat(prior_varq,[1,Nr,Nq,Nq]).*Bdnm{i} & V.dnm;

end

% prior moments
%--------------------------------------------------------------------------
pE   = E;
pC   = diag(sparse(spm_vec(V)));

% -------------------------------------------------------------------------
% Modified Struve function
function f=struvem(v,x,n)
    % Calculates the Modified Struve Function
    %
    % struvem(v,x) 
    % struvem(v,x,n)
    % 
    % L_v(x) is the modified struve function and n is the length of
    % the series calculation (n=100 if unspecified)
    %
    % from: Abramowitz and Stegun: Handbook of Mathematical Functions
    % 		http://www.math.sfu.ca/~cbm/aands/page_498.htm

    if nargin<3
    n=100;
    end

    k=0:n;

    x=x(:)';
    k=k(:);

    xx=repmat(x,length(k),1);
    kk=repmat(k,1,length(x));

    TOP=1;
    BOT=gamma(kk+1.5).*gamma(kk+v+1.5);
    RIGHT=(xx./2).^(2.*kk+v+1);
    FULL=TOP./BOT.*RIGHT;

    f=sum(FULL);
end


end 


