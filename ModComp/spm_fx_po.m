function [f, J] = spm_fx_po (phi,u,P,M)
% State equation for a coupled phase oscillators model
% with different frequencies. 
% FORMAT [f,J] = spm_fx_po(phi,u,P,M)
%
% phi       state variables
% u         []
% P         model (variable) parameter structure
% M         model (fixed) parameter structure
%
% f         Flow vector, dphi/dt and of dtheta/dt
% J         Jacobian, J(i,j)=df_i/dphi_j
% ---------------------------------------------
% Last modification AY 19.02.2018
% Edited comments AY 18.02.2019

% The structure of P:
% P.anm is 4D matrix (NrxNr,Nnm,Nnm)
% P.bnm is 4D matrix (NrxNr,Nnm,Nnm)
% P.cnm is 4D matrix (NrxNr,Nnm,Nnm)
% P.dnm is 4D matrix (NrxNr,Nnm,Nnm)
% If the rho function is fitted, then the structure P has
% P.rs is 2D matrix (Nr,Ns)
% P.rc is 2D matrix (Nr,Ns)
% These parameters are for gx function.
% ----------------------------------- %

if isfield(P,'dnm') 
    Nr=size(P.dnm,1);
    Nnm=size(P.dnm,3);
else
    Nr=0;
    Nnm=0;
%     Nm=0;
end

if size(phi,2)==1 % phi is a column (not line)
    phi = phi';
end

nm = (1:Nnm)';
nmx = nm*phi(1:Nr); % the size is Nnm x Nr
cosnx = cos(nmx);   % 
sinnx = sin(nmx);   % the size is Nnm x Nr

ncosnx = repmat(nm,1,Nnm).*cosnx; % n*cos(n*x),  the size is Nnm x Nr
nsinnx = repmat(nm,1,Nnm).*sinnx; % n*sin(n*x),  the size is Nnm x Nr

Q = zeros(Nr,1); % sum matrix for interaction between region
J = zeros(Nr,Nr); % Jacobian
for i=1:Nr
    % reset Qc and Qs for every region
    Qc = zeros(Nnm,1); 
    Qs = zeros(Nnm,1);
    for j=1:Nr
        if j~=i % the term with i=j is equal to 0
            anm = reshape(P.anm(i,j,:,:),[Nnm,Nnm]); % anm is 2D matrix
            bnm = reshape(P.bnm(i,j,:,:),[Nnm,Nnm]); % bnm is 2D matrix
            cnm = reshape(P.cnm(i,j,:,:),[Nnm,Nnm]); % cnm is 2D matrix
            dnm = reshape(P.dnm(i,j,:,:),[Nnm,Nnm]); % dnm is 2D matrix

            Qc = Qc + anm*cosnx(:,j) + bnm*sinnx(:,j); %  a column of size Nnm
            Qs = Qs + cnm*cosnx(:,j) + dnm*sinnx(:,j);
            
            J(i,j) = cosnx(:,i)'*(-anm*nsinnx(:,j) + bnm*ncosnx(:,j)) + ...
                     sinnx(:,i)'*(-cnm*nsinnx(:,j) + dnm*ncosnx(:,j));
        end
    end
    Q(i) = Q(i) + cosnx(:,i)'*Qc + sinnx(:,i)'*Qs;
    J(i,i) = -nsinnx(:,i)'*Qc + ncosnx(:,i)'*Qs;
end

f = M.freq+P.df+Q; % the right hand side 

end
