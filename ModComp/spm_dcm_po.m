function DCM = spm_dcm_po(DCM)   
% This program estimates the parameters of the extended DCM for phase coupling. 
% The suffix PO means for phase oscillators. The final name used in
% the article is eDCM PC. 
% 
% FORMAT DCM = spm_dcm_po(DCM)   
% 
% DCM     
%    name: name string
%       M:  Forward model
%              M.dipfit - leadfield specification
%              M.istransf 
%       xY: data   [1x1 struct]
%       xU: design [1x1 struct]
%
%   Sname: cell of source name st rings
%
%   Inputs connection constraints: 
%       A: [nr x nr]
%       B{i}: [nr x nr]
%   In the script these coefficients are redefined as real-valued
%   coefficients.
%       sc: [Nsig x nr]
%       ss: [Nsig x nr]
%       rc: [nr x Nsig]
%       rs: [nr x Nsig]
%       anm: [nr x nr x Nq x Nq]
%       bnm: [nr x nr x Nq x Nq]
%       cnm: [nr x nr x Nq x Nq]
%       dnm: [nr x nr x Nq x Nq]
%       The same matrices for B{i} with prefix B.
%
%   options.type         - 'ECD'
% ----------------------------------------------------------------
% This program is a modification of spm_dcm_phase.m written by Will Penny
% from SPM DCM toolbox of Wellcome Trust Centre for Neuroimaging
% ($Id: spm_dcm_phase.m 4884 2012-09-03 13:33:17Z guillaume $)
% ----------------------------------------------------------------
% Initial verstion AY 20.02.2017
% Last modification: AY 18.02.2019 edited comments.
% ----------------------------------------------------------------

% check options 
%==========================================================================
% clear spm_erp_L

% Filename and options
%--------------------------------------------------------------------------
try, DCM.name;                  catch, DCM.name           = 'DCM_PO'; end
try, DCM.options.Nmodes;        catch, DCM.options.Nmodes = 4;         end
try, h     = DCM.options.h;     catch, h                  = 1;         end
try, onset = DCM.options.onset; catch, onset              = 80;        end

% 
disp('Estimating Model ...');
addpath('../ModComp/');

xY     = DCM.xY;
xU     = DCM.xU;
xU.dt  = xY.dt;

% dimensions
%--------------------------------------------------------------------------
Nt     = length(xY.y);                  % number of trials
Nr     = size(DCM.A,1);                 % number of sources
nx     = Nr;                            % number of states

Ns     = size(xY.y{1},1);               % number of samples
xU.u   = zeros(Ns,1);

Nu     = size(xU.X,2);                  % number of trial-specific effects

Nq = DCM.M.Nq;
Nsig = DCM.M.Nsig;
if ~isfield(DCM.M,'Nrho')
    Nrho = Nsig; % for compatibility with old codes.
else
    Nrho = DCM.M.Nrho;
end

% DCM = meanfreq(DCM); % The results is in DCM.M.freq

%%%% The matrix A is defined with nr x nr 
DCM.anm = zeros(Nr,Nr,Nq,Nq);
DCM.bnm = zeros(Nr,Nr,Nq,Nq);
DCM.cnm = zeros(Nr,Nr,Nq,Nq);
DCM.dnm = zeros(Nr,Nr,Nq,Nq);

for n = 1:Nq
    for m = 1:Nq
        DCM.anm(:,:,n,m) = DCM.A;
        DCM.bnm(:,:,n,m) = DCM.A;
        DCM.cnm(:,:,n,m) = DCM.A;
        DCM.dnm(:,:,n,m) = DCM.A;
    end
end

% for the moment we are using only one mandatory trial specific matrix
DCM.Banm{1} = zeros(Nr,Nr,Nq,Nq);
DCM.Bbnm{1} = zeros(Nr,Nr,Nq,Nq);
DCM.Bcnm{1} = zeros(Nr,Nr,Nq,Nq);
DCM.Bdnm{1} = zeros(Nr,Nr,Nq,Nq);

for n = 1:Nq
    for m = 1:Nq
        DCM.Banm{1}(:,:,n,m) = DCM.B{1};
        DCM.Bbnm{1}(:,:,n,m) = DCM.B{1};
        DCM.Bcnm{1}(:,:,n,m) = DCM.B{1};
        DCM.Bdnm{1}(:,:,n,m) = DCM.B{1};
    end
end

%%% transformation part
% We have 2 options (regulated by istransf): 
% 1. We transform all data into true phase at the beginning, using the
% apprixmation of the transformation function from protophase to phase (the
% inverse transforamtion function).
% 2. We work with observable phase. The approximation to the transformation
% function is used to find the initial values of the Fourier coefficients
% of the forward transformation function. 
if DCM.options.istransf % if we simulate with true phases, i.e. without transformation:
    % Finding the initial approximation of the sigma function
    [DCM.sc,DCM.ss] = appxSigma(DCM.xY.y,Nsig); % if we call it without Nsig, then Nsig = 8;
    for n=1:Nt % we transform all data from observable (protophase) into theoretical (true) phase.
        xY.y{n} = transfv(xY.y{n},DCM.sc,DCM.ss); % using universal function
    end
else % if we fit the forward transformation function
    if Nsig>0 
        [DCM.sc,DCM.ss] = appxSigma(DCM.xY.y,Nsig); % with a fixed precision, error corrected
        % Calculate the Fourier coefficients of the forward transformation
        % function rho:
        [DCM.rc,DCM.rs] = invcoef(DCM.sc,DCM.ss,1000,Nrho); % with precision Nrho
             
        DCM.Brc{1} = zeros(Nr,Nrho);
        DCM.Brs{1} = zeros(Nr,Nrho);
    end
end

% Set error covariance components - one per region
%--------------------------------------------------------------------------
xY.Q   = spm_Ce(Ns*Nt*ones(1,Nr));

% Offsets
xY.X0  = sparse(Ns,0);

% Inputs
%==========================================================================

% trial-specific effects
%--------------------------------------------------------------------------
try
    if length(DCM.Banm) ~= Nu;
        warndlg({'Please ensure number of trial specific effects', ...
                 'encoded by DCM.xU.X & DCM.B are the same'})
    end
catch
    DCM.Banm = {};
    DCM.Bbnm = {};
    DCM.Bcnm = {};
    DCM.Bdnm = {};
    if isfield(DCM,'rs') 
        DCM.Brc = {};
        DCM.Brs = {};
    end
end

% model specification and nonlinear system identification
%==========================================================================
M      = DCM.M;  

% Commented: frequency and frequency bands are already defined. 
% % M.freq = mean(DCM.options.Fdcm,2); % this should be an array 
% % M.fb = 0.5*(DCM.options.Fdcm(:,2)-DCM.options.Fdcm(:,1)); % this is also 

try, M = rmfield(M,'FS'); end

if ~isfield(M,'dipfit')
    M.dipfit=[];
end

% prior moments
%--------------------------------------------------------------------------
[pE,pC] = spm_po_priors(DCM,M.fb,M.dipfit);

% likelihood model
%--------------------------------------------------------------------------
M.IS  = 'spm_gen_po';
M.f   = 'spm_fx_po';
M.g   = 'spm_gx_po';
M.G   = 'spm_lx_po';

M.pE  = pE;
M.pC  = pC;
M.n   = nx; % true and protophases
M.l   = Nr;
M.ns  = Ns;

% Don't plot progress
try, M.nograph; catch, M.nograph=0; end

% Initial state
try
    M.x;
catch
    M.x=zeros(Nr,1); % only for true phases.
end

% Set up initial phases 
for n=1:length(xY.y)
    xx=xY.y{n}(1,:);
    if ~DCM.options.istransf % if we did not do transformation of all data
        xx0 = transf(xx',DCM.sc,DCM.ss);
    else
        xx0 = xx'; % otherwise the initial phases are the same
    end
    M.trial{n}.x = double(xx(:)'); % number of observables are only Nr
    M.trial{n}.x0 = double(xx0(:)); % these values are used to define the intrinsic initial conditions
end

% EM: inversion
%--------------------------------------------------------------------------
% [Qp,Qg,Cp,Cg,Ce,F] = spm_nlsi_N(M,xU,xY); % this is for linear case
[Qp,Cp,Eh,F] = spm_nlsi_GN(M,xU,xY); % for non-linear case.
Ce           = exp(-Eh);

% Data ID
%==========================================================================
if isfield(M,'FS')
    try
        ID  = spm_data_id(feval(M.FS,xY.y,M));
    catch
        ID  = spm_data_id(feval(M.FS,xY.y));
    end
else
    ID  = spm_data_id(xY.y);
end


% Bayesian inference {threshold = prior} NB Prior on A,B  and C = exp(0) = 1
%==========================================================================
warning('off','SPM:negativeVariance');
dp  = spm_vec(Qp) - spm_vec(pE);
Pp  = spm_unvec(1 - spm_Ncdf(0,abs(dp),diag(Cp)),Qp);
warning('on','SPM:negativeVariance');

% neuronal and sensor responses (x and y)
%--------------------------------------------------------------------------
% x   = feval(M.IS,Qp,M,xU);        % prediction (source space)
% L   = feval(M.G, Qg,M);           % get gain matrix

% % trial-specific responses (in mode, channel and source space)
% %--------------------------------------------------------------------------
% for i = 1:Nt
%     s    = x{i};                  % prediction (source space)
%     y{i} = s*L';                  % prediction (sensor space)
%     %r  = xY.y{i} - y;            % residuals  (sensor space)
% end

% Change back design matrix to user specified
xU.X=DCM.xU.oldX;

% store estimates in DCM
%--------------------------------------------------------------------------
DCM.M  = M;                    % model specification
% DCM.xY = xY;                   % data structure
DCM.xU = xU;                   % input structure
DCM.Ep = Qp;                   % conditional expectation f(x,u,p)
DCM.Cp = Cp;                   % conditional covariances G(g)
DCM.Pp = Pp;                   % conditional probability
DCM.Ce = Ce;                   % conditional error covariance
DCM.F  = F;                    % Laplace log evidence
DCM.ID = ID;                   % data ID
% DCM.y  = y;                    % Model predictions

% and save
%--------------------------------------------------------------------------
save(DCM.name, 'DCM', spm_get_defaults('mat.format'));
assignin('base','DCM',DCM)

end