function [ia, ib] = invcoef(a,b,N,iNsig)
% This function calculates the Fourier coefficients of the inverse function of a
% function given by its Fourier coefficients a anb b. 
% The definition of the coefficients: 
% a and b are of the size of [Nr x Nsig], where Nsig is the 
% number of Fourier terms and Nr is the number of regions. 
% The output coefficients ia and ib have not (!) the same size as a and b.
% They are the size of [Nr x iNsig]. 
% N is the number of points for uniformly distributed phase from 0 to 2pi.
% 
% AY 20.02.2018
% Comments edited AY 18.02.2019
    
    [Nr,Nsig] = size(a); 
    
    % allocating space for the coefficients of the inverse function
    ia = zeros(Nr,iNsig);
    ib = zeros(Nr,iNsig);

    % creating the uniformly distributed phase values
    uphi = (0:N-1)*2*pi/N;
    k = (1:Nsig)';
    ik = 1./(1:Nsig);
    
    kuphi = k*uphi; 
    coskuphi = cos(kuphi);
    sinkuphi = sin(kuphi);

    for ir = 1:Nr
        % calculating the non-uniform phase
        nuphi = uphi + (a(ir,:).*ik)*sinkuphi + (b(ir,:).*ik)*(1-coskuphi);
        knuphi = (1:iNsig)'*nuphi; % is of the size of iNsig x N
        ia(ir,:) = 2*sum(cos(knuphi),2)/N; % (!) matlab transfers here column to line.
        ib(ir,:) = 2*sum(sin(knuphi),2)/N;
    end
end