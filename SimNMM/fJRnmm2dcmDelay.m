function dydt = fJRnmm2dcmDelay(t,y,p)
%%% AY 27.09.2018
%%% this version is a modification of fJRnmm2dcm.m.
%%% --- orig. descr. of fJRnmm2dcm.m --- %%%
%%% The state equations for modified the Jansen-Rit neural mass model
%%% as in David et al 2003 and 2004.
%%% t - time
%%% y - state variables
%%% p - parameters 
%%% 
%%% The description of all parameters are given in
%%% initSimJR2param60_[0:3].m
%%% ----------------------------------------------------------------------
%%% AY 30.08.2017
%%% Last modification: AY 18.02.2019 edited comments.
%%% ----------------------------------------------------------------------
    dydt = zeros(1,16); % 16 state variables 
    
    try 
        isfield(p,'a');
    catch
        p.a1 = p.a;
        p.a2 = p.a;
    end
    
    try 
        isfield(p,'b');
    catch
        p.b1 = p.b;
        p.b2 = p.b;
    end
    %%% for the first colnmn
    % for the first excitatory PSP block 
    dydt(1) = y(4);
    dydt(4) = p.A1*p.a1*Sigm(y(2)-y(3)) - 2*p.a1*y(4) - p.a1*p.a1*y(1);
    % for the second excitatory PSP block 
    dydt(2) = y(5);
    dydt(5) = p.A1*p.a1*(p.noiseMean1 + p.C21*Sigm(p.C11*y(1)) + p.K2*y(14)) ...
                - 2*p.a1*y(5) - p.a1*p.a1*y(2);
    % for the inhibitory PSP block 
    dydt(3) = y(6);
    dydt(6) = p.B1*p.b1*(p.C41*Sigm(p.C31*y(1))) ...
            - 2*p.b1*y(6) - p.b1*p.b1*y(3);        

    %%% for the second colnmn
    % for the first excitatory PSP block         
    dydt(7) = y(10);
    dydt(10) = p.A2*p.a2*Sigm(y(8)-y(9)) - 2*p.a2*y(10) - p.a2*p.a2*y(7);
    % for the second excitatory PSP block 
    dydt(8) = y(11);
    dydt(11) = p.A2*p.a2*(p.noiseMean2 + p.C22*Sigm(p.C12*y(7)) + p.K1*y(13)) ...
                - 2*p.a2*y(11) - p.a2*p.a2*y(8);
    % for the inhibitory PSP block 
    dydt(9) = y(12);
    dydt(12) = p.B2*p.b2*(p.C42*Sigm(p.C32*y(7))) ...
            - 2*p.b2*y(12) - p.b2*p.b2*y(9);
    
    %%% other two intercolumn branches that represent the delay block
    dydt(13) = y(15);
    dydt(15) = p.A1*p.ad*Sigm(y(2)-y(3)) - 2*p.ad*y(15) - p.ad*p.ad*y(13);

    dydt(14) = y(16);
    dydt(16) = p.A2*p.ad*Sigm(y(8)-y(9)) - 2*p.ad*y(16) - p.ad*p.ad*y(14);
    
    
    %%% the sigmoid function
    function out = Sigm(x)
        out = 2*p.e0./(1+exp(p.r*(p.v0-x)));
    end

    function out = VisIn(x)
        w = 0.005;
        n = 7; 
        q = 0.5;
        if x>=1.0
            out = q*((x-1.0/w).^n).*exp(-(x-1.0)/w);
        else
            out = 0.0;
        end
    end
end

