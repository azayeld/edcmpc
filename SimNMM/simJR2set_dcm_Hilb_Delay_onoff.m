%%% This is a modification of the script simJR2set_dcm_Delay.m.
%%% In this version we use the balanced input to the regions implemented in 
%%% simJR2alltrialdcmDelay.m. Another modification is that the Hilbert
%%% transformation is apllied to the whole time interval, with and without
%%% coupling. This gives the better phase estimaion.
%%% ----- AY 16.10.2018 -----
%%% -------------------------
%%% ----- description of the previous version of the script ---------- %%%
%%% This is a modified verision of simJR2set_dem.m. This script 
%%% simulates the set of trials of the JR neural mass model (NMM) with given 
%%% values of parameters. 
%%% The phase of the signals are extracted using the Hilbert transformation. 
%%% ----- AY 01.10.2018 -----
%%% 
%%% -----------------------------------------------------------------------
%%% Last modification AY 18.02.2019 edited comments.
clear; 
    
%% the file with parameters
sim.do_plot = 1;

% different cases:
% 0: no coupling
% 1: the 2nd is coupled to the 1st
% 2: the 1st is coupled to the 2nd
% 3: bidirectional coupling
for iii=0:3
% iii = 2;
    
    %%% run the script with parameters initiation
    run(['initSimJR2param60_' num2str(iii)]);
    disp(['run initSimJR2param60_' num2str(iii) '...']);

    %%% modifications of the inital parameters
    %%% coupling strength
    Ks1 = 400.0;
    Ks2 = 800.0;
    if p.K1~=0
        p.K1 = Ks1;
    end
    if p.K2~=0
        p.K2 = Ks2;
    end
    
    p.ad = 30; % 30 s^-1 % the delay in the delay block
    % change std of noise 
    p.noiseMean1 = 220; % 220;
    p.noiseMean2 = 220; % 220;
    p.noiseAmpl1 = 0.1; %0.2; % 0.25;
    p.noiseAmpl2 = 0.1; %0.2; % 0.25;
    p.noiseStd = 1.0; %1.0;
    
    %%% change integration parameters
    intP.Ttotal = 1.3;
%     intP.N = 2.5*10^3; % total number of steps
    intP.N = 4*1024; % total number of steps
    sim.Ntrial = 15; % number of trials
    intP.Ttran = 0.5;
    intP.Ttran0 = 1.5; % to calculate the initial state
    intP.Ton = 0.3;    % time interval with coupling
    intP.Toff = 0.5;   % time interval without coupling  
    sim.cutr = intP.Ttran/(intP.Ttran+intP.Ton+intP.Toff);
    sim.Ndt = 2; % was 5;
    
    %%% output file: _H_ referes to Hilbert transformation
  
    % Ks1 = 400.0, Ks2 = 800.0; with noise and with rand. init. cond and
    % on-off (before and after). Hilbert appl. to all range,
    sim.savefilename = ['outSimJR2_H_test_onoff_noise_107_' num2str(iii) '.mat'];
    
    % Ks1 = 400.0, Ks2 = 800.0; with noise and with rand. init. cond and
    % on-off (before and after). Hilbert appl. to all range, stronger noise
%     sim.savefilename = ['outSimJR2_H_test_onoff_noise_108_' num2str(iii) '.mat'];
    
    disp(['changed to' sim.savefilename ', run it ...']);

%     sim.Ntrial = 1; % number of trials
%     intP.Ttran = intP.Ttotal/intP.N; % one dt
%     intP.Ttran = 0.5;
    
    %% testing the simulation of all trials
%     Ss = simJR2alltrialdcmDelay(p,intP,sim.Ntrial);
    Ss = simJR2alltrial3(p,intP,sim.Ntrial);

    
    sim.dt = intP.Ttotal/intP.N;

    hf1 = figure(1);
    set(hf1,'Units','centimeters','Position',2*[2 2 8.4 10]);
%     subplot(3,1,1);
    plot(Ss{1}.T,Ss{1}.Y(:,1),'k-');
    hold on;
    % subplot(2,1,2);
    plot(Ss{1}.T,Ss{1}.Y(:,2),'r-');
    hold off;
    xlim([0 Ss{1}.T(end)]);
    legend(gca,['1st'],['2nd']);
    title(['K1=' num2str(p.K1) ', K2=' num2str(p.K2)]);
    
    hf11 = figure(11);
    set(hf1,'Units','centimeters','Position',2*[2 2 8.4 10],'Name','Y vs dY');
    subplot(2,1,1);
    plot(Ss{1}.Y(:,1),Ss{1}.dY(:,1),'k-');
    subplot(2,1,2);
    plot(Ss{1}.Y(:,2),Ss{1}.dY(:,2),'r-');
%     xlim([0 Ss{1}.T(end)]);
%     legend(gca,['1st'],['2nd']);
    title(['K1=' num2str(p.K1) ', K2=' num2str(p.K2)]);

    %%% Finding the spectrum of the signals (1st trial only) using
    %%% spectorgram.
    %%% Commented, not supported with all versions of the Matlab.
%     lngWin = round(length(Ss{1}.T)/10);
%     lngOver = round(0.9*length(Ss{1}.T)/10);
%     freqRng = [2.0:2:50];
%     Fs = intP.N/intP.Ttotal;
%     subplot(3,1,2);
%     [spS1,spF1,spT1,spP1]=spectrogram(Ss{1}.Y(:,1),lngWin,lngOver,freqRng,Fs,'yaxis');
%     sf1 = surf(spT1,spF1,10*log10(spP1));
%     colormap(gca,'jet');
%     set(sf1,'EdgeColor','none');
%     axis tight
%     view(0,90);
%     xlabel('Time (s)');
%     ylabel('Frequency (Hz)');
% 
%     subplot(3,1,3);
%     [spS2,spF2,spT2,spP2]=spectrogram(Ss{1}.Y(:,2),lngWin,lngOver,freqRng,Fs,'yaxis');
%     sf2 = surf(spT2,spF2,10*log10(spP2));
%     colormap(gca,'jet');
%     set(sf2,'EdgeColor','none');
%     axis tight
%     view(0,90);
%     xlabel('Time (s)');
%     ylabel('Frequency (Hz)');


    %% find spectrum
    %% calculate mean spectrum
    Fs = intP.N/intP.Ttotal; % sampling rate
    [L,Nr] = size(Ss{1}.Y); % all trials have the same length
    hL = ceil(L/2); % make half L integer
    P = zeros(hL+1,Nr); % allocale memory for power spectrum
    f = Fs*(0:hL)/L; % frequency axes, same for all

    %%% loop by sets
    % for ns = 1:sim.Nset
    %     Sset = S{ns,1};
    %     tims = Sset.t; %%% time
        %%% loop by trials
        for n=1:sim.Ntrial,
            %%% loop by regions
            for c=1:Nr,
                xr=Ss{n}.Y(:,c);
                %%% commented,since all signals have the same length            
                %%% L = length(xr); 
                fY = fft(xr);
                P2 = abs(fY/L);
                P1 = P2(1:hL+1);
                P1(2:end-1) = 2*P1(2:end-1);
                P(:,c) = P(:,c) + P1;
            end %% end loop by regions
        end %%% end loop by trials
    % end %%% end loop by set

    P = P/(sim.Nset*sim.Ntrial);

    rngFreq = [0.1, 50];  % frequency range
    irngFreq = round(rngFreq*Ss{1}.T(end))+1; % index of the frequency range

    %%% plot mean spectrum 
    figure(400);
    subplot(1,2,1);
    plot(f(irngFreq(1):irngFreq(2)),P(irngFreq(1):irngFreq(2),1),'r');
    title('PS  of 1st reg');
    subplot(1,2,2);
    plot(f(irngFreq(1):irngFreq(2)),P(irngFreq(1):irngFreq(2),2),'b');
    title('PS  of 2nd reg');

    disp('Spectrum calculation complete ...');

    hf2 = figure(2);
    plot(Ss{1}.Y(:,1),Ss{1}.Y(:,2),'k-');

    %% filter signals 
    Fn = Fs/2; % Nyquist frequency
    % allocate memory for the intrinsic varibles
    yyf = zeros(L,Nr);

    ts = round(L*sim.cutr); % define the index that correspond to cutr
    tims = Ss{1}.T; % time variabel is the same for all trials
    tphi = tims(ts:end-ts); % cut edges
    Ntphi = length(tphi);
    phiy = zeros(Ntphi,Nr); % allocate space for phase
    % aphi = zeros(Ntphi*sim.Nset*sim.Ntrials,Nr); % allocalte space for all phase values
    % phi0 = zeros(sim.Nset*sim.Ntrials,Nr);  % allocate space for 1st phases.

    %%% loop by sets
    % for ns = 1:sim.Nset
    %%% we have only one set 
    S{1}.t = Ss{1}.T;
    %     Sset = S{ns,1};
    %     tims = Sset.t; %%% time
        tims = Ss{1}.T; % time is the same as t

        %%% loop by trials
        for n=1:sim.Ntrial,
            %%% loop by regions
            for c=1:Nr,
                xr=Ss{n}.Y(:,c); 
%                 xr = xr - mean(xr);
                xr = xr - (max(xr)+min(xr))/2;
%                 %%% Filtering: commented, not used
%                 % alternativefiltering with  field trip band pass filter
%     %             xrf = ft_preproc_bandpassfilter(xr', Fs, sim.Fdcm{c}, sim.fltorder);
%                 [b,a] = butter(sim.fltorder, sim.Fdcm{c}/Fn,'bandpass');
%     %             [b,a] = butter(sim.fltorder, sim.Fdcm{c}(2)/Fn,'low');
%     %             freqz(b,a);
%                 xrf = filtfilt(b,a,xr);
%     %             xrf = filter(b,a,xr);
%                 yyf(:,c) = xrf; % filtered signal
                %%%% commented filtering
                
                xrf = xr;
                yyf(:,c) = xrf;

                %%% Phase extraction:
%                 hx=spm_hilbert(xrf); % hilbert for filtered signal
%                 hx = hilbert(xrf,1024);
                hx=spm_hilbert(xr); % hilbert for not filtered signal
%                 hx = hilbert(xr);
                %%% testing part 
                if sim.do_plot
                    figure(420+c);
                    plot(real(hx),imag(hx),'b');
                    hold on;
                    plot(real(hx(ts:end-ts)),imag(hx(ts:end-ts)),'r');
                    hold off;
                    legend('orig. Hlbrt', 'cut Hlbrt');
                end

                phi =unwrap(double(angle(hx)));
                phiy(:,c) = phi(ts:end-ts); % cut off tails
                tphi = tims(ts:end-ts);
                %%% testing part 
                if sim.do_plot
                    figure(410+c);
                    plot(tims,xr,'b');
                    hold on;
                    plot(tims,xrf,'r');
                    plot(tphi,cos(phiy(:,c)),'k');
                    hold off;
                end
           end %% end loop by regions
           S{1}.yy{n} = Ss{n}.Y;
           S{1}.xx{n} = phiy;
           S{1}.tphi = tims(ts:end-ts)-tims(ts);
        end %%% end loop by trials
    % end %%% end loop by set
    disp('Filtering and phase extraction complete ...');

    % plot the last result
    phi1 = phiy(:,1)-phiy(1,1);
    phi2 = phiy(:,2)-phiy(1,2);

    % plot the trials and the phase difference
    figure(5);
    subplot(3,1,1); 
    plot(mod(phi1,2*pi),'b');
    hold on;
    plot(mod(phi2,2*pi),'r');
    hold off;
    legend('phi1','phi2');
    subplot(3,1,2);
    plot(mod(phi1-phi2,2*pi));
    legend('phi1-phi2');

%     subplot(3,1,3);
%     plot(mod(phi1,2*pi),mod(phi2,2*pi),'b');
%     % hold on;
%     % plot(cos(phiy(:,2)),sin(phiy(:,2)),'r');
%     % hold off;
%     axis square
    %%% --- modification AY 26.09.2018 --- %%%
    subplot(3,1,3);
    plot(tphi(1:end-1),diff(phi1)./diff(tphi),'b');
    hold on;
    plot(tphi(1:end-1),diff(phi2)./diff(tphi),'r');
    hold off;
    legend('dphi1/dt','dphi2/dt');
    
    %% resample all points 
    % the variables that have to be resampled
    % S.tphi 
    % S.xx
    Ntphi=length(S{1}.tphi);
    try isfield(sim,'Ndt')
        Ndt = sim.Ndt;
    catch
        Ndt = 20;  % we take only every 20th point, down sample. 
    end
    ires = [1:Ndt:Ntphi]; % resample index.

    S{1}.tphi = S{1}.tphi(ires);
    %%% loop by trials
    for n=1:sim.Ntrial,
        S{1}.xx{n} = S{1}.xx{n}(ires,:);
    end
    sim.dt = sim.dt*Ndt;

    
    %%% AY 26.09.2018 repetition of the figure 5  for resampled data set
    figure(6);
    dphi1dt = diff(S{1}.xx{1}(:,1))./diff(S{1}.tphi);
    dphi1dt = dphi1dt - mean(dphi1dt);
    dphi2dt = diff(S{1}.xx{1}(:,2))./diff(S{1}.tphi);
    dphi2dt = dphi2dt - mean(dphi2dt);
    
    plot(S{1}.tphi(1:end-1),dphi1dt,'b');
    hold on;
    plot(S{1}.tphi(1:end-1),dphi2dt,'r');
    hold off;
    legend('dphi1/dt','dphi2/dt');

    %% save results
    save(sim.savefilename,'sim','S','intP','p');
    disp(['The results are saved in ' sim.savefilename '.']);
    pause;
end