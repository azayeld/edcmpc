function out = simJR2alltrial3(p,intP,Ntr)
%%% This script is a modification of the simJR2alltrialdcmDelay.m
%%% It generates synthetic data set with transient activation of the coupling.
%%% Initially, the coupling is turned off for a given transient time interval.
%%% Then for a given period of time the coupling is activated. 
%%% Finally, for another transient time period the coupling is deactivated again. 
%%% The inputs:
%%%     - intP.Tran0  is an initial transeint time period to calculate the period
%%%     and the limit cycle, necessary for defining the random initial
%%%     conditions.
%%%     - intP.Tran is the initial time period without coupling
%%%     - intP.Ton is the time interval with coupling
%%%     - intP.Toff is the final time interval wihtout coupling.
%%%     - Ntr is the number of trials to generate.
%%%     Tran0 |{ Tran | Ton | Toff }, {} is an output.
%%% 
%%% AY 16.10.2018
%%% --------------------------
%%% We generate all trials at ones. 
%%% The initial conditions are set to be more or less uniformly
%%% distributed along the limit cycle. 
%%% AY 10.10.2018
%%% ---------------------
%%% AY 27.09.2018
%%% This is a modification of the original simJR2trialdcm function.
%%% The rhs with activated delay block. 
%%%
%%% --- the description of orig. func. simJR2trialdcm.m ------- %%%
%%% The function produces one trial of the Jansen & Rit neural mass model 
%%% for given values of the system parameters and the integration
%%% parameters. For integration we use the Runge Kutta method for SDE, 
%%% with modified model as in David et al (2003), (2004).
%%% Input parameters:
%%%     intP is a structure of parameter values for intergration 
%%%     p is a sturcture for the parameters of the system. 
%%% Output:
%%%     - out is a structure:
%%%         t - time      
%%%         Y - the signals.
%%%     both are cutted from transent time till the end of the simulation.
%%% AY 11.09.2017
%%% --- end of the description of orig. func. simJR2trialdcm.m ------- %%%
%%% -----------------------------------------------------------------------
%%% Last modification: AY 18.02.2019 edited comments

% - Ttotal is the total time of integration
N = intP.N; % the number of intervals
dt = intP.Ttotal/N; % time step

%% set inital values
% initial vectors
v0 = [0.1 25.0 15 -1.0 -75 -30];
vx = [0.03 0.03 -0.2 -0.2];

y0 = [v0 v0 vx];

%% we run the system first without coupling to identify the limit cycle of every oscillator 
%%% save the coupling strengths
oK1 = p.K1;
oK2 = p.K2;
%%% set them to zero
p.K1 = 0;
p.K2 = 0;

%%% define the number of points for every stage
iTran = round(intP.Ttran/dt);

if isfield(intP,'Tran0')
    iTran0 = round(intP.Ttran0/dt); 
else 
    iTran0 = iTran;
end


if isfield(intP,'Ton')
    iOn = round(intP.Ton/dt); 
else 
    iOn = iTran;
end

if isfield(intP,'Toff')
    iOff = round(intP.Toff/dt); 
else 
    iOff = iTran;
end


%% run the system for some transient time
% [Ytr, Ttr] = intJR2delay(y0,round(N*0.25),dt,p);
[Ytr, Ttr] = intJR2delay(y0,iTran0,dt,p); % we run up to Tran0 time.

%% find the period of an output (2-3 and 8-9)
% for the first region
yout1 = Ytr(:,2)-Ytr(:,3);
yout10 = yout1 - mean(yout1);
zrs1 = find([yout10; 0].*[0; yout10]<0); %% find zeros
if length(zrs1)>3                  % 1:6, 7, 8
    Y10 = Ytr(zrs1(end-2):zrs1(end),[1:6,13,15]);
    T10 = Ttr(zrs1(end-2):zrs1(end),:);
else
    disp('not enough zero points in the first system');
end

% for the second region 
yout2 = Ytr(:,8)-Ytr(:,9);
yout20 = yout2 - mean(yout2);
zrs2 = find([yout20; 0].*[0; yout20]<0); %% find zeros
if length(zrs2)>3                  % 1:6 , 7, 8
    Y20 = Ytr(zrs2(end-2):zrs2(end),[7:12,14,16]);
    T20 = Ttr(zrs2(end-2):zrs2(end),:);
else
    disp('not enough zero points in the second system');
end

%% measure the mean of the y_d's
myd1 = mean(Y10(:,7));
myd2 = mean(Y20(:,7));

%% store initial noise Mean
mNoise1 = p.noiseMean1;
mNoise2 = p.noiseMean2;

%% loop by trials
for itr = 1:Ntr
    %% generate random initial conditions
        % random indexes within the stored Limit Cycles Y10 and Y20:
        rind1 = round(rand*(length(T10)-1))+1;
        rind2 = round(rand*(length(T20)-1))+1;

        % compose the vector of initial conditions
        % 1-6 -> Y10(1:6)
        y0tr = [Y10(rind1,1:6), Y20(rind2,1:6), ...
                Y10(rind1,7), Y20(rind2,7), ...
                Y10(rind1,8), Y20(rind2,8)]; 
    %% turn off coupling 
        p.K1 = 0.0;
        p.K2 = 0.0;
        %%% restore the mean amplitude of the noise 
        p.noiseMean1 = mNoise1; 
        p.noiseMean2 = mNoise2;
            
    %% run without coupling
        [Y1, T1] = intJR2delay(y0tr,iTran,dt,p);
        Y = Y1;
        T = T1;
         
    %% turn on the coupling
        p.K1 = oK1;
        p.K2 = oK2;
        %%% correct the mean amplitude of the noise 
        p.noiseMean1 = mNoise1 - oK2*myd2; 
        p.noiseMean2 = mNoise2 - oK1*myd1;

        
    %% run with coupling 
        [Y2, T2] = intJR2delay(Y(end,:),iOn,dt,p);
%       [Y2, T2] = intJR2delay(y0tr,iOn,dt,p);
        Y = [Y; Y2];
        T = [T; T2+T(end)];
    
%     %% turn off the coupling 
        p.K1 = 0;
        p.K2 = 0;
      %%% restore the mean amplitude of the noise 
        p.noiseMean1 = mNoise1; 
        p.noiseMean2 = mNoise2;
      %% run without coupling
        [Y3, T3] = intJR2delay(Y(end,:),iOff,dt,p);

        %% collect all output
        Y = [Y; Y3];
        T = [T; T3+T(end)];

    % No cutting at this stage. We save all time period into output.
    S{itr}.T = T(:)-T(1);
    % as an output we use Y(:,2)-Y(:,3) and Y(:,8)-Y(:,9)
    S{itr}.Y = [Y(:,2)-Y(:,3) , Y(:,8)-Y(:,9)];
    S{itr}.dY = [Y(:,5)-Y(:,6) , Y(:,11)-Y(:,12)];
  
        
end
out = S;

end
