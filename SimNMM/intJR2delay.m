%%% Script integrates the system of stochastic differential equations,
%%% namely, the neural mass model. The rhs functions are 
%%% - fJRnmm2dcmDelay.m
%%% - gJRnmm2.m 
%%% AY 10.10.2018
%%% Last modification: AY 18.02.2019 edited comments

function [Y,T] = intJR2delay(y0,N,dt,p)
%%% parameters:
%%% y0 initial conditions
%%% N the number of points
%%% dt time step
%%% p the structure of parameters that is transfered to the rhs functions
%%% NOTE: p is not universal. It is defined only for NMM model JR2.

    %% create transform matrix for noise vector
    mTrn = zeros(2,p.Nsize);
    mTrn(1,5) = 1;
    mTrn(2,11) = 1;

    Y = zeros(N,p.Nsize);
    T = zeros(N,1);
    %% integration
    Yt = y0; 
    t = 0.0;
    for j = 1:N 
        t = t + dt;
        Winc = sqrt(dt)*normrnd(0.0,p.noiseStd,[1,2]); %
        Wincv = Winc*mTrn; % vector Winner increment, is non-zero only for (1,5) and (2,11).

        af = fJRnmm2dcmDelay(t,Yt,p);
        bf = gJRnmm2(t,Yt,p);
        u = Yt + af*dt;  
        up = u + bf*sqrt(dt);  
        um = u - bf*sqrt(dt);  
        u = u + bf.*Wincv;

        au = fJRnmm2dcmDelay(t,u,p);
        bup = gJRnmm2(t,up,p);
        bum = gJRnmm2(t,um,p);

        Yt = Yt + 0.5*(au+af)*dt + 0.25*(2*bf+bup+bum).*Wincv + ...
            0.25*(bup-bum).*(Wincv.*Wincv - dt)/sqrt(dt); 

        Y(j,:) = Yt;
        T(j) = t;
    end

end