function out = gJRnmm2(t,y,p)
%%% the stochastic term of the JR neural mass model for two mutually 
%%% coupled regions 
%%% t - time
%%% y - state variables
%%% p - parameters 
%%% 
%%% The description of all parameters are given in
%%% initSimJR2param60_[0:3].m
%%% ----------------------------------------------------------------------
%%% AY 07.09.2017
%%% ----------------------------------------------------------------------
%%% Last modification: AY 21.02.2018 edited comments.
    try 
        isfield(p,'a');
    catch
        p.a1 = p.a;
        p.a2 = p.a;
    end
    
    out = zeros(1,16);
    out(1,5)  = p.A1*p.a1*p.noiseAmpl1; % only to variables 5 and 11
    out(1,11) = p.A2*p.a2*p.noiseAmpl2; %
end