%%% initiate parameters of the Jansen-Rit neural mass model with two
%%% connected regions and defines the integration parameters.
%%% All parameters of the system are stored in a paramters structure p.
%%% ----------------------------------------------------------------------
%%% AY 11.09.2017
%%% ----------------------------------------------------------------------
%%% last modification 12.09.2017: implemented alpha and beta oscillations. 
%%% last modification 16.09.2017: alpha non sine, and alpha sin
%%% uni-directional connection, minimal noise. On the base of 40_15.
%%% co connection.
%%% 30 cause stable alpha and the initial conditions are more random.
%%% Increased cutting ratio. 
%%% The number of trials are 10.

%% simulation parameters
sim.savefilename = 'outSimJR2_60_3.mat';
sim.Ntrial = 10; % number of trials
sim.Nset  = 1; % number of sets

%% parameters of filtering 
%%% define filter regions
sim.Fdcm{1} = [5.0 12.5];
% sim.Fdcm{2} = [5.0 12.5]; % alpha
sim.Fdcm{2} = [12.0 30.0]; % beta
% sim.Fdcm{2} = [35.0 45.0]; % gamma
sim.fltorder = 1;
sim.cutr = 0.25; % ratio of cut tails

%% paramters of the integration
% integration time
intP.timeInt1 = 1.5;
% intP.timeInt2 = 3.0;
intP.Ttotal = intP.timeInt1;

intP.N = 1.5*10^4; % total number of steps
sim.Ndt = 20; % interstep multiplier, to reduce sampling rate for phases


intP.Ttran = 1.0; % transient time 

%% paramters of the system
p.Nsize = 16; % the size of the system

% p.e0 - the maximum firering rate of neural population
p.e0 = 2.5; % s^-1
% p.v0 - the PSP for which a 50% firing rate is achieved
p.v0 = 6; % mV
% r - the steepness of the sigmoidal transformation
p.r = 0.56; % mv^-1

%%% General notations about the Jansen-Rit neural mass model: 
%%% A and B are the maximum EPSP and IPSP, respectively. 
%%% a and b are the time constants of the excitatory and inhibitory linear
%%% transformations.
%%% C1, C2, C3, and C4 are connectivity constants. 
%%% The relation between these coefficients:
%%% C1 = C2/0.8 = 4*C3 = 4*C4.
%%% inputF1 and inputF2 are the input functions for regions. 
%%% These parameters are for every region (column). 
%%% 
%%% The parameters for coupling between regions (columns):
%%% K1 and K2 are coupling coefficients,
%%% ad is a delay in the dealy block.
%%%
%%% Everywhere the indexes 1 and 2 are related to the first and to the
%%% second regions (columns), respectively. 

% alpha oscillations, Jansen Rit
ataue1 = 20; % original 10
ataui1 = 15; % original 20

% non-sine alpha
% ataue1 = 12; % original 10
% ataui1 = 26; % original 20

% alpha non-sine oscillations
% ataue2 = 15; % original 10
% ataui2 = 25; % original 20

% ataue2 = 12; % original 10
% ataui2 = 28; % original 20

% % gamma oscillations
% ataue2 = 4; % original 10
% ataui2 = 4; % original 20

%%% alpha oscillation from David 2003;
% taue = 10.8;
% taui = 22; 

% %%% beta oscillations 
% ataue2 = 10; % original 10
% ataui2 = 6; % original 20
ataue2 = 10; % original 10
ataui2 = 10; % original 20

% %%% gamma oscillations
% gtaue = 4.6;
% gtaui = 2.9;

p.a1 = 1000/ataue1;% s^-1
p.b1 = 1000/ataui1; % s^-1

p.a2 = 1000/ataue2;% s^-1
p.b2 = 1000/ataui2; % s^-1


p.A_a =  3.25/100; % the ratio Aa in the original model
p.B_b =  22/50;  % the ratio Bb in the original model

% p.A1 = 3.25; % mV
% p.B1 = 22; % mV
% p.A2 = 3.25; % mV
% p.B2 = 22; % 17.6; % mV

%%% adjusted by the ratio to ensure oscilatatory solution
p.A1 = p.A_a*p.a1; % mV
p.B1 = p.B_b*p.b1; % mV
p.A2 = p.A_a*p.a2; % mV
p.B2 = p.B_b*p.b2; % mV

p.ad = 30; % was 30 s^-1

p.C1 = 135; % unitless, this should produce alpha like oscillations
p.C11 = p.C1;
p.C21 = 0.8*p.C1;
p.C31 = p.C1/4;
p.C41 = p.C1/4;

p.C2 =  135; % 170; % 135; %  108;% 350; 128;
p.C12 = p.C2;
p.C22 = 0.8*p.C2;
p.C32 = p.C2/4;
p.C42 = p.C2/4;

%%% input function parameters
% p.noiselow = 120; % not used
% p.noisehigh = 320;
p.noiseMean1 = 220;
p.noiseMean2 = 220;
p.noiseAmpl1 = 0.25;
p.noiseAmpl2 = 0.25;
p.noiseStd = 1.0;


%%% input functions
% p.inputF1 = @(t) p.noiseMean1; % constant input, the stochastic part is 
% p.inputF2 = @(t) p.noiseMean2; % defined in gJRnmm2.m

%%% coupling coefficients, bi-directional
p.K1 = 25.0; %120; % 200;
p.K2 = 25.0; % 150;



