%%% Demonstration script, 1st example of simulation of the syntetic signals
%%% of two coupled stochastic phase oscillators and finding the coupling
%%% functions by means of extended DCM for phase coupling (eDCM PC).
%%% This demonstration script consists of the following steps:
%%%     1. Preparation of the parameters of the simulation.
%%%     2. Simulation of the syntetic signals.  
%%%     3. Testing the syntetic data sets. 
%%%     4. Fitting the signals and finding the coupling functions with eDCM PC.
%%%     5. Plotting the results.
%%% Every step is performed by an external script. Please refere to the
%%% Manual of the eDCM PC and the comments of the scripts for more details.
%%% ----------------------------------------
%%% AY 10.03.2021
%%% ----------------------------------------

clear;

%% Setting the parameters of the simulation. 
%%% Different connetivity cases:
%%% Case 0: no connection between oscillators
%%% Case 1: the 1st oscillator is connected to the 2nd oscillator
%%% Case 2: the 2nd oscillator is connected to the 1st oscillator
%%% Case 3: both oscillators are connected to each other
casenum = 1;

%%% With or without transformation:
istransf = 1; % 1 with, 0 without.

%% Calling the script that sets the parameters. Uncomment the appropriate one.
sim = set_param_sim(casenum,istransf);
%%% the special case of connectivivty matrix
%%% for comparison of the original and the extended DCM PC.
% sim = set_param_sim_sp(istrans);

%% Generate the syntetic data set
gen_stPhOsc_dataset(sim);

%% Testing the data set. 
addpath('../../TestDataset');
test_datasets(sim.foutname,4,0);

%% Fitting data set.
addpath('C:/MWork/spm12'); % here add path to spm
spm eeg;
addpath('../../Fitting');
% set fitting parameters
fitpar = set_param_fit_phosc;
foutname = fFit_eDCM_PC(fitpar); % call fitting function of eDCM PC.

%% Plotting results
% foutname = 'dcm_nsig4_setn1_stPhOsc_dataset_R1.mat'; % for direct testing
addpath('../../PlotRes');
plotpar.Nmesh = 20;
plotpar.showdata = 1;
plotpar.showbars = 1;
plot_res_edcm_pc(foutname,plotpar);

%% Plotting comparison results
plot_comp_res_edcm_pc(foutname,sim.foutname);