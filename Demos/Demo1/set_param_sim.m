function [sim] = set_param_sim(casenum,istransf)
%%% This function sets the parameters of the simulation of the syntetic
%%% signals created by integration of the model of two coupled stochastic 
%%% phase oscillators. The result is the sim structure, which will be
%%% transfered to gen_stPhOsc_dataset function.
%%% Input parameter:
%%% casenum is the number of the coupling cases:
%%%     Case 0: no connection between oscillators
%%%     Case 1: the 1st oscillator is connected to the 2nd oscillator
%%%     Case 2: the 2nd oscillator is connected to the 1st oscillator
%%%     Case 3: both oscillators are connected to each other
%%% istransf is the flag: 
%%%     istransf = 1; the transformation is applied.
%%%     istransf = 0; the transformation is not applied. 

%% Define the output file name
% The definition of only the root of the filename; the index is defined later, 
% when the connection type is specified. 
foutname = 'stPhOsc_dataset'; 

%% Define the data set properties in the sim structure 
sim.do_plot = 0; 
sim.Nset = 1; % 15; % number of sets
sim.Ntrials = 20; % Number of trials in each set
sim.noise.is = 0; % not used.
% sim.noise.eta = 0.075;
% sim.noise.dev = 0.15;
%%% :::special case: to compare the original and new extension of DCM PC
sim.noise.eta = 0.1;
sim.noise.dev = 0.15;
%%% end of the special case:::

sim.Nr = 2; % the number of regions

%%% the parameters of the oscillators - the natural frequencies
% sim.f = [1; 1+0.43]; % 
sim.f = [1; 1]; % 

%% Defining the connectivity matrix
%%% Allocate the coefficients
sim.Nnm = 1; % Number of terms in Fourier series. Only the 1st terms.
sim.anm = zeros(sim.Nr,sim.Nr,sim.Nnm,sim.Nnm);
sim.bnm = zeros(sim.Nr,sim.Nr,sim.Nnm,sim.Nnm);
sim.cnm = zeros(sim.Nr,sim.Nr,sim.Nnm,sim.Nnm);
sim.dnm = zeros(sim.Nr,sim.Nr,sim.Nnm,sim.Nnm);

%%% Define the vector of the connection strength:
convec = [0.2; 0.1; -0.17; 0.15; -0.1; -0.2; 0.19; -0.15];

%%% Different connection types.
switch casenum
    %%% Case 0. no connection
    case 0
        sfxout = 0; % default state
        % no change 
    %%% Case 1: 1st osc is connected to 2nd osc
    case 1 
        sfxout = 1;
        sim.anm(2,1,1,1) = convec(1);
        sim.bnm(2,1,1,1) = convec(2);
        sim.cnm(2,1,1,1) = convec(3);
        sim.dnm(2,1,1,1) = convec(4);
    %%% Case 2. 2nd osc is connected to 1st osc
    case 2
        sfxout = 2;
        sim.anm(1,2,1,1) = convec(5);  
        sim.bnm(1,2,1,1) = convec(6);
        sim.cnm(1,2,1,1) = convec(7);
        sim.dnm(1,2,1,1) = convec(8);
    %%% Case 3. both osc.s are connected to each other
    case 3
        sfxout = 3;
        sim.anm(2,1,1,1) = convec(1);
        sim.bnm(2,1,1,1) = convec(2);
        sim.cnm(2,1,1,1) = convec(3);
        sim.dnm(2,1,1,1) = convec(4);
        sim.anm(1,2,1,1) = convec(5);
        sim.bnm(1,2,1,1) = convec(6);
        sim.cnm(1,2,1,1) = convec(7);
        sim.dnm(1,2,1,1) = convec(8);
end
%%% end of the connectivity matrix definition

%% Definition of the parameters of the transformation
switch istransf
    %%% without transformation:
    case 0
        sfxrho = '_noR'; % suffix _noR added when no transformation is applied
        sim.rs = zeros(2,1); % (Nr,Nsig)    
        sim.rc = zeros(2,1); % (Nr,Nsig)
    %%% with transformation:
    case 1
        sfxrho = '_R'; % suffix _R added when the transformation is applied
        sim.rs = [ 0.15;
                   0.05];
        sim.rc = [ 0.13;
                   0.07];
end

%% Definition of the frequency band
% fb = max([max(abs(sim.ss),[],2), max(abs(sim.ss),[],2), [sim.p.a; sim.p.b]],[],2);
fb = 0.5; % fixed value of the frequency band
sim.fb = fb;  

%% Definition of the integration parameters
sim.dt = 0.01; % the time step
sim.tr = 5; % to take every tr point. (!) dt for phase is also corrected. 
sim.T = 5; %4; % 50; % simulation time
sim.T1 = 0.0; % the cut off ratio to remove transient time; zero means no cutting
%%% Note! The real time interval of the trial is T-T1.

%% Definiton of the final output file name
sim.foutname = [foutname sfxrho num2str(sfxout) '.mat']; 

end %%% end of the function
