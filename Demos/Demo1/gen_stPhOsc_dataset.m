function [out] = gen_stPhOsc_dataset(sim)
%%% This function generates syntetic data sets of the simulation of 
%%% the coupled stochastic phase oscillators with the real-valued Fourier 
%%% coefficients. The function can produce the syntetic data sets with or 
%%% without the application of the transformation function (the forward 
%%% transformation function rho) depending on the values of the parameters
%%% in sim structure.
%%% 
%%% The data set is generated from a model of two coupled oscillators 
%%% with different frequencies 
%%% dphi1/dt = omg1+sum_nm (anm(1,2)*cos(phi1)*cos(phi2)+bnm(1,2)*cos(phi1)*sin(phi2)
%%%                +cnm(1,2)*sin(phi1)*cos(phi2)+dnm(1,2)*sin(phi1)*sin(phi2))+ eta1(t),
%%% dphi2/dt = omg2+sum_nm (anm(2,1)*cos(phi2)*cos(phi1)+bnm(2,1)*cos(phi2)*sin(phi1)
%%%                +cnm(2,1)*sin(phi2)*cos(phi1)+dnm(2,1)*sin(phi2)*sin(phi1))+ eta2(t),
%%% where, eta1(t) and eta2(t) are normally distributed noises. 
%%% The transformation function is applied to the generted phases (phi1) using 
%%% theta1 = phi1 + \sum_l 1/l (sc*sin(l phi1) + ss*(1-cos(l phi1))).
%%%
%%% The data set is produced by the external funciton simSt2PhOsc.m.
%%% The results are collected by trials and by sets of data sets.
%%% The format of the output of the data set is sutable for the programs
%%% plot_rhs_phase_dataset.m and for test_dataset.m.
%%%
%%% -------------------------------------------------------
%%% This function is a modification of the script run_gen_stpo_dataset.m. 
%%% Some parts of the script is written on the base of the script gen_finger.m from
%%% SPM DCM toolbox of Wellcome Trust Centre for Neuroimaging
%%% written by Will Penny.
%%% 
%%% AY 09.03.2021
%%% --------------------------

addpath('../../SimPhOsc');
%% generation of the dataset
% if gen_data == 1 
    S = cell(sim.Nset,1);
    for n = 1:sim.Nset
        [yy,tt] = simSt2PhOsc(sim); % external function that generates the data set
        
        % thin out the data to sim.tr 
        for itr=1:sim.Ntrials
            tmp = yy{itr};
            yy{itr} = tmp(1:sim.tr:end,:);
        end
        tt = tt(1:sim.tr:end);

        S{n}.xx=yy; %% to fit the format of the outout
        S{n}.t = tt;
    end
    sim.dt = sim.tr*sim.dt; % increase the dt tr times.
    save(sim.foutname,'sim','S');
    %%% !!! turn off this output when using test_jumps.m !!!
    disp(['The results are saved in ' sim.foutname '.']);
% end
out = 0;
end 