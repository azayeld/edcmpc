function fitpar = set_param_fit_JR_NMM(inputfilename)
%%% This function sets the parameter values of fitpar structure for fitting
%%% with fFit_eDCM_PC. 
%%% The structure of the fitpar:
%%%     - inputfilename
%%%     - inputfilepath
%%%     - istransf 
%%%     - Nr is the number of regions.
%%%     - Nu is the number of between trial effects.
%%%     - isslowphase is a flag for calculation in slow phases, not used.
%%%     - A=[0 1; 1 0]; the coupling matrix.
%%%     - B{1} = zeros(size(A)); % trial effect matrix.
%%%     - Nsig is the number of the Fourier terms for the Sigma function.
%%%     - Nq is the number of the Fourier terms for the coupling function q.
%%%     - issets is the flag to perform the fitting for all sets in the
%%%     data sets. The results for every set will be saved in separate
%%%     output file.
    fitpar.inputfilename = inputfilename;
%     fitpar.inputfilename = 'outSimSt2JR_set_1.mat';
    fitpar.inputfilepath = '../Demos/Demo2/';
    fitpar.istransf = 0;
    fitpar.Nr = 2;
    fitpar.Nu = 0;
    fitpar.isslowphase = 0;
    fitpar.A = [0 1; 1 0];
    fitpar.B = zeros(size(fitpar.A));
    fitpar.Nsig = 2;
    fitpar.Nrho = 1;
    fitpar.Nq = 1;
    fitpar.issets = 0;
end