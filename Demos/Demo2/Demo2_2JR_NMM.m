%%% Demonstration script, 2nd example of generation of the synthetic data 
%%% and subsequent reconstruction of coupling and transformation functions
%%% using eDCM PC. The simulation model is two coupled stochastic Jansen 
%%% and Rit (neural mass models) models. The demonstration script consists
%%% of the following steps:
%%%     1. Preparation of the parameters of the simulation.
%%%     2. Simulation of the syntetic signals, which includes extraction of
%%%        the observable phases from the signals. 
%%%     3. Testing the syntetic data sets. 
%%%     4. Fitting the signals and finding the coupling functions with eDCM PC.
%%%     5. Plotting the results.
%%% Every step is performed by an external script. Please refere to the
%%% Manual of the eDCM PC and the comments of the scripts for more details.
%%% ----------------------------------------
%%% AY 16.04.2021
%%% ----------------------------------------

clear;

%% Run SPM
pathToSPM = 'C:\MWork\spm12'; 
addpath(pathToSPM);
eval('spm eeg;');

%% Setting the parameters of the simulation
initparamfile = 'initSimSt2JRparam.m'; 

%% Generate the synthetic data set
addpath('../../SimNMM');
simfilename = simSt2JRset(initparamfile);

%% Testing the data set. 
addpath('../../TestDataset');
test_datasets(simfilename,1,0);

%% Fitting data set
addpath('../../Fitting');
fitpar = set_param_fit_JR_NMM(simfilename);
foutname = fFit_eDCM_PC(fitpar); % call fitting function of eDCM PC.

%% Plotting results
% foutname = 'dcm_nsig2_setn1_outSimSt2JR_set_2.mat';
addpath('../../PlotRes');
plotpar.Nmesh = 20;
plotpar.showdata = 1;
plotpar.showbars = 1;
plot_res_edcm_pc(foutname,plotpar);