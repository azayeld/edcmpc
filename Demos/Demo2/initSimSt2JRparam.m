%%% initiate parameters of the Jansen-Rit neural mass model with two
%%% connected regions and defines the integration parameters.
%%% All parameters of the system are stored in a paramters structure p.
%%% The integration parameters are stored in intP, and the simulation
%%% parameters are stored in the structure sim. 

%% simulation parameters
sim.savefilename = 'outSimSt2JR_set_2.mat';
sim.Ntrial = 20; % number of trials
sim.Nset  = 1; % number of sets

%% parameters of filtering 
%%% define filter regions
sim.Fdcm{1} = [5.0 12.5];
% sim.Fdcm{2} = [5.0 12.5]; % alpha
sim.Fdcm{2} = [12.0 30.0]; % beta
% sim.Fdcm{2} = [35.0 45.0]; % gamma
sim.fltorder = 1;

%% paramters of the integration
% integration time
% intP.timeInt1 = 1.5;
% intP.timeInt2 = 3.0;
% intP.Ttotal = 1.3;

intP.N = 4*1024; % 1.5*10^4; total number of steps
sim.Ndt = 5; % 2, 5, 20; interstep multiplier, to reduce sampling rate for phases


intP.Ttran = 0.5;  % transient time without coupling
intP.Ttran0 = 1.5; % transient time to calculate the initial state. 
intP.Ton = 0.3;    % time interval with coupling
intP.Toff = 0.5;   % time interval without coupling  
intP.Ttotal = intP.Ttran + intP.Ton + intP.Toff;

sim.cutr = intP.Ttran/(intP.Ttran+intP.Ton+intP.Toff); % 0.25; % ratio of cut tails


%% paramters of the system
p.Nsize = 16; % the size of the system

% p.e0 - the maximum firering rate of neural population
p.e0 = 2.5; % s^-1
% p.v0 - the PSP for which a 50% firing rate is achieved
p.v0 = 6; % mV
% r - the steepness of the sigmoidal transformation
p.r = 0.56; % mv^-1

%%% General notations about the Jansen-Rit neural mass model: 
%%% A and B are the maximum EPSP and IPSP, respectively. 
%%% a and b are the time constants of the excitatory and inhibitory linear
%%% transformations.
%%% C1, C2, C3, and C4 are connectivity constants. 
%%% The relation between these coefficients:
%%% C1 = C2/0.8 = 4*C3 = 4*C4.
%%% inputF1 and inputF2 are the input functions for regions. 
%%% These parameters are for every region (column). 
%%% 
%%% The parameters for coupling between regions (columns):
%%% K1 and K2 are coupling coefficients,
%%% ad is a delay in the dealy block.
%%%
%%% Everywhere the indexes 1 and 2 are related to the first and to the
%%% second regions (columns), respectively. 

% alpha oscillations, Jansen Rit
ataue1 = 20; % original 10
ataui1 = 15; % original 20

% non-sine alpha
% ataue1 = 12; % original 10
% ataui1 = 26; % original 20

% alpha non-sine oscillations
% ataue2 = 15; % original 10
% ataui2 = 25; % original 20

% ataue2 = 12; % original 10
% ataui2 = 28; % original 20

% % gamma oscillations
% ataue2 = 4; % original 10
% ataui2 = 4; % original 20

%%% alpha oscillation from David 2003;
% taue = 10.8;
% taui = 22; 

% %%% beta oscillations 
% ataue2 = 10; % original 10
% ataui2 = 6; % original 20
ataue2 = 10; % original 10
ataui2 = 10; % original 20

% %%% gamma oscillations
% gtaue = 4.6;
% gtaui = 2.9;

p.a1 = 1000/ataue1;% s^-1
p.b1 = 1000/ataui1; % s^-1

p.a2 = 1000/ataue2;% s^-1
p.b2 = 1000/ataui2; % s^-1


p.A_a =  3.25/100; % the ratio Aa in the original model
p.B_b =  22/50;  % the ratio Bb in the original model

% p.A1 = 3.25; % mV
% p.B1 = 22; % mV
% p.A2 = 3.25; % mV
% p.B2 = 22; % 17.6; % mV

%%% adjusted by the ratio to ensure oscilatatory solution
p.A1 = p.A_a*p.a1; % mV
p.B1 = p.B_b*p.b1; % mV
p.A2 = p.A_a*p.a2; % mV
p.B2 = p.B_b*p.b2; % mV

p.ad = 30; % 30 s^-1 % the delay in the delay block

p.C1 = 135; % unitless, this should produce alpha like oscillations
p.C11 = p.C1;
p.C21 = 0.8*p.C1;
p.C31 = p.C1/4;
p.C41 = p.C1/4;

p.C2 =  135; % 170; % 135; %  108;% 350; 128;
p.C12 = p.C2;
p.C22 = 0.8*p.C2;
p.C32 = p.C2/4;
p.C42 = p.C2/4;

%%% input function parameters
% p.noiselow = 120; % not used
% p.noisehigh = 320;
p.noiseMean1 = 220; % 220;
p.noiseMean2 = 220; % 220;
p.noiseAmpl1 = 0.1; %0.2; % 0.25;
p.noiseAmpl2 = 0.1; %0.2; % 0.25;
p.noiseStd = 1.0; %1.0;


%%% input functions
% p.inputF1 = @(t) p.noiseMean1; % constant input, the stochastic part is 
% p.inputF2 = @(t) p.noiseMean2; % defined in gJRnmm2.m

%%% coupling coefficients, the first to the second
p.K1 = 800.0; % 400,0; 0.0; 
p.K2 = 0.0; % 1200; % 800.0; 


