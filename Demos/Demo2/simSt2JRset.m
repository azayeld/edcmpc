function foutname = simSt2JRset(simparfilename)
%%% this function generates the set of trials of the JR neural mass model 
%%% (NMM) with the parameter values given in the structure sim. This is the
%%% modified version of simJR2set_dcm_Hilb_Delay_onoff.m
%%%
%%% The input:
%%%     simparfilename is the name of a script that sets the values of 
%%%         the integration and simulation parameters.

    %% run the script with parameters initiation
    disp(['run ' simparfilename '...']);
    run(simparfilename); 
    disp('completed');
    %%% now the following structures with the parameter values are loaded:
    %       p is the structure with the system parameters
    %       intP is the structure with the integration parameters
    %       sim is the structure with the simulation parameters
    %   see details in the simparfile. 
    
    %% run the simulation of the system
    disp(['run simulation ...']);
    Ss = simJR2alltrial3(p,intP,sim.Ntrial); % the generated trials are stored in Ss structure.
    disp('completed');
    
    sim.dt = intP.Ttotal/intP.N;
    
    %% plot results: allocate subplots
    hf40 = figure(40);
    f40w = 700;
    f40h = 650;
    Nvec = [f40w f40h f40w f40h]; % the normalization vector
    set(hf40,'Renderer','Painters','Units','pixels','Position',[100 100 f40w f40h],'Color',[1 1 1]); 
    % plot the original signal and the cosine of the extracted phase
    sp1 = subplot('Position',[50 500 250 100]./Nvec);
    sp2 = subplot('Position',[50 350 250 100]./Nvec);
    
    % plot the mean spectrum    
    sp3 = subplot('Position',[350 350 125 250]./Nvec);
    sp4 = subplot('Position',[525 350 125 250]./Nvec);
    
    % plot modded phase variables 
    sp5 = subplot('Position',[50 175 250 100]./Nvec);
    
    % plot phase difference
    sp6 = subplot('Position',[55 75 250 75]./Nvec);
    
    % plot the derivatives of the phases
    sp7 = subplot('Position',[350 75 300 200]./Nvec);

    %% Find spectrum
    %%% calculate mean spectrum
    Fs = intP.N/intP.Ttotal; % sampling rate
    [L,Nr] = size(Ss{1}.Y); % all trials have the same length
    hL = ceil(L/2); % make half L integer
    P = zeros(hL+1,Nr); % allocale memory for power spectrum
    f = Fs*(0:hL)/L; % frequency axes, same for all

    %%% loop by trials
    for n=1:sim.Ntrial,
        %%% loop by regions
        for c=1:Nr,
            xr=Ss{n}.Y(:,c);
            %%% commented,since all signals have the same length            
            %%% L = length(xr); 
            fY = fft(xr);
            P2 = abs(fY/L);
            P1 = P2(1:hL+1);
            P1(2:end-1) = 2*P1(2:end-1);
            P(:,c) = P(:,c) + P1;
        end %% end loop by regions
    end %%% end loop by trials

    P = P/(sim.Nset*sim.Ntrial);

    rngFreq = [0.1, 50];  % frequency range
    irngFreq = round(rngFreq*Ss{1}.T(end))+1; % index of the frequency range
    
    % plot results 
    plot(sp3,f(irngFreq(1):irngFreq(2)),P(irngFreq(1):irngFreq(2),1),'b');
    xlim(sp3,[0 50]);
    % title(sp3,'PS of 1st reg');
    set(sp3,'XTick',[0:10:50]);
    xlabel(sp3,'f, Hz');
    ylabel(sp3,'Power Spectrum of 1st region');

    plot(sp4,f(irngFreq(1):irngFreq(2)),P(irngFreq(1):irngFreq(2),2),'r');
    xlim(sp4,[0 50]);
    % title(sp4,'PS of 2nd reg');
    set(sp4,'XTick',[0:10:50]);
    xlabel(sp4,'f, Hz');
    ylabel(sp4,'Power Spectrum of 2nd region');
    
    %% Filter signals and find the phase using Hilbert transformation
    disp('Filtering and phase extraction...');
    Fn = Fs/2; % Nyquist frequency
    % allocate memory for the intrinsic varibles
    yyf = zeros(L,Nr);

    ts = round(L*sim.cutr); % define the index that correspond to cutr
    tims = Ss{1}.T; % time variabel is the same for all trials
    tphi = tims(ts:end-ts); % cut edges
    Ntphi = length(tphi);
    phiy = zeros(Ntphi,Nr); % allocate space for phase
    % aphi = zeros(Ntphi*sim.Nset*sim.Ntrials,Nr); % allocalte space for all phase values
    % phi0 = zeros(sim.Nset*sim.Ntrials,Nr);  % allocate space for 1st phases.
    
    S{1}.t = Ss{1}.T; % store time variable
    tims = Ss{1}.T; % time is the same as t

    %%% loop by trials
    for n=1:sim.Ntrial,
        %%% loop by regions
        for c=1:Nr,
            xr=Ss{n}.Y(:,c); 
            % xr = xr - mean(xr);
            xr = xr - (max(xr)+min(xr))/2;
%                 %%% Filtering: commented, not used
%                 % alternative filtering with  field trip band pass filter
%     %             xrf = ft_preproc_bandpassfilter(xr', Fs, sim.Fdcm{c}, sim.fltorder);
%                 [b,a] = butter(sim.fltorder, sim.Fdcm{c}/Fn,'bandpass');
%     %             [b,a] = butter(sim.fltorder, sim.Fdcm{c}(2)/Fn,'low');
%     %             freqz(b,a);
%                 xrf = filtfilt(b,a,xr);
%     %             xrf = filter(b,a,xr);
%                 yyf(:,c) = xrf; % filtered signal
            %%%% commented filtering

            xrf = xr;
            yyf(:,c) = xrf;

            %%% Phase extraction:
            %  hx=spm_hilbert(xrf); % hilbert for filtered signal
            %  hx = hilbert(xrf,1024);
            hx=spm_hilbert(xr); % hilbert for not filtered signal
            %  hx = hilbert(xr);

            % extract observable phase as the angle 
            phi =unwrap(double(angle(hx)));
            phiy(:,c) = phi(ts:end-ts); % cut off tails
            tphi = tims(ts:end-ts); 

            % plot the results only for the first trial
            if n==1 
                if c==1 
                    ax = sp1;
                    plot(ax,tims,xr,'b');
                else 
                    ax = sp2;
                    plot(ax,tims,xr,'r');
                end
                
                xlim(ax,[0 intP.Ttotal]);
                hold(ax,'on');
                plot(ax,tphi,cos(phiy(:,c)),'k');
                %%% plot transient, on and off times
                % plot(ax,intP.Ttran*[1 1],ylim(ax),'k--');
                % plot(ax,(intP.Ttran+intP.Ton)*[1 1],ylim(ax),'k--');
                plot(ax,tphi(1)*[1 1],ylim(ax),'k:');
                plot(ax,tphi(end)*[1 1],ylim(ax),'k:');
                hold(ax,'off');
                xlabel(ax,'time');
                ylabel(ax,['y_' num2str(c)]);
            end
       end %% end loop by regions
       S{1}.yy{n} = Ss{n}.Y;
       S{1}.xx{n} = phiy;
       S{1}.tphi = tims(ts:end-ts)-tims(ts);
       
       if n==1
            tphi = tims(ts:end-ts);
            phi1 = phiy(:,1); % -phiy(1,1);
            phi2 = phiy(:,2); % -phiy(1,2);
            
            % plot modded phases            
            plot(sp5,tphi,mod(phi1,2*pi),'b');
            hold(sp5,'on');
            plot(sp5,tphi,mod(phi2,2*pi),'r');
            hold(sp5,'off');
            xlim(sp5,[tphi(1) tphi(end)]);
%             legend(sp5,'phi1','phi2');
            xlabel(sp5,'time');
            ylabel(sp5,'\theta_1,\theta_2');
            
            
            % plot phase difference
            plot(sp6,tphi,mod(phiy(:,1)-phiy(:,2),2*pi),'k-');
            xlim(sp6,[tphi(1) tphi(end)]);
%             legend(sp6,'\phi_1-\phi_2');
            xlabel(sp6,'time');
            ylabel(sp6,'\theta_1-\theta_2');
            
       end
       
    end %%% end loop by trials
    disp('Filtering and phase extraction complete ...');
    

     %% resample all points 
    % the variables that have to be resampled
    %       S.tphi 
    %       S.xx
    
    Ntphi=length(S{1}.tphi);
    try isfield(sim,'Ndt');
        Ndt = sim.Ndt;
    catch
        Ndt = 20;  % we take only every 20th point, down sample. 
    end
    ires = [1:Ndt:Ntphi]; % resample index.

    S{1}.tphi = S{1}.tphi(ires);
    %%% loop by trials
    for n=1:sim.Ntrial,
        S{1}.xx{n} = S{1}.xx{n}(ires,:);
    end
    sim.dt = sim.dt*Ndt;
    
    %% plot the results of the resampled phases
    dphi1dt = diff(S{1}.xx{1}(:,1))./diff(S{1}.tphi);
    dphi1dt = dphi1dt - mean(dphi1dt);
    dphi2dt = diff(S{1}.xx{1}(:,2))./diff(S{1}.tphi);
    dphi2dt = dphi2dt - mean(dphi2dt);
    
    plot(sp7,S{1}.tphi(1:end-1),dphi1dt,'b');
    hold(sp7,'on');
    plot(sp7,S{1}.tphi(1:end-1),dphi2dt,'r');
    hold(sp7,'off');
    ylabel(sp7,'d\theta_1/dt, d\theta_2/dt');
    xlabel(sp7,'time');
    
    save(sim.savefilename,'sim','S','intP','p');
    disp(['The results are saved in ' sim.savefilename '.']);
    
    foutname = sim.savefilename;
end